Contact: mailto:security@rugged.works
Expires: 2026-01-01T04:00:00.000Z
Preferred-Languages: en
Canonical: https://rugged.works/.well-known/security.txt
Policy: https://rugged.works/SECURITY.md
