# Reporting Security Issues

The Rugged team and community take security issues in Rugged seriously,
particularly since our goal is to secure software supply chains. We appreciate
your efforts to responsibly disclose your findings, and will make every effort
to acknowledge your contributions.

To report a security issue, please email security@rugged.works to let us know of a potential vulnerability.

Until we establish a secure communication channel with you, PLEASE OMIT any detailed
description of the issue, the steps you took to create the issue, affected
versions, and if known, mitigations for the issue. 

Our vulnerability management team will acknowledge receiving your email within 3 
working days to establish a secure channel within which to share further details. 
This project follows a 90 day disclosure timeline.

After the initial reply to your report, the security team will keep you
informed of the progress towards a fix and full announcement, and may ask for
additional information or guidance.

Report security bugs in third-party modules to the person or team maintaining
the module. In particular, if you are reporting a vulnerability with The Update
Framework (TUF) specification, rather than the Rugged implementation of TUF,
please report issues to them directly: https://theupdateframework.io/docs/security/reporting/.
