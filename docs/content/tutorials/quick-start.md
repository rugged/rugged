---
title: Quick Start
weight: 10

---

This tutorial will show you how to:
* [Install development tools](#local-development-tools)
* [Download Rugged](#download-the-code)
* [Install the Rugged system](#install-the-rugged-system)
* [Create sample comtent](#create-development-fixtures)
* [Run basic Rugged commands](#run-basic-rugged-commands)


Local development tools
-----------------------

We recommend a modern Unix-like operating system (eg. Ubuntu or OSX). On Windows OS, we suggest using [Virtualbox](/tutorials/getting-started/virtualbox) to get a working Linux system.


### Git

We use [Git](https://git-scm.com/) as a version control system, so make sure that it's installed:

```bash
$ sudo apt install git    # For Debian

$ brew install git        # For OSX
```


### DDEV

We use [DDEV](https://ddev.com) to build and manage a Docker-based local development environment. Installing it should just require:

``` bash
$ curl -LO https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh && bash install_ddev.sh
```

If that doesn't work, refer to the [DDEV Installation instructions](https://ddev.readthedocs.io/en/stable/#installation).

Finally, make sure to install a local Certificate Authority (CA):

```bash
$ mkcert -install
```

This allows DDEV to provide locally hosted sites under HTTPS without triggering browser warnings.


### Drumkit

We use [Drumkit](http://drumk.it) to automate common development tasks. Drumkit requires GNU Make, so make sure that it's installed:

```bash
apt install make    # For Debian

brew install make   # For OSX
```


Download the Code
-----------------

For development and testing, this project uses Git submodules. So make sure to use `--recursive` whenever you clone the project. If you ever forget to, you can always run `git submodule update --init` from within the project.

```
git clone --recursive https://gitlab.com/rugged/rugged.git
cd rugged
```


Install the Rugged system
-------------------------

First, we need to bootstrap Drumkit:

```bash
$ . d       # on Bash shells

$ source d  # on non-Bash shells
```

**N.B.** We're sourcing the file `d` here. This loads some variables into the environment for this terminal session. You'll need to run `. d` (or `source d`) whenever you want to use Drumkit in a new terminal.

Next, we can start the DDEV Docker containers:

```bash
make start
```


Create development fixtures
---------------------------

In order to explore Rugged's functionality, it's easiest to start by creating some sample content:

```bash
$ make fixtures
```

You can then review the generated fixtures with:

```bash
$ tree fixtures
```

In particular, the TUF metadata Rugged generates can be found at `fixtures/tuf_repo/metadata`.


Run basic Rugged commands
-------------------------

Finally, let's run a few Rugged commands to see what's going on.

We've added a DDEV command that delegates calls to `rugged` into the "packaging pipeline" container. For convenience, it also handles calling `rugged`, via `sudo` as the 'rugged' user.

```bash
ddev rugged
```

We can now see the various sub-commands that are available. Next, let's see what's in the logs:

```bash
ddev rugged logs
```

This prints the last 10 lines of the logs of each of the Rugged workers. Many commands support a `--worker` option, that will show just the results from a single worker.

Let's try that option out when we look at the status of the repo:

```bash
ddev rugged status --worker=targets-worker
```

This display all relevant information about the state of the repo *from the target worker's perspective*.

Let's try again, but look at the timestamp worker's status

```bash
ddev rugged status --worker=timestamp-worker
```

One difference worth noting between these is which role has the "Signing" capability. Each worker performs a specific role withing the TUF system. With one important exception (the root worker), each will only have access to a single signing (ie. private) key. This is a key aspect of the TUF security model.


Next Steps
----------

We suggest that you check out the other [tutorials](/tutorials) and [how-to guides](/how-to) next.

Alternatively, you can also read more [background information](/background) about how and why Rugged operates the way that it does.
