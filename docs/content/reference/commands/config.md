---
title: "rugged config"
hidden: True
---

#### Command help text
```terminal
$ rugged config --help

Usage: rugged config [OPTIONS]

  Print the config for a TUF repository.

Options:
  --local        Print the local config. Defaults to true, unless a worker is
                 specified.
  --worker TEXT  The specific worker from which to retrieve config. Can be
                 passed multiple times.
  --help         Show this message and exit.
```

#### Command example output
```terminal
$ rugged config --worker=test-worker

=== Configuration for test-worker ===
=== Configuration for test-worker ===
host                  rabbitmq
password              REDACTED
username              rugged-rabbitmq
log_file              /var/log/rugged/rugged.log
log_format            %(asctime)s %(levelname)s (%(module)s.%(funcName)s): %(message)s
repo_path             /var/rugged/tuf_repo
inbound_targets_path  /var/rugged/incoming_targets
repo_targets_path     /var/rugged/tuf_repo/targets
repo_metadata_path    /var/rugged/tuf_repo/metadata
roles                 {'root': {'keys': ['root', 'root1']}, 'snapshot': {'keys': ['snapshot']}, 'targets': {'keys': ['targets']}, 'timestamp': {'keys': ['timestamp']}}
workers               {'root': {'name': 'root-worker'}, 'snapshot': {'name': 'snapshot-worker'}, 'targets': {'name': 'targets-worker'}, 'timestamp': {'name': 'timestamp-worker'}, 'test': {'name': 'test-worker'}}
consistent_snapshot   False
print_host_headers    True

```
