---
title: "rugged status"
hidden: True
---

#### Command help text
```terminal
$ rugged status --help

Usage: rugged status [OPTIONS]

  Print a status message for a TUF repository.

Options:
  --local        Print the local repo status. Defaults to true, unless a
                 worker is specified.
  --worker TEXT  The specific worker from which to retrieve repo status. Can
                 be passed multiple times.
  --help         Show this message and exit.
```

#### Command example output
```terminal
$ rugged status --worker=test-worker

=== Repository status for test-worker ===
  Targets  Total Size
---------  ------------
        1  6 Bytes

Role       Capability    Signatures      Version  TUF Spec    Expires
---------  ------------  ------------  ---------  ----------  -------------------
targets    Signing       1 / 1                 2  1.0.28      2022-04-22T21:57:38
snapshot   Verification  1 / 1                 2  1.0.28      2022-04-22T21:57:38
timestamp  Verification  1 / 1                 2  1.0.28      2022-04-16T21:57:38
root       Verification  2 / 1                 1  1.0.28      2023-04-15T21:57:38

Key name    Role       Key type(s)      Scheme    Path
----------  ---------  ---------------  --------  -------------------------------------------
targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
snapshot    snapshot   public           ed25519   /var/rugged/verification_keys/snapshot.pub
timestamp   timestamp  public           ed25519   /var/rugged/verification_keys/timestamp.pub
root        root       public           ed25519   /var/rugged/verification_keys/root.pub
root1       root       public           ed25519   /var/rugged/verification_keys/root1.pub

```
