---
title: "rugged add-targets"
hidden: True
---

#### Command help text
```terminal
$ rugged add-targets --help

Usage: rugged add-targets [OPTIONS]

  Add targets to a TUF repository.

Options:
  --help  Show this message and exit.
```

#### Command example output
```terminal
$ rugged add-targets 

Added the following targets to the repository:
example.txt
Updated targets metadata.
Updated snapshot metadata.
Updated timestamp metadata.
```
