---
title: "rugged initialize"
hidden: True
---

#### Command help text
```terminal
$ rugged initialize --help

Usage: rugged initialize [OPTIONS]

  Initialize a new TUF repository.

Options:
  --local  Generate keys locally, rather than delegating to the root worker.
  --help   Show this message and exit.
```

#### Command example output
```terminal
$ rugged initialize 

TUF repository initialized.
```
