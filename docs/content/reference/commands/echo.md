---
title: "rugged echo"
hidden: True
---

#### Command help text
```terminal
$ rugged echo --help

Usage: rugged echo [OPTIONS]

  Ping a worker, by sending an echo task on the queue.

Options:
  -m, --message TEXT     The message to send to the worker
  --worker TEXT          The specific worker to ping. Can be passed multiple
                         times.
  -u, --username TEXT    The username for the RabbitMQ connection.
  -p, --password TEXT    The password for the RabbitMQ connection.
  -h, --host TEXT        The hostname for the RabbitMQ connection.
  -t, --timeout INTEGER  The time to wait for a task to complete (in seconds)
  --help                 Show this message and exit.
```

#### Command example output
```terminal
$ rugged echo --worker=test-worker

Sending test-worker Ping!...
Done. Response was: test-worker PONG: Ping!
```
