---
title: Click (CLI Creation Kit)
menutitle: Click (CLI)
weight: 50

---

From the [Click Project](https://click.palletsprojects.com/en/8.0.x/) docs:

>Click is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary. It’s the “Command Line Interface Creation Kit”. It’s highly configurable but comes with sensible defaults out of the box.

>It aims to make the process of writing command line tools quick and fun while also preventing any frustration caused by the inability to implement an intended CLI API.


## Documentation

- [Documentation site](https://click.palletsprojects.com/en/8.0.x/#documentation)
