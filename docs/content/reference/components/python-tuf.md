---
title: Python TUF
weight: 30

---

[Python TUF](https://github.com/theupdateframework/python-tuf) is the reference implementation of [The Update Framework (TUF)](/package-signing/tuf/reference/tuf/). It is written in Python and intended to conform to version 1.0 of the [TUF specification](https://theupdateframework.github.io/specification/latest/).

**N.B.** Python-TUF contains both a legacy implementation and a more modern one. We are currently targeting the legacy implementation.

## Documentation

- [Root `README`](https://github.com/theupdateframework/python-tuf/blob/develop/README.md)
- [Threat model (vectors of attack)](https://github.com/theupdateframework/python-tuf/blob/develop/tuf/ATTACKS.md)
- [Advanced (server/repository) tutorial](https://github.com/theupdateframework/python-tuf/blob/develop/docs/TUTORIAL.md)
- [TUF Developer Tools](https://github.com/theupdateframework/python-tuf/blob/develop/tuf/README-developer-tools.md)
- [Client (`updater.py`) README](https://github.com/theupdateframework/python-tuf/tree/develop/tuf/client)
