---
title: Satis
weight: 50

---

[Satis](https://github.com/composer/satis) is a static Composer repository generator. It can be used to generate a composer repository for development and testing purposes.

This composer repository is used as a "stand in" for https://packages.drupal.org (and possibly Packagist) in testing scenarios.

We will reference the Satis-built composer repository in the `repositories` section of our [test Drupal site's composer.json](https://gitlab.com/consensus.enterprises/clients/drupal-assoc/tuf/-/blob/main/d9-site/composer.json).

We install Satis via composer inside `./satis`, and you can run it locally using `ddev satis`.

## Documentation

- [Official documentation](https://getcomposer.org/doc/articles/handling-private-packages.md#satis)

