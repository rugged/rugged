---
title: Celery Distributed Task Queue
menutitle: Celery Task Queue
weight: 40

---

From the [Celery Project](https://docs.celeryproject.org) docs:

>Celery is a simple, flexible, and reliable distributed system to process vast amounts of messages, while providing operations with the tools required to maintain such a system.
>
>It’s a task queue with focus on real-time processing, while also supporting task scheduling.

## Documentation

- [Documentation site](https://docs.celeryproject.org/en/stable/index.html)
