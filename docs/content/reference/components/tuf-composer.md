---
title: PHP-TUF Composer Plugin
weight: 10

---

[PHP-TUF Composer Integration Plugin](https://github.com/php-tuf/composer-integration) integrates the [PHP-TUF library](/package-signing/tuf/reference/components/php-tuf/) into Composer operations such as downloading and package discovery.

**N.B.** [`generate.py`](https://github.com/php-tuf/composer-integration/blob/main/generate.py) illustrates how to use [`repository_tool`](https://github.com/theupdateframework/python-tuf/blob/develop/tuf/repository_tool.py) directly.

## Documentation

- [Root `README`](https://github.com/php-tuf/composer-integration/blob/main/README.md)
- [Automated tests](https://github.com/php-tuf/composer-integration/actions/workflows/build-fixture.yml) and related [config](https://github.com/php-tuf/composer-integration/blob/main/.github/workflows/build-fixture.yml).
