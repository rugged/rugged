---
title: Package release workflow with Monitor Worker
menuTitle: Packaging Workflow (w/ Monitor)
weight: 20

---

The diagram below illustrates a generic package release workflow with Rugged TUF signing integrated.

Examples:
* [`packages.json`](/examples/packages.drupal.org/8/packages.json)
* [`<PACKAGE>.json`](/examples/packages.drupal.org/files/packages/8/p2/drupal/config_enforce.json)
* [`<PACKAGE>-<VERSION>.zip`](/examples/ftp.drupal.org/files/projects/config_enforce-1.0.0-rc2.zip)
* [`targets.json`](/examples/tuf/metadata/targets.json)
* [`snapshot.json`](/examples/tuf/metadata/snapshot.json)
* [`timestamp.json`](/examples/tuf/metadata/timestamp.json)

{{< mermaid >}}

sequenceDiagram

    participant PP as Packaging<br />Pipeline
    participant POST as Post-to-TUF<br />Directory (NFS)
    participant TUF as Monitor<br />Worker
    participant FS as Shared<br />Filesystem(s)
    participant TARGETS as Targets<br />Worker
    participant SNAPSHOT as Snapshot<br />Worker
    participant TIMESTAMP as Timestamp<br />Worker

    autonumber
    activate TUF
    rect rgba(0, 0, 0, .05)
        note over POST,TUF: Part 1: Periodic scan for new targets
        loop Every 5 seconds
            TUF->>POST: Scan post-to-TUF directory
            activate POST
            POST->>TUF: List of post-to-TUF directory contents
            deactivate POST
            break If a target is being processed
                TUF--xTUF: STOP<br /> - - (Allow currently processing target to complete) - -
                deactivate TUF
            end
                activate TUF
            break If no targets are ready to be processed
                TUF--xTUF: STOP<br /> - - - - - - - - - - - (Nothing to do) - - - - - - - - - - - -
                deactivate TUF
            end
        end
    end

    rect rgba(0, 255, 255, .1)
        note over PP: Part 2: Regular packaging pipeline
        activate PP
        PP->>PP: Clone code
        PP->>PP: Build package
        note right of PP: &lt;PACKAGE&gt;-&lt;VERSION&gt;.zip
        PP->>PP: Generate package<br />metadata
        note right of PP: &lt;PACKAGE&gt;.json
    end

    rect rgba(255, 0, 0, .1)
        note over PP,TUF: Part 3: Post to TUF
        PP->>PP: Generate timestamp
        PP->>POST: Create unique temporary directory
        activate POST
        note left of POST: tuf_tmp_&lt;TIMESTAMP&gt;/

        loop Every 5 seconds
            TUF->>POST: Scan post-to-TUF directory
            activate TUF
            POST->>TUF: List of post-to-TUF directory contents
            break If no targets are ready
                TUF--xTUF: STOP<br /> - - (Monitor worker only looks for 'tuf_ready_') - -
                deactivate TUF
            end
        end

        PP->>POST: Copy target files to directory<br />(across network filesystem boundary)
        PP->>POST: Rename temporary directory
        deactivate PP
        deactivate POST
        note left of POST: tuf_ready_&lt;TIMESTAMP&gt;/

        loop Every 5 seconds
            TUF->>POST: Scan post-to-TUF directory
            activate TUF
            activate POST
            POST->>TUF: List of post-to-TUF directory contents
            deactivate POST
            break If another target is being processed
                TUF--xTUF: STOP<br /> - - (Allow currently processing target to complete) - -
                deactivate TUF
            end
        end
    end

    rect rgba(0, 255, 0, .1)
        note over POST,FS: Part 4: Prepare inbound targets
        loop Every 5 seconds
            TUF->>POST: Scan post-to-TUF directory
            activate TUF
            activate POST
            POST->>TUF: List of post-to-TUF directory contents
            TUF-->TUF: No currently processing targets
            critical Ready target is detected
                TUF->>POST: Rename directory to indicate processing
                note right of POST: tuf_processing_&lt;TIMESTAMP&gt;/
                POST->>FS: Create processing directory in inbound directory
                activate FS
                note left of FS: tuf_processing_&lt;TIMESTAMP&gt;/
                TUF->>POST: 
                POST->>FS: Move target files<br />to inbound processing directory<br />(across network filesystem boundary)
                TUF->>FS: Move target files<br />from inbound processing directory<br />to inbound directory
                TUF->>FS: Delete inbound processing directory

                TUF->>TARGETS: Trigger signing of target files
                activate TARGETS
            end
        end
    end

    rect rgba(0, 0, 0, .05)
        note over TUF,TIMESTAMP: Part 5: Regular TUF signing
        TARGETS->>FS: Scan inbound directory
        loop For each target file
            FS-->>TARGETS: Read target file
            TARGETS->>TARGETS: Update Targets metadata<br />(with signature of target file)
        end
        TARGETS->>TARGETS: Sign Targets metadata
        TARGETS-->>FS: Write updated Targets metadata
        note right of FS: targets.json
        TARGETS->>TUF: Return status
        deactivate TARGETS

        TUF->>SNAPSHOT: Trigger Snapshot update
        activate SNAPSHOT
        SNAPSHOT->>SNAPSHOT: Update and sign<br />Snapshot metadata
        SNAPSHOT-->>FS: Write updated Snapshot metadata
        note right of FS: snapshot.json
        SNAPSHOT ->>TUF: Return status
        deactivate SNAPSHOT

        TUF->>TIMESTAMP: Trigger Timestamp update
        activate TIMESTAMP
        TIMESTAMP->>TIMESTAMP: Update and sign<br />Timestamp metadata
        TIMESTAMP-->>FS: Write updated Timestamp metadata
        note right of FS: timestamp.json
        TIMESTAMP->>TUF: Return status
        deactivate FS
        deactivate TIMESTAMP
    end

    rect rgba(0, 0, 0, .05)
        note over POST,TUF: Part 6: Clean up and release semaphore
        TUF->>POST: Delete processing directory
        deactivate POST
        deactivate TUF
    end

{{< /mermaid >}}
