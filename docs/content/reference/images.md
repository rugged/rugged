---
title: Docker images reference
menutitle: Docker images
weight: 10
---

## Docker container images

See the [background information](../background/images) for details on how this
structure fits together.


The top-level [`build/`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build)
directory houses all of the container image building machinery. Within this directory, there are 2 sub-folders:

* `packer/` - Packer-specific scripts and config
  * `docker/` - houses Packer JSON build config files for each container image
    * `packaging-pipeline.json` - based on `drud/ddev-webserver`, with our Python scripts deployed into it.
    * `test-worker.json` - based on Ubuntu Focal, with Python, Celery, and TUF libraries installed
    * `rabbitmq.json` - based on upstream RabbitMQ image, with custom credentials injected and exported to persist on the container volume
  * `scripts/` - shell scripts to manage simple provisioning steps (eg. `apt install`)
* `ansible/` - houses the Ansible playbooks and roles for more complex provisioning
  * `*.yml` - Ansible playbooks run via Packer during container builds, to provision and test the images
  * `roles/rugged.workers/` - Ansible role with tasks to provision dependencies, system users/dirs, configure Supervisor and deploy Python/Celery code.

### Images and services

* `packaging-pipeline`
  * port 80/443: nginx (serving Satis composer repository + Drupal site)
  * also houses `send_ping.py` and other "packaging pipeline" scripts
* `rabbitmq`
  * port 5672: rabbitmq service
  * port 15672: rabbitmq management UI (https://rabbitmq.ddev.site:15672)
* `flower`
  * port 8888: flower Celery management ui (https://flower.ddev.site:8888)
* `test-worker`
  * supervisord managed `test-worker.py` Celery app staying up and running

### Provisioning scripts

* [`apt.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/packer/scripts/apt.sh) - configure and install some base Apt setup
* [`cleanup.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/packer/scripts/apt.sh) - clean unnecessary apt packages
* [`php.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/packer/scripts/php.sh) - install critical PHP packages
* [`python.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/packer/scripts/python.sh) - install core Python packages
* [`utils.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/packer/scripts/utils.sh) - install some utility packages

There are currently some extra scripts we introduced custom to this project,
which should get deduplicated shortly against the ones above, coming from
[Drumkit](https://drumk.it) upstream.

* [`scripts/common/setup.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/scripts/common/setup.sh) - apt setup
* [`scripts/drumkit/setup.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/scripts/drumkit/setup.sh) - gnu make
* [`scripts/tuf/setup.sh`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/scripts/tuf/setup.sh) - pip3 and misc python dependencies/libraries (tuf, click, celery)

### Ansible playbooks and roles

* [`build/ansible/*.yml`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/ansible)
  * `rabbitmq.yml` - Playbook to build rabbitmq container with custom user/password baked in.
  * `packaging-pipeline.yml` - Playbook to build web frontend container, by triggering `tuf.workers` task `packaging-pipeline.yml`
  * `packaging-pipeline-test.yml` - Playbook to test the frontend container, by calling `send_ping.py --help` to confirm it's present and runs without error.
  * `test-worker.yml` - Playbook to provision the test-worker container, by calling `tuf.workers` task `test-worker.yml` to provision worker script and arrange `/test-worker-start.sh` to start Supervisor.
  * `test-worker-test.yml` - Playbook to test test-worker container, by confirming the container starts Supervisor and the Celery worker itself.

* [`build/ansible/roles/rugged.workers`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/build/ansible/roles/rugged.workers)
  * `files/*` - symlinks to the Python scripts
  * `tasks/*` - tasks to provision the workers and scripts, called by the playbooks above.
  * `templates/worker.conf.j2` - Supervisor config template, to manage the worker
