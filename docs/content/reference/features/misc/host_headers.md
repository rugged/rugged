---
title: "Feature: Host headers in command output can be disabled."
hidden: True
---

#### Test results for [`features/misc/host_headers.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/misc/host_headers.feature)
Running `behat features/misc/host_headers.feature` results in:

```gherkin
@rugged @cli @host-headers
Feature: Host headers in command output can be disabled.
  In order to more easily test command output,
  As a Rugged developer
  I need to be able to disable host headers in a config file.

  Background:
    Given I rebuild fixtures

  Scenario: Host headers are enabled by default, but can be disabled.
    When I run "sudo -u rugged rugged status"
    Then I should get:
      """
      === Repository status for root-worker ===
      === Repository status for timestamp-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for targets-worker ===
      """
    Given I run "sudo cp features/fixtures/no_host_headers.yaml /var/rugged/.config/rugged/config.yaml"
    When I run "sudo -u rugged rugged status"
    Then I should not get:
      """
      === Repository status for root-worker ===
      === Repository status for timestamp-worker ===
      === Repository status for snapshot-worker ===
      === Repository status for targets-worker ===
      """

1 scenario (1 passed)
6 steps (6 passed)
```
