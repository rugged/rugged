---
title: "Feature: Ensure tests are running against latest code."
hidden: True
---

#### Test results for [`features/misc/pipenv.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/misc/pipenv.feature)
Running `behat features/misc/pipenv.feature` results in:

```gherkin
@rugged @pipenv
Feature: Ensure tests are running against latest code.
  In order to test recent changes to the codebase
  As a developer
  I need to ensure that tests are run under pipenv.

  Scenario: Check that `rugged` ie being called from the correct location.
    When I run "which rugged"
    Then I should get:
      """
      /usr/local/bin/rugged
      """
    When I run "sudo -u rugged rugged --version"
    Then I should get:
      """
      rugged, version 0.1.0
      """
    When I run "pipenv run sudo -u rugged rugged --version"
    Then I should get:
      """
      rugged, version 0.1.0
      """

1 scenario (1 passed)
6 steps (6 passed)
```
