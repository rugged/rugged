---
title: "Feature: Testing tools"
hidden: True
---

#### Test results for [`features/misc/testing.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/misc/testing.feature)
Running `behat features/misc/testing.feature` results in:

```gherkin
@testing @example
Feature: Testing tools
  In order to test
  As a developer
  I need to ensure the proper tools are installed and working

  Scenario: Check that Behat is installed
    When I run "./bin/behat --version"
    Then I should get:
      """
      behat 3.
      """

1 scenario (1 passed)
2 steps (2 passed)
```
