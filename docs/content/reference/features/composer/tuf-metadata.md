---
title: "Feature: The TUF Composer plugin validates TUF metadata."
hidden: True
---

#### Test results for [`features/composer/tuf-metadata.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/composer/tuf-metadata.feature)
Running `behat features/composer/tuf-metadata.feature` results in:

```gherkin
@composer-plugin @php-tuf @satis @tuf @rugged @tuf-metadata @slow
Feature: The TUF Composer plugin validates TUF metadata.
  In order to securely build PHP codebases
  As a site builder
  I need Composer to validate TUF metadata.

  Background:
    Given I rebuild fixtures
    And I register the Rugged repo with Composer
    And I use the "rugged-composer.json" Composer file
    And I am in the "d9-site" directory
    And I run "composer install -vv"
    And I run "rm -rf web/modules/contrib/token/"

  @composer-install
  Scenario: When all TUF metadata is present, Composer TUF plugin does not emit errors during install.
    When I try to run "composer install -vv"
    Then I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      """
    Then I should not get:
      """
      root.json` resulted in a `404 Not Found`
      timestamp.json` resulted in a `404 Not Found`
      snapshot.json` resulted in a `404 Not Found`
      targets.json` resulted in a `404 Not Found`
      """

  @composer-update
  Scenario: When all TUF metadata is present, Composer TUF plugin does not emit errors during update.
    When I try to run "composer update -vv"
    And I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      """
    Then I should not get:
      """
      root.json` resulted in a `404 Not Found`
      timestamp.json` resulted in a `404 Not Found`
      snapshot.json` resulted in a `404 Not Found`
      targets.json` resulted in a `404 Not Found`
      """

  @composer-update
  Scenario Outline: When any TUF metadata is missing, Composer TUF plugin emits errors during update.
    Given I run "rm /var/rugged/pkg_repo/metadata/<role>.json"
    When I try to run "composer update -vv"
    Then I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      Client error: `GET http://packages.ddev.site/metadata/
      <role>.json` resulted in a `404 Not Found`
      """

    Examples:
      | role      |
      | timestamp |

  @composer-update
  Scenario Outline: When any TUF metadata is missing, Composer TUF plugin refuses to download packages during update.
    Given I run "rm /var/rugged/pkg_repo/metadata/<role>.json"
    When I try to run "composer update -vv"
    Then I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      """
    And the following files should not exist:
      """
      web/modules/contrib/token/token.info.yml
      """

    Examples:
      | role      |
      | root      |
      | timestamp |
      | snapshot  |
      | targets   |

7 scenarios (7 passed)
67 steps (67 passed)
```
