---
title: "Feature: A worker to sign TUF repo timestamp metadata."
hidden: True
---

#### Test results for [`features/workers/timestamp_worker.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/workers/timestamp_worker.feature)
Running `behat features/workers/timestamp_worker.feature` results in:

```gherkin
@rugged @workers @timestamp-worker
Feature: A worker to sign TUF repo timestamp metadata.
  In order to securely sign TUF repo timestamp metadata
  As an administrator
  I need to dispatch tasks to a 'timestamp' worker.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
    When I run "sudo -u rugged rugged echo --worker=timestamp-worker --timeout=1"
    Then I should get:
      """
      Sending timestamp-worker Ping!...
      Done. Response was: timestamp-worker PONG: Ping!
      """

  Scenario: Retrieve worker logs.
    Given I run "sudo -u rugged rugged echo --worker=timestamp-worker --timeout=1"
    When I run "sudo -u rugged rugged logs --worker=timestamp-worker"
    Then I should get:
      """
      INFO (base_worker.echo): timestamp-worker received echo task: Ping!
      """

2 scenarios (2 passed)
7 steps (7 passed)
```
