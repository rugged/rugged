---
title: The Update Framework
weight: 5

---

[The Update Framework (TUF)](https://theupdateframework.io/) helps developers maintain the security of software update systems, providing protection even against attackers that compromise the repository or signing keys.

## Documentation

- [Overview of TUF](https://theupdateframework.io/overview/)
- [Roles and metadata](https://theupdateframework.io/metadata/)
- [TUF Specification](https://theupdateframework.github.io/specification/latest/)
