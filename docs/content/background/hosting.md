---
title: Hosting Requirements

---

The hosting of the TUF meta-data itself should be pretty straight-forward. It should not require much more than:
* Nginx serving static files over HTTPS only
* A read-only NFS(-like) mount of the TUF meta-datai (the aforementioned static files)
