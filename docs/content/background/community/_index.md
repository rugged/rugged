---
title: Community
weight: 40

---

Rugged's community of maintainers, contributors and supporters is critical to helping the project achieve its goals.

{{% children %}}
