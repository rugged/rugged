---
title: Testing Celery Workers
menutitle: Celery Workers
weight: 10
---

## How to test the Celery workers

To check that the Celery workers are up and running, run:

```
make send-ping
```

This will trigger the [send_ping.py](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/packager/send_ping.py)
script to send an [`echo`](https://gitlab.com/drupal-infrastructure/package-signing/tuf/-/blob/main/workers/test-worker.py#L14)
task and echo back the message. You should see:

```
PINGing test-worker...
/home/linuxbrew/.linuxbrew/bin/ddev exec /usr/local/bin/send_ping.py
Sending test-worker Ping!...
Task sent. Getting response...
done. Response was: Generic Worker PONG: Ping!

done.
```

To send a specific message, run:

```
make send-ping-with-message MESSAGE=custom-message-goes-here
```

You should see:

```
make send-ping-with-message MESSAGE=custom-message-goes-here
PINGing test-worker...
/home/linuxbrew/.linuxbrew/bin/ddev exec /usr/local/bin/send_ping.py -m custom-message-goes-here
Sending test-worker custom-message-goes-here...
Task sent. Getting response...
done. Response was: Generic Worker PONG: custom-message-goes-here

/home/linuxbrew/.linuxbrew/bin/ddev exec /usr/local/bin/send_ping.py --message custom-message-goes-here
Sending test-worker custom-message-goes-here...
Task sent. Getting response...
done. Response was: Generic Worker PONG: custom-message-goes-here

done.
```

This secondary target is primarily used in our tests, and calls `send_ping.py`
twice to exercise both `-m` and `--message` variations of the argument.
