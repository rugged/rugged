---
title: Development
weight: 5

---

This section covers hands-on example of development tasks.

See the [Architecture](/reference/architecture) section for a broader discussion of Rugged's components, and how they fit together.

{{% children depth=3 %}}
