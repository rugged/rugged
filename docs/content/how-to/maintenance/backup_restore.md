---
title: How to Backup and Restore TUF Metadata
menuTitle: Backup & Restore
---

The reason to take a backup is to ensure that you can rollback to a known valid set of metadata files.

### Backup

Backing up Rugged repository metadata is relatively straight-forward:

1. Pause the automated functions in the repository (`rugged pause-processing`)
2. Ensure that the repository is consistent (`rugged validate`)
3. Take a backup of existing repository metadata (`cp -r metadata/ metadata.backup`)
4. Resume the automated functions in the repository (`rugged resume-processing`)

Steps 1 and 4 (pause/resume) are intended to ensure that we're backup up a valid set of metadata. If Rugged was in the process of, for example, adding a target, one of the metadata files could be partially written when copied, or the metadata may be temporarily inconsistent. By pausing before our backup, we ensure that any in-flight operations are completed before we proceed.

Step 2 is to ensure that we are backing up valid data. We want to avoid the possibility of overwriting a backup with invalid data.

In Step 3, we recommend just copying the contents of the `metadata` directory. This should suffice for smaller repositories.

However, larger repositories can contain gigabytes of metadata. In that case, `rsync` or an actual backup system (eg. Restic) might be a better choice.

### Restore

Restoring Rugged repository metadata is basically just the reverse of the backup procedure:

1. Pause the automated functions in the repository (`rugged pause-processing`)
2. Copy the backup data over the existing repository metadata (`cp -r metadata.backup/ metadata`)
3. Ensure that the repository is consistent (`rugged validate`)
4. Resume the automated functions in the repository (`rugged resume-processing`)

Some operations might leave cruft in the metadata directory. For example, if `rugged change-hashed-bins-count` failed to complete, there could be `bin_n.json` files using the newer schema. In that case, it might be worthwhile to clean out the metadata directory (eg. `rm metadata/*.json`) before copying in the backup data.
