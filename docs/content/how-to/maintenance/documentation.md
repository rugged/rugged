---
title: How to update documentation
weight: 100

---

We maintain the DA-TUF documentation site using [Hugo](http://www.gohugo.io/).
To get started contributing to [this
project](https://gitlab.com/consensus.enterprises/clients/drupal-assoc/tuf)
clone it from Gitlab and install Hugo:

```console
$ git clone --recursive https://gitlab.com/hres/consensus.enterprises/clients/drupal-assoc/tuf
$ cd tuf
$ make hugo        # install Hugo into the project's Drumkit
$ make docs-start  # run the local server
```

Your local DA-TUF docs site should now be available for browsing:
[http://localhost:1313/](http://localhost:1313/). When you find a typo, an
error, unclear or missing explanations or instructions, just start editing.
Find the page you’d like to edit; everything is in the `docs/content/`
directory. Make your changes and commit:

```console
$ vim docs/content/_index.md            # Add/edit/remove whatever you see fit. Be bold!
$ git diff                              # Make sure there aren’t any unintended changes.
diff --git a/docs/content/_index.md b/docs/content/_index.md
[...]
$ git commit -am”Fixed docs typo.”      # Useful commit message are a good habit.
```

To stop the local Hugo server process, you can kill the process:

```console
$ make docs-kill
```
