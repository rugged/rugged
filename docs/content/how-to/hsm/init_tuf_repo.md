---
title: Initialize TUF repo
weight: 100
---

In order to host TUF metadata using HSM-based root keys, we need to include those keys in root metadata and then add signatures from those HSMs.

This runbook contains multiple stages that will need to be accomplished by people serving in one of two roles:
* The **TUF Administrator** is responsible for coordinating the process, and performing certain steps that involve interacting with Rugged directly.
* The **Key-holders** are responsible for ensuring that the off-line root keys are kept secure, as well as using those keys to sign root metadata.

All participants should be familiar with [Runbook notation](../#notation).

## Prepare the environment

* **DO** [Install Rugged](TBD) on the Preparation computer
    * Check that Rugged is working as expected:
    ```shell
    ddev rugged
    ```

* **DO** Create the directory in which Rugged will create partial root metadata:
    ```bash
    $ mkdir fixtures/tuf_repo/partial
    $ sudo chown 440 fixtures/tuf_repo/partial
    $ mkdir fixtures/tuf_repo/tmp
    $ sudo chown 440 fixtures/tuf_repo/tmp
    ```

* **DO** Ensure that the following files do not exist:
    * `fixtures/tuf_repo/partial/1.root.json`
    * `fixtures/tuf_repo/partial/signable-1.root.json`

    We will generate these files in the following steps. So we want to confirm that we have a clean environment.

* **DO** Ensure proper ownership of key directories:
    ```bash
    $ sudo chown 440 fixtures/signing_keys/ -R
    $ sudo chown 440 fixtures/verification_keys/ -R
    ```

* **DO** Ensure that access to the repository that stores the artifacts that resulted from the [key-generation ceremony](TBD)

## Generate online keys (TUF)

* **DO** Generate the `snapshot` keypair:
    ```shell
    ddev rugged generate-keys --local --role=snapshot
    ```
* **DO** Generate the `targets` keypair:
    ```shell
    ddev rugged generate-keys --local --role=targets
    ```
* **DO** Generate the snapshot keypair:
    ```shell
    ddev rugged generate-keys --local --role=timestamp
    ```

## Generate partial root metadata

* **DO** Initialize new partial root metadata:

    ```shell
    ddev rugged initialize-partial-root-metadata
    ```

* **DO** Add online verification keys:
    ```shell
    ddev rugged add-verification-key snapshot /var/rugged/verification_keys/snapshot/snapshot.pub

    ddev rugged add-verification-key targets /var/rugged/verification_keys/targets/targets.pub

    ddev rugged add-verification-key timestamp /var/rugged/verification_keys/timestamp/timestamp.pub
    ```

* **DO** Add HSM-generated verification keys:
    ```shell
    ddev rugged add-verification-key root /opt/rugged/root_pubkey.pem --key-type=pem
    ddev rugged add-verification-key root /opt/rugged/root1_pubkey.pem --key-type=pem
    ```

* **DO** Check that the contents of the partial root metadata conform to expectations:
    ```shell
    ddev rugged show-partial-root-metadata
    ```
    This should result in output like:
    ```text
    Retrieving partial root metadata for version 1 (1.root.json).
    === METADATA ===
    Expires in 364 days, 23 hours and 59 minutes
    === SIGNATURES ===
    Signatures: 0
    Threshold: 1
    === KEYS ===
    root 2/2 (ed25519) keyid:
    root 1/2 (ed25519) keyid:
    timestamp 1/1 (ed25519) keyid:
    targets 1/1 (ed25519) keyid:
    snapshot 1/1 (ed25519) keyid:
    === ROLES ===
    root: 2 of 1 keys (keyids:
    snapshot: 1 of 1 keys (keyid:
    targets: 1 of 1 keys (keyid:
    timestamp: 1 of 1 keys (keyid:
    ```

## Send partial root metadata for Key-holder signatures

* **DO** Ensure that the following files exist:
    * `fixtures/tuf_repo/partial/1.root.json`
    * `fixtures/tuf_repo/partial/signable-1.root.json`
* **DO** Send the signable partial root metadata file (`signable-1.root.json`) to each of the Key-holders.
    * **N.B.** This file only contains public keys. As such, no special precautions are required when sending the file.

* **GO TO** [Sign partial root metadata with YubiHSM 2](TBD) (Keyholder of `YubiHSM2-1`)

* **GO TO** [Sign partial root metadata with YubiHSM 2](TBD) (Keyholder of `YubiHSM2-2`)

* **GO TO** [Sign partial root metadata with YubiHSM 2](TBD) (Keyholder of `YubiHSM2-3`)

## Sign partial root metadata with YubiHSM 2

* **DO** Insert the thumb drive into the preparation computer.

* **DO** Copy `signable-1.root.json` to the thumb drive. Eject the thumb drive.

* **DO** Insert the thumb drive into the ceremony computer.

* **DO** Generate a signature of the partial root metadata:
    ```
    $ yubihsm-shell \
        --action=sign-eddsa \
        --object-id=100 \
        --algorithm=ed25519 \
        --in=signable-1.root.json \
        --out=root_signature.bin \
        --outformat=binary \
        --auth-key=2
    ```

* **DO** Send the signature file (`root_signature.bin`) back to the TUF Administrator.

## Complete generation of root metadata

* **DO** Add the signatures:
    ```bash
    $ ddev rugged add-root-signature /opt/rugged/root_pubkey.pem /opt/rugged/root_signature.bin --key-type=pem
    $ ddev rugged add-root-signature /opt/rugged/root1_pubkey.pem /opt/rugged/root1_signature.bin --key-type=pem
    ````

* **DO** Check that the contents of the now-complete root metadata conform to expectations:
    ```bash
    $ ddev rugged show-partial-root-metadata
    ```

* **DO** Ensure that output confirma to expectations:
    ```
    Retrieving partial root metadata for version 1 (1.root.json).
    === METADATA ===
    Expires in 364 days,
    === SIGNATURES ===
    Signatures: 2
    Threshold: 1
    Signature 1 of 2: signed by root 1/2 -- UNVERIFIED (keyid:
    Signature 2 of 2: signed by root 2/2 -- UNVERIFIED (keyid:
    === KEYS ===
    snapshot 1/1 (ed25519) keyid:
    root 2/2 (ed25519) keyid:
    timestamp 1/1 (ed25519) keyid:
    root 1/2 (ed25519) keyid:
    targets 1/1 (ed25519) keyid:
    === ROLES ===
    root: 2 of 1 keys (keyids:
    snapshot: 1 of 1 keys (keyid:
    targets: 1 of 1 keys (keyid:
    timestamp: 1 of 1 keys (keyid:
    ```

## Initialize the TUF repo

* **DO** Initialize a TUF repo using the generated root metadata:
    ```
    $ chown 440 fixtures/tuf_repo/ -R
    $ cp fixtures/tuf_repo/partial/1.root.json fixtures/tuf_repo/metadata/
    $ ddev rugged initialize --local
    Initializing new TUF repository at /var/rugged/tuf_repo.
    warning: No keys found for 'root'.
    warning: Initialized 'root' metadata from disk.
    If you did not intend to initialize with existing 'root' metadata then delete '1.root.json' and re-run this command.
    Updated targets metadata.
    Updated snapshot metadata.
    Updated timestamp metadata.
    TUF repository initialized.
    ```

* **DO** Validate the resulting TUF repo metadata:
    ```
    $ ddev rugged validate metadata
    Metadata for the 'root' role is valid.
    Metadata for the 'timestamp' role is valid.
    Metadata for the 'snapshot' role is valid.
    Metadata for the 'targets' role is valid.
    ```
