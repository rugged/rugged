---
title: How to Use Hardware Security Modules
menutitle: How to Use HSMs
weight: 20

---

This section provides guidance on how to use Hardware Security Modules (HSMs) to keep `root` keys offline. Other keys can also be kept offline to enhance security further, such as the `targets` and `bins` keys.

It is inspired by the Python Software Foundation's TUF key generation and signing ceremonies [runbook](https://github.com/psf/psf-tuf-runbook) and is based on requirements for [securely managing offline keys](https://peps.python.org/pep-0458/#managing-offline-keys) as outlined in PEP-458.

{{% children %}}

## Notation

This document is designed to be read as a *runbook* -- a collection of discrete instructions with remediation steps that, if followed correctly, should result in the intended effects.

We use the following notation:

* **DO** *actions*: Perform the following actions.
* **IF** *condition* **THEN** *actions*: If *condition* is met, then perform the following *actions*.
* **GO TO** *heading*: Go to the referenced heading in the runbook and perform the stated actions thereon.
* **END**: You've reached an end state.

This document uses [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119) to describe optional and mandatory steps.
