import click
import json
import os
import sys
from collections import OrderedDict
from rugged.commands.lib.partial_root_metadata import write_partial_root_metadata
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.repo import (
    get_spec_version_string,
    seconds_from_now,
)
from tuf.api.metadata import (
    Metadata,
    Role,
    Root,
)
from rugged.tuf.metadata_files import (
    get_current_partial_root_metadata_path,
    load_partial_metadata_from_file,
 )
config = get_config()
log = get_logger()

@click.command("initialize-partial-root-metadata")
def initialize_partial_root_metadata_cmd() -> None:
    """ Initialize a new Root metadata. """

    file_path = get_current_partial_root_metadata_path()
    log.debug(f"Checking for existing file at: {file_path}.")
    if os.path.exists(file_path):
        log.error(f"A file already exists at the desired path: {file_path}")
        #@TODO: Consider adding a force option to replace the existing file.
        log.info("Consider removing the existing file and running this command again.")
        sys.exit(os.EX_CANTCREAT)

    log.info(f"Initializing partial root metadata at: {file_path}")
    expiry = config['roles']['root']['expiry'].get()
    metadata = Metadata[Root](
        signed=Root(
            version=1,
            spec_version=get_spec_version_string(),
            keys={},
            roles=_get_repo_roles_for_root_role(),
            consistent_snapshot=False,
            expires = seconds_from_now(expiry),
        ),
        signatures=OrderedDict(),
    ).to_dict()

    write_partial_root_metadata(metadata)


def _get_repo_roles_for_root_role():
    """ Return configures roles. """
    repo_roles = {}
    for role_name, role_info in config['roles'].get().items():
        threshold = role_info['threshold']
        try:
            log.debug(f"Setting '{role_name}' signature threshold to {threshold}.")
            repo_roles[role_name] = Role([], threshold=threshold)
        except ValueError as e:
            log_exception(e)
            error = "Failed to initialize partial root metadata."
            raise RuggedMetadataError(error)
    return repo_roles
