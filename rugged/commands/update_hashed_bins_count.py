import click
import sys
from rugged.commands.lib.update_metadata import (
    update_snapshot_metadata,
    update_targets_metadata,
    update_timestamp_metadata,
)
from rugged.lib.validate import (
    validate_metadata,
)
from rugged.lib.config import get_config
from rugged.lib.logger import (
    get_logger,
    log_exception,
)
from rugged.lib.semaphores.pause import (
    monitor_worker_is_paused,
)
from rugged.lib.semaphores.processing import (
    monitor_worker_is_processing,
)
from rugged.lib.semaphores.refresh import (
    monitor_worker_is_refreshing,
)
from rugged.tuf.repo import RuggedRepository


config = get_config()
log = get_logger()


@click.command("update-hashed-bins-count")
@click.option(
    "--confirm-backup",
    is_flag=True,
    default=False,
    help="Confirm that a recent backup was taken.",
)
@click.option(
    "--skip-pause-checks",
    is_flag=True,
    default=False,
    help="Skip checks to confirm that the repository is paused. (Not recommended)",
)
@click.option(
    "--skip-consistency-check",
    is_flag=True,
    default=False,
    help="Skip validation of repository consistency. (Not recommended)",
)
@click.option(
    "--skip-current-bin-count-confirmation",
    is_flag=True,
    default=False,
    help="Skip confirmation of the current bin count. (Not recommended)",
)
@click.option(
    "--skip-desired-bin-count-confirmation",
    is_flag=True,
    default=False,
    help="Skip confirmation of the desired bin count. (Not recommended)",
)
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="(For testing) Re-sign targets, snapshot and timestamp metadata with locally available signing keys, rather than delegating to the respective workers.",
)
def update_hashed_bins_count_cmd(
        confirm_backup,
        skip_pause_checks,
        skip_consistency_check,
        skip_current_bin_count_confirmation,
        skip_desired_bin_count_confirmation,
        local,
    ) -> None:
    """ Re-write the repository to use a new number of hashed bins. """

    log.info("Preparing to re-write the repository to use a new number of hashed bins.")

    log.info("Loading repository.")
    repo = _load_repo()

    log.info("Starting safety checks.")
    _confirm_backup(confirm_backup)
    _check_repo_is_paused(skip_pause_checks)
    _check_repo_is_consistent(skip_consistency_check)
    _confirm_bin_counts(repo, skip_current_bin_count_confirmation, skip_desired_bin_count_confirmation)
    _check_for_bin_name_collisions(repo)

    log.info("Starting to re-write the repository.")
    current_bins = _retrieve_current_bins(repo)
    _initialize_updated_bins(repo)
    _move_targets_to_new_bins(repo, current_bins, local)


def _load_repo():
    """ Return the fully loaded TUF repo. """
    try:
        repo = RuggedRepository()
        # We load the metadata individually here, because repo.load() read the
        # updated config, and look for new bin_n metadata files that don't
        # exist yet.
        repo.load_metadata('timestamp')
        repo.load_metadata('snapshot')
        repo.load_metadata('targets')
        repo.load_metadata('bins')
        return repo
    except Exception as e:
        log_exception(e)
        log.error("Failed to instantiate repository.")
        sys.exit("Check the logs for more detailed error reporting.")

def _confirm_backup(confirm_backup) -> None:
    """ Confirm that a recent backup has been taken. """

    log.warning("If this operation is interrupted before completing, it can leave the TUF repository in an inconsistent state, and thus make it unusable.")
    log.warning("Taking a backup is highly recommended. See: https://rugged.works/how-to/maintenance/backup_restore/)")
    if confirm_backup:
        log.debug("--confirm-backup flag received. Skipping prompt.")
    else:
        click.confirm("Please confirm that you have taken a recent backup.", abort=True)
    log.info("Recent backup confirmed.")


def _check_repo_is_paused(skip_pause_checks) -> None:
    """ Check that the repository is paused. """

    log.warning("If changes occur to the repository while this operation is in process, it can leave the TUF repository in an inconsistent state, and thus make it unusable.")
    if skip_pause_checks:
        log.debug("--skip-pause-checks flag received. Skipping checks.")
        return

    log.info("Checking that the monitor-worker is paused.")
    if monitor_worker_is_paused():
        log.info("The monitor-worker is paused.")
    else:
        log.error("The monitor-worker appears not to be paused.")
        log.info("Re-run the command with --debug for more information.")
        sys.exit("Check status of repository.")

    log.info("Checking that the monitor-worker is not processing targets.")
    if not monitor_worker_is_processing():
        log.info("The monitor-worker is not processing targets.")
    else:
        log.error("The monitor-worker appears to be processing targets.")
        log.info("Re-run the command with --debug for more information.")
        sys.exit("Check status of repository.")

    log.info("Checking that the monitor-worker is not refreshing expiry periods.")
    if not monitor_worker_is_refreshing():
        log.info("The monitor-worker is not refreshing expiry periods.")
    else:
        log.error("The monitor-worker appears to be refreshing expiry periods.")
        log.info("Re-run the command with --debug for more information.")
        sys.exit("Check status of repository.")


def _check_repo_is_consistent(skip_consistency_check) -> None:
    """ Validate that the repository is consistent. """

    if skip_consistency_check:
        log.debug("--skip-consistency-check flag received. Skipping check.")
        log.info("Skipped validation of repository consistency.")
        return

    log.info("Validating the consistency of the repository.")
    validate_metadata()


def _confirm_bin_counts(repo, skip_current_bin_count_confirmation, skip_desired_bin_count_confirmation) -> None:
    """ Confirm that existing and desired bin counts are correct. """

    current_bin_count = len(_get_current_bins(repo))
    if skip_current_bin_count_confirmation:
        log.debug("--skip-current-bin-count-confirmation option received. Skipping prompt.")
    else:
        click.confirm(f"Please confirm the current number of bins being used in the repository: {current_bin_count}", abort=True)
    log.info(f"Current bin count: {current_bin_count}")

    desired_bin_count = _get_desired_bin_count()
    if skip_desired_bin_count_confirmation:
        log.debug("--skip-desired-bin-count-confirmation option received. Skipping prompt.")
    else:
        click.confirm(f"Please confirm the desired number of bins to be used in the repository: {desired_bin_count}", abort=True)
    log.info(f"Desired bin count: {desired_bin_count}")

def _check_for_bin_name_collisions(repo):
    """ Check for collisions between old and new bin_n filenames. """
    current_bin_names = list(_get_current_bins(repo).keys())
    desired_bin_names = list(dict(repo.generate_hash_bins()).keys())
    collisions = set(current_bin_names).intersection(desired_bin_names)
    if collisions:
        log.error("Duplicates were found between the current bins names and desired bin names.")
        log.debug(f"The following duplicates were found between the current bins names and desired bin names: {collisions}")
        log.info("Re-run the command with --debug for more information.")
        sys.exit(f"Cannot update to desired hashed bins count ({len(desired_bin_names)}).")
    log.info("No duplicates were found between the current bins names and desired bin names.")

def _retrieve_current_bins(repo) -> dict:
    """ Retrieve current bins. """
    log.info("Retrieving current bins from repository.")
    current_bins = _get_current_bins(repo)
    current_bin_names = list(current_bins.keys())
    first_bin = current_bin_names[0]
    last_bin = current_bin_names[-1]
    log.info(f"Retrieved {len(current_bins)} bins, starting with '{first_bin}' and ending with '{last_bin}'.")
    return current_bins

def _get_current_bins(repo) -> dict:
    """ Return a list of current bins being used in the repository. """
    _assert_hashed_bins_enabled()
    return repo.roles['bins'].signed.delegations.roles

def _get_desired_bin_count() -> int:
    """ Return the desired number of bins to be used in the repository. """

    _assert_hashed_bins_enabled()
    return config['number_of_bins'].get()

def _assert_hashed_bins_enabled() -> None:
    """ Confirm that hashed bins are enabled. """
    assert config['use_hashed_bins'].get()

def _initialize_updated_bins(repo) -> dict:
    """ Initialize updated bins. """
    log.info("Re-initializing 'bins' and 'bin_n' metadata based on current config.")
    repo._init_bin_roles()
    updated_bins = repo.roles['bins'].signed.delegations.roles
    updated_bin_names = list(updated_bins.keys())
    first_bin = updated_bin_names[0]
    last_bin = updated_bin_names[-1]
    log.info(f"Initialized {len(updated_bins)} bins, starting with '{first_bin}' and ending with '{last_bin}'.")

def _move_targets_to_new_bins(repo, current_bins, local) -> None:
    """ Move targets to new bins. """
    for bin_name, bin_n in current_bins.items():
        log.info(f"Processing targets from '{bin_name}' metadata.")
        repo.load_metadata(bin_name)
        bin_n_targets = repo.roles[bin_name].signed.targets
        for filename, target in bin_n_targets.items():
            new_bin = repo.find_hash_bin(filename)
            log.info(f"Moving '{filename}' from '{bin_name}' to '{new_bin}'.")
            repo.add_target_to_metadata(filename, target)
        _delete_processed_bin(repo, bin_name)
    new_bins = _get_current_bins(repo)
    _write_unsigned_new_bins(repo, new_bins)
    if local:
        _sign_new_bins_locally(repo, new_bins)
        repo.update_snapshot()
        repo.write_metadata('snapshot')
        repo.update_timestamp()
        repo.write_metadata('timestamp')
    else:
        update_targets_metadata()
        update_snapshot_metadata()
        update_timestamp_metadata()

def _delete_processed_bin(repo, bin_name) -> None:
    """ Delete a processed bin. """
    repo.delete_metadata(bin_name)
    log.info(f"Deleted '{bin_name}' role from repository.")
    del(repo.roles['snapshot'].signed.meta[f"{bin_name}.json"])
    log.info(f"Deleted '{bin_name}' role from snapshot metadata.")
    repo.delete_metadata_file(bin_name)
    log.info(f"Deleted metadata file for '{bin_name}' role.")

def _write_unsigned_new_bins(repo, new_bins) -> None:
    """ Write metadata files for new bins. """
    repo.write_metadata('bins')
    for bin_name in new_bins.keys():
        repo.write_metadata(bin_name)

def _sign_new_bins_locally(repo, new_bins) -> None:
    """ Sign (and write) metadata files for new bins, using locally-available private keys. """
    repo.update_hashed_bin('bins')
    repo.write_metadata('bins')
    for bin_name in new_bins.keys():
        repo.update_hashed_bin(bin_name)
        repo.write_metadata(bin_name)
