import click
import json
import os
import sys
from collections import OrderedDict
from glob import glob
from re import sub
from rugged.commands.lib.partial_root_metadata import(
    partial_root_metadata_path,
    write_partial_root_metadata,
)
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.tuf.metadata_files import (
    get_metadata_path,
    load_production_metadata_from_file,
)
from rugged.tuf.repo import (
    get_spec_version_string,
    seconds_from_now,
)
from tuf.api.metadata import Role

config = get_config()
log = get_logger()

@click.command("initialize-partial-root-metadata-for-update")
def initialize_partial_root_metadata_for_update_cmd() -> None:
    """ Initialize a new partial Root metadata file from production Root metadata. """

    try:
        log.debug("Loading production Root metadata from file.")
        metadata = load_production_metadata_from_file('root')
    except FileNotFoundError as e:
        metadata_dir = config['repo_metadata_path'].get()
        file_path = get_metadata_path('root', metadata_dir)
        log.error(f"No production root metadata file exists at the expected path: {file_path}")
        log.info("Consider using the 'initialize' command to generate valid root metadata.")
        sys.exit(os.EX_NOINPUT)

    expiry = config['roles']['root']['expiry'].get()
    metadata.signed.expires = seconds_from_now(expiry)
    log.debug(f"Setting new expiry for partial Root metadata: {metadata.signed.expires}")

    metadata.signed.version += 1
    log.debug(f"Setting new version of partial Root metadata: {metadata.signed.version}")

    log.debug("Clearing signatures from partial Root metadata.")
    metadata.signatures.clear()

    log.debug("Updating signature thresholds from config.")
    _update_signature_thresholds(metadata)

    #@TODO: log.debug("Updating roles in partial Root metadata from config.")

    file_path = partial_root_metadata_path(metadata.signed.version)
    log.info(f"Initializing partial root metadata for update at: {file_path}")

    log.debug(f"Checking for existing file at: {file_path}.")
    if os.path.exists(file_path):
        log.error(f"A file already exists at the desired path: {file_path}")
        sys.exit(os.EX_CANTCREAT)

    write_partial_root_metadata(metadata.to_dict())

def _update_signature_thresholds(metadata):
    """ Return configures roles. """
    repo_roles = {}
    for role_name, role in metadata.signed.roles.items():
        role_config = config['roles'][role_name].get()
        log.debug(f"Setting '{role_name}' signature threshold to {role_config['threshold']}.")
        role.threshold = role_config['threshold']
