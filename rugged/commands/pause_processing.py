import click
import os
import sys
from rugged.exceptions.timeout_error import RuggedTimeoutError
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.lib.semaphores.pause import (
    create_pause_processing_flag,
    get_pause_processing_flag_path,
)
from rugged.lib.semaphores.processing import (
    wait_for_processing_task_to_complete,
)
from rugged.lib.semaphores.refresh import (
    wait_for_refreshing_task_to_complete,
)

config = get_config()
config_processing_timeout = config["wait_for_processing_task_to_complete_timeout"].get()
config_refreshing_timeout = config["wait_for_refreshing_task_to_complete_timeout"].get()
log = get_logger()

@click.command("pause-processing")
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Exit with success, even if waiting for other tasks to complete times out.",
)
@click.option(
    "--processing-timeout",
    default=config_processing_timeout,
    help=f"(Default: {config_processing_timeout}) Time (in seconds) to wait for 'find-new-targets' task to complete processing. Set to '0' to skip wait.",
)
@click.option(
    "--refreshing-timeout",
    default=config_refreshing_timeout,
    help=f"(Default: {config_refreshing_timeout}) Time (in seconds) to wait for 'refresh-expiry' task to complete processing. Set to '0' to skip wait.",
)
def pause_processing_cmd(force, processing_timeout, refreshing_timeout) -> None:
    """ Pause processing of scheduled tasks on the monitor-worker. """
    log.info("Pausing processing of scheduled tasks.")
    try:
        create_pause_processing_flag()
        if processing_timeout:
            wait_for_processing_task_to_complete(processing_timeout)
        if refreshing_timeout:
            wait_for_refreshing_task_to_complete(refreshing_timeout)
    except RuggedTimeoutError as e:
        log_exception(e)
        pause_flag_path = get_pause_processing_flag_path()
        log.warning(f"Left pause-processing flag in place at: {pause_flag_path}")
        sys.exit(os.EX_TEMPFAIL)
    log.info("Paused processing of scheduled tasks.")
