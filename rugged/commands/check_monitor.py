import click
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task

config = get_config()
log = get_logger()
workers = config['workers'].get()


@click.command("check-monitor")
def check_monitor_cmd() -> None:
    """ Report on the status of the monitor-worker. """
    results = run_task(workers['monitor']['name'], 'check_monitor_task')

    if len(results['successes']):
        log.info("\n".join(results['successes']))
    if len(results['warnings']):
        log.warning("\n".join(results['warnings']))
    if len(results['failures']):
        log.error("\n".join(results['failures']))
        log.error("Failed verifying monitor-worker status.")
        sys.exit("Check the logs for more detailed error reporting.")
