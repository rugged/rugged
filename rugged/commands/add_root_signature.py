import click
import os
import sys
from rugged.commands.lib.partial_root_metadata import write_partial_root_metadata
from rugged.commands.lib.signatures import (
    load_signature,
    verify_signature_is_valid_for_key,
)
from rugged.commands.lib.verification_keys import (
    load_verification_key,
)
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.metadata_files import (
    get_current_partial_root_metadata_path,
    load_partial_metadata_from_file,
)


log = get_logger()


@click.command("add-root-signature")
@click.argument("path_to_verification_key", type=click.Path(exists=True))
@click.argument("path_to_signature", type=click.Path(exists=True))
@click.option(
    "--key-type",
    default='tuf',
    help="The type of key used to generate the signature ('tuf' or 'pem', default 'tuf').",
)
def add_root_signature_cmd(key_type, path_to_verification_key, path_to_signature) -> None:
    """ Add a signature to partial Root metadata. """

    try:
        metadata = load_partial_metadata_from_file('root').to_dict()
    except FileNotFoundError as e:
        log_exception(e)
        file_path = get_current_partial_root_metadata_path()
        log.error(f"A metadata file was not found at the expected path: {file_path}")
        sys.exit(os.EX_NOINPUT)

    log.debug(f"Checking validity of verification key: {path_to_verification_key}")
    # We don't have a mechanism to sign root meta with and external TUF key.
    # @TODO: Consider adding a `rugged sign-partial-root-metadata` command.
    keyid, key = load_verification_key(key_type, path_to_verification_key)
    if keyid not in metadata['signed']['roles']['root']['keyids']:
        log.error(f"The provided keyid was not found among root keys: {path_to_verification_key}")
        sys.exit(os.EX_DATAERR)
    log.debug(f"The provided verification key was found among root keys: {path_to_verification_key}")

    signature = load_signature(key_type, keyid, path_to_signature)
    log.debug(f"Loaded signature for {keyid} from {path_to_signature}: {signature['sig']}")

    verified = verify_signature_is_valid_for_key(key, signature, metadata['signed'])

    sig_str = signature['sig']
    if not verified:
        log.error(f"The provided signature {sig_str} is invalid for the {keyid} key")
        sys.exit(os.EX_DATAERR)
    log.debug(f"The provided signature {sig_str} is verified as valid for the {keyid} key")

    metadata['signatures'].append(signature)

    write_partial_root_metadata(metadata)

