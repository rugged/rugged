import click
import logging
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.workers import get_workers

log = get_logger()


@click.command("echo")
@click.option(
    '--message',
    '-m',
    default='Ping!',
    help='The message to send to the worker.',
)
@click.option(
    '--worker',
    default=[],
    multiple=True,
    help="The specific worker to ping. Can be passed multiple times.",
)
@click.option(
    '--timeout',
    '-t',
    default=1,
    help='The time to wait for a task to complete (in seconds).',
)
@click.option(
    '--broker-connection-string',
    default=None,
    help='The string used to connect to the broker.',
)
def echo_cmd(worker, message, timeout, broker_connection_string) -> None:
    """ Ping a worker, by sending an echo task on the queue. """

    workers = get_workers(worker)

    log.level == logging.DEBUG
    for worker in workers:
        log.info(f"Sending {worker} {message}...")
        response = run_task(
            worker,
            'echo',
            [worker, message],
            timeout,
            broker_connection_string,
        )
        log.info(f"Done. Response was: {response}")
