import click
import os
import sys
from rugged.commands.lib.partial_root_metadata import (
    partial_root_metadata_path,
    write_partial_root_metadata,
)
from rugged.commands.lib.verification_keys import load_verification_key
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.metadata_files import (
    get_current_partial_root_metadata_path,
    load_partial_metadata_from_file,
)
from rugged.tuf.repo import seconds_from_now

config = get_config()
log = get_logger()


@click.command("add-verification-key")
@click.argument("role")
@click.argument("path_to_public_key", type=click.Path(exists=True))
@click.option(
    "--key-type",
    default='tuf',
    help="The type of key to add ('tuf' or 'pem', default 'tuf').",
)
@click.option(
    "--root-expires",
    help="The timestamp that root metadata will expire (eg. '2123-12-16T16:24:07Z').",
)
def add_verification_key_cmd(key_type, root_expires, role, path_to_public_key) -> None:
    """ Add a verification key to partial Root metadata. """

    try:
        metadata = load_partial_metadata_from_file('root').to_dict()
    except FileNotFoundError as e:
        log_exception(e)
        file_path = get_current_partial_root_metadata_path()
        log.error(f"A metadata file was not found at the expected path: {file_path}")
        sys.exit(os.EX_NOINPUT)

    _validate_role_exists(role)

    log.debug(f"Adding '{key_type}' key for '{role}' role from: {path_to_public_key}")
    keyid, key = load_verification_key(key_type, path_to_public_key)
    metadata['signed']['keys'][keyid] = key
    # If we're re-adding an existing key, don't duplicate the keyid.
    if keyid not in metadata['signed']['roles'][role]['keyids']:
        metadata['signed']['roles'][role]['keyids'].append(keyid)
    #@TODO: add an else clause here to log a suggestion to use the remove-verification-key command (when it exists)

    log.debug(f"Updating partial metadata expiry.")
    if root_expires is None:
        expiry = config['roles']['root']['expiry'].get()
        metadata['signed']['expires'] = seconds_from_now(expiry).strftime('%Y-%m-%dT%H:%M:%SZ')
    else:
        metadata['signed']['expires'] = root_expires

    file_path = partial_root_metadata_path(metadata['signed']['version'])
    write_partial_root_metadata(metadata)
    log.info(f"Updated partial root metadata at: {file_path}")


def _validate_role_exists(role: str) -> None:
    """ Ensure that the provided role exists in role configuration. """

    log.debug(f"Checking that '{role}' role is configured in the TUF repo.")
    if role not in config['roles'].keys():
        log.error(f"The '{role}' role is not configured in the TUF repo.")
        sys.exit(os.EX_DATAERR)
