import click
from rugged.lib.logger import get_logger
from rugged.lib.semaphores.processing import reset_stale_processing_dirs

log = get_logger()


@click.command("reset-processing")
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Force reset of any processing directories, even if they are not stale.",
)
def reset_processing_cmd(force) -> None:
    """ Reset stale add-targets processing directories. """

    if force:
        log.info("Forcing reset of all processing directories to ready state.")
    else:
        log.info("Resetting stale processing directories to ready state.")
    reset_stale_processing_dirs(force)
    log.info("Reset any stale processing directories to ready state.")
