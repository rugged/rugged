import click
import os
import sys
from rugged.commands.lib.partial_root_metadata import (
    write_partial_root_metadata,
)
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.metadata_files import (
    get_current_partial_root_metadata_path,
    load_partial_metadata_from_file,
)
from rugged.tuf.repo import seconds_from_now

config = get_config()
log = get_logger()


@click.command("remove-verification-key")
@click.argument("keyid")
@click.option(
    "--yes", "-y",
    is_flag=True,
    default=False,
    help="Assume 'yes' to confirmation prompt.",
)
def remove_verification_key_cmd(keyid, yes) -> None:
    """ Remove a verification key from partial Root metadata. """

    try:
        metadata = load_partial_metadata_from_file('root').to_dict()
    except FileNotFoundError as e:
        log_exception(e)
        file_path = get_current_partial_root_metadata_path()
        log.error(f"A metadata file was not found at the expected path: {file_path}")
        sys.exit(os.EX_NOINPUT)

    keys = metadata['signed']['keys']
    if keyid not in keys:
        log.error(f"The provided key ID ({keyid}) was not found in partial root metadata.")
        sys.exit(os.EX_USAGE)

    log.debug(f"Finding role to which the provided key ({keyid}) is assigned.")
    roles = metadata['signed']['roles']
    role = None
    for role_name, role_info in roles.items():
        if keyid in role_info['keyids']:
            role = role_name
            # Technically, a key *could* be assigned to multiple roles. But you shouldn't.
            # @TODO: Consider checking for that scenario here.
            break
    if not role:
        log.error(f"The provided key ID ({keyid}) is not assigned to a role in partial root metadata.")
        sys.exit(os.EX_USAGE)

    log.info(f"The provided key ({keyid}) is assigned to the '{role}' role.")

    if yes:
        log.info('--yes option provided. Bypassing key-removal confirmation prompt.')
    else:
        prompt = f"Remove key with ID {keyid}"
        click.confirm(prompt, default=False, abort=True)

    log.info(f"Proceeding to remove key with ID {keyid}")

    log.debug(f"Removing key ({keyid}) from list of keys in partial Root metadata.")
    del(keys[keyid])
    log.debug(f"Removed key ({keyid}) from list of keys in partial Root metadata.")

    log.debug(f"Removing key ({keyid}) from key IDs for '{role}' role in partial Root metadata.")
    roles[role]['keyids'].remove(keyid)
    log.debug(f"Removed key ({keyid}) from key IDs for '{role}' role in partial Root metadata.")

    log.debug(f"Updating partial metadata expiry.")
    expiry = config['roles']['root']['expiry'].get()
    metadata['signed']['expires'] = seconds_from_now(expiry).strftime('%Y-%m-%dT%H:%M:%SZ')

    write_partial_root_metadata(metadata)
    file_path = get_current_partial_root_metadata_path()
    log.info(f"Updated partial root metadata at: {file_path}")
