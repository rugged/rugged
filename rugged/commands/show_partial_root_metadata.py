import click
import humanize
import json
import os
import sys
from collections import OrderedDict
from datetime import datetime
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.tuf.metadata_files import (
    load_partial_metadata_from_file,
    get_current_partial_root_metadata_path,
)
from rugged.commands.lib.signatures import verify_signature_is_valid_for_key
from rugged.commands.lib.partial_root_metadata import (
    partial_root_metadata_is_valid,
    partial_root_metadata_meets_key_threshold_for_role,
    partial_root_metadata_meets_signature_threshold,
)
from rugged.lib.constants import (
    RUGGED_OUTPUT_COLOUR_RESET as RESET,
    RUGGED_OUTPUT_COLOUR_GREEN as GREEN,
    RUGGED_OUTPUT_COLOUR_YELLOW as YELLOW,
)
from rugged.lib.logger import get_logger
from typing import Dict

log = get_logger()

@click.command("show-partial-root-metadata")
def show_partial_root_metadata_cmd() -> None:
    """ Print information about partial Root metadata. """
    try:
        metadata = load_partial_metadata_from_file('root').to_dict()
        log.info(f"Retrieved partial Root metadata for version {metadata['signed']['version']} ({metadata['signed']['version']}.root.json).")

        _print_metadata(metadata)
        _print_signatures(metadata)
        _print_keys(metadata)
        _print_roles(metadata)
    except FileNotFoundError as e:
        file_path = get_current_partial_root_metadata_path()
        log.error(f"No partial root metadata file exists at the expected path: {file_path}")
        log.info("Consider using the 'initialize-partial-root-metadata' or 'initialize-partial-root-metadata-for-update' command to generate partial root metadata.")
        sys.exit(os.EX_NOINPUT)

def _print_metadata(metadata: dict) -> None:
    """ Print metadata information from partial Root metadata. """
    log.info("=== METADATA ===")
    now = datetime.utcnow().replace(second=59)
    expires = metadata['signed']['expires'].rstrip('Z')
    expiry = datetime.fromisoformat(expires).replace(second=0)
    expires_in = humanize.precisedelta((now - expiry), suppress=['months', 'seconds'], format="%0.0f")
    log.info(f"Expires in {expires_in}")
    if partial_root_metadata_is_valid(metadata):
        valid = f"{GREEN}Metadata is valid for deployment{RESET}"
    else:
        valid = f"{YELLOW}Metadata is not valid for deployment{RESET}"
    log.info(valid)


def _print_signatures(metadata: Dict) -> None:
    """ Print signature information from partial Root metadata. """

    signatures = metadata['signatures']
    signed = metadata['signed']
    roles = signed['roles']
    keys = signed['keys']

    log.info("=== SIGNATURES ===")
    message = f"Signatures: {len(signatures)}"

    if partial_root_metadata_meets_signature_threshold(metadata):
        valid = f" ({GREEN}Meets threshold{RESET})"
    else:
        valid = f" ({YELLOW}Does not meet threshold{RESET})"
    log.info(message + valid)

    if 'root' in roles.keys():
        log.info(f"Threshold: {roles['root']['threshold']}")

    for index, signature in enumerate(signatures):
        keyid = signature['keyid']
        verified = 'INVALID'

        if verify_signature_is_valid_for_key(keys[keyid], signature, signed):
            verified = 'VALID'

        for name, role in roles.items():
            for key_index, role_key in enumerate(role['keyids']):
                if role_key == keyid:
                    log.info(f"Signature {index+1} of { len(signatures) }: signed by {name} {key_index+1}/{ len(role['keyids']) } -- {verified} (keyid: { _short_keyid(keyid) })")


def _print_keys(metadata) -> None:
    """ Print key information from partial Root metadata. """

    roles = OrderedDict(sorted(metadata['signed']['roles'].items()))
    keys = metadata['signed']['keys']

    log.info("=== KEYS ===")
    for name, role in roles.items():
        for keyid, key in keys.items():
            for index, role_keyid in enumerate(role['keyids']):
                if role_keyid == keyid:
                    log.info(f"{name} {index+1}/{ len(role['keyids']) }:")
                    log.info(f"  type: {key['scheme']}")
                    log.info(f"  keyid: { keyid }")


def _print_roles(metadata) -> None:
    """ Print role information from partial Root metadata. """

    roles = metadata['signed']['roles']
    keys = metadata['signed']['keys']

    log.info("=== ROLES ===")
    for name, role in roles.items():
        log.info(f"{name}:")
        log.info(f"  Keys required (to meet signature threshold): {role['threshold']}")
        if partial_root_metadata_meets_key_threshold_for_role(metadata, name):
            valid = f" ({GREEN}Meets threshold{RESET})"
        else:
            valid = f" ({YELLOW}Does not meet threshold{RESET})"
        log.info(f"  Keys provided: { len(role['keyids']) }{valid}")
        log.info(f"  keyids: { _get_role_keyid_string(role['keyids']) }")


def _short_keyid(keyid: str) -> str:
    """ Return a shortened key ID. """

    return f"{keyid[:8]}…"


def _get_role_keyid_string(keyids) -> str:
    """ Return a formatted list of keyids. """

    if len(keyids) < 1:
        return f"No keyids found"
    keyid_list = []
    for keyid in keyids:
        keyid_list.append(_short_keyid(keyid))
    return ", ".join(keyid_list)
