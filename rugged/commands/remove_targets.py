import click
import os
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.tuf.repo import RuggedRepository
from rugged.tuf.targets import remove_targets
from rugged.commands.lib.update_metadata import (
    update_snapshot_metadata,
    update_timestamp_metadata,
)

config = get_config()
log = get_logger()
workers = config['workers'].get()


@click.command("remove-targets")
@click.argument('targets', nargs=-1)
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="(For testing) Remove targets locally, rather than delegating to the targets worker.",
)
def remove_targets_cmd(targets, local) -> None:
    """ Remove targets from a TUF repository. """
    if not targets:   # Click does not appear to support "1 or more arguments"
        log.error("Missing argument '[TARGETS]...'.")
        sys.exit(os.EX_NOINPUT)
    if local:
        result, data = remove_targets(targets)
    else:
        result, data = run_task(workers['targets']['name'], 'remove_targets_task', [targets])
    if result:
        message = "Removed the following targets from the repository:\n"
        message += "\n".join(data['removed_targets'])
        message += "Updated targets metadata."
        log.info(message)
    else:
        log.error("Failed to remove one or more targets from TUF repository.")
        sys.exit("Check the logs for more detailed error reporting.")
    if local:
        repo = RuggedRepository()
        repo.load()
        repo.update_snapshot()
        repo.write_metadata('snapshot')
        repo.update_timestamp()
        repo.write_metadata('timestamp')
    else:
        update_snapshot_metadata()
        update_timestamp_metadata()
