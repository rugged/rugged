import sys
from rugged.commands.lib.update_metadata import (
    update_snapshot_metadata,
    update_timestamp_metadata,
)
from rugged.exceptions.repository_error import RuggedRepositoryError
from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_TUF_WORKERS
from rugged.lib.logger import (
    get_rugged_logger,
    log_exception,
)
from rugged.lib.task_queue import run_task

config = get_config()
log = get_rugged_logger()


def fetch_all_targets_metadata():
    """ Fetch all TUF metadata from targets-workers. """

    worker = 'targets-worker'
    log.info(f"Fetching all metadata from {worker}.")

    metadata = {}
    try:
        result, metadata[worker] = run_task(worker, 'get_all_targets_metadata_task')
        if result:
            log.debug(f"Fetched all metadata from {worker}.")
        else:
            log.error(f"Failed to fetch all metadata from {worker}.")
    except Exception as e:
        log_exception(e)
        log.error(f"Failure during attempt to fetch all metadata from {worker}.")
        sys.exit("Check the logs for more detailed error reporting.")

    return metadata


def fetch_expiring_metadata():
    """ Fetch imminently expiring TUF metadata from workers. """

    log.info("Fetching expiring metadata from workers.")

    expiring_metadata = {}
    for worker in RUGGED_TUF_WORKERS:
        log.debug(f"Fetching expiring metadata from {worker}.")
        try:
            result, metadata = run_task(worker, 'get_expiring_metadata_task')
            if result:
                log.debug(f"Fetched expiring metadata from {worker}.")
            else:
                log.error(f"Failed to fetch expiring metadata from {worker}.")
        except Exception as e:
            log_exception(e)
            log.error(f"Failure during attempt to fetch expiring metadata from {worker}.")
            sys.exit("Check the logs for more detailed error reporting.")
        expiring_metadata[worker] = metadata
    return expiring_metadata


def refresh_metadata(metadata):
    """ Refresh and update the appropriate metadata. """

    if metadata['targets-worker']:
        _refresh_expiring_metadata('targets-worker', metadata)
        update_snapshot_metadata()
        update_timestamp_metadata()
        return

    if metadata['snapshot-worker']:
        _refresh_expiring_metadata('snapshot-worker')
        update_timestamp_metadata()
        return

    if metadata['timestamp-worker']:
        _refresh_expiring_metadata('timestamp-worker')
        return

    log.info("No metadata expiry periods were refreshed.")


def _refresh_expiring_metadata(worker, expiring_metadata={}):
    """ Dispatch a task to have a worker refresh its imminently expiring metadata. """

    log.info(f"Dispatching task to refresh expiring metadata on {worker}.")
    try:
        if expiring_metadata:
            result, message = run_task(worker, 'refresh_expiry_task', [expiring_metadata[worker]])
        else:
            result, message = run_task(worker, 'refresh_expiry_task')
        if result:
            log.info(message)
        else:
            log.error(message)
            sys.exit(f"Check the {worker} logs for more detailed error reporting.")
    except RuggedRepositoryError:
        log.error(f"Failed to refresh expiry period on {worker}.")
        sys.exit("Check the logs for more detailed error reporting.")
