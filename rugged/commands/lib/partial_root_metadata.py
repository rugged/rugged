import json
import os
import sys
from rugged.lib.constants import RUGGED_PARTIAL_METADATA_DIR
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.repo import (
    load_metadata_from_file,
    write_metadata_to_file,
)
from securesystemslib.formats import encode_canonical
from tuf.api.metadata import (
    Metadata,
    Root,
)
from typing import Any, Dict

log = get_logger()

def load_partial_root_metadata(version: str) -> Dict[str, Any]:
    """ Load partial root metadata from disk. """

    file_path = partial_root_metadata_path(version)
    log.debug(f"Checking for expected metadata file at: {file_path}.")
    if not os.path.exists(file_path):
        log.error(f"A metadata file was not found at the expected path: {file_path}")
        sys.exit(os.EX_NOINPUT)

    log.debug(f"Loading partial metadata file from: {file_path}.")
    metadata = load_metadata_from_file("root", file_path)
    log.debug(f"Loaded partial metadata file from: {file_path}.")

    return metadata.to_dict()


def partial_root_metadata_path(version: str) -> str:
    """ Return the path to the partial root metadata. """

    return f"{RUGGED_PARTIAL_METADATA_DIR}/{version}.root.json"


def write_partial_root_metadata(metadata: Dict[str, Any]) -> None:
    """ Write partial root metadata to disk. """

    version = metadata['signed']['version']
    file_path = partial_root_metadata_path(version)
    file_dir = os.path.dirname(file_path)
    if not os.path.exists(file_dir):
        log.info(f"Partial root metadata path not found. Creating: {file_dir}")
        os.mkdir(file_dir)
    write_signable_root_metadata(metadata)
    write_metadata_to_file("root", Metadata[Root].from_dict(metadata), file_path)


def write_signable_root_metadata(metadata: Dict[str, Any]) -> None:
    """ Write signable root metadata to disk. """

    version = metadata['signed']['version']
    file_path = f"{RUGGED_PARTIAL_METADATA_DIR}/signable-{version}.root.json"

    try:
        with open(file_path, "w") as signable_file:
            signed = encode_canonical(metadata['signed'])
            signable_file.write(signed)
        log.debug(f"Wrote signable root metadata to file '{file_path}'.")
    except Exception as e:
        log_exception(e)
        error = f"Failed to write signable root metadata to file '{file_path}'."
        log.error(error)


def partial_root_metadata_is_valid(metadata: Dict[str, Any]) -> bool:
    """ Check whether partial root metadata is valid. """
    if not partial_root_metadata_meets_signature_threshold(metadata):
        return False
    for name, role in metadata['signed']['roles'].items():
        if not partial_root_metadata_meets_key_threshold_for_role(metadata, name):
            return False
    return True


def partial_root_metadata_meets_signature_threshold(metadata: Dict[str, Any]) -> bool:
    """ Check whether partial root metadata has enough signatures. """
    threshold = metadata['signed']['roles']['root']['threshold']
    log.debug(f"Partial root metadata has signature threshold set to {threshold}.")

    signature_count = len(metadata['signatures'])
    log.debug(f"Partial root metadata currently has {signature_count} signatures.")

    return signature_count >= threshold


def partial_root_metadata_meets_key_threshold_for_role(metadata: Dict[str, Any], role: str) -> bool:
    """ Check whether partial root metadata has enough keys for a given role. """
    threshold = metadata['signed']['roles'][role]['threshold']
    log.debug(f"Partial root metadata has signature threshold for the '{role}' role set to {threshold}.")

    key_count = len(metadata['signed']['roles'][role]['keyids'])
    log.debug(f"Partial root metadata currently has {key_count} keys for the '{role}' role.")

    return key_count >= threshold
