import click
from rugged.lib.logger import get_logger
from rugged.lib.semaphores.pause import delete_pause_processing_flag

log = get_logger()


@click.command("resume-processing")
def resume_processing_cmd() -> None:
    """ Resume processing of scheduled tasks on the monitor-worker. """
    log.info("Resuming processing of scheduled tasks.")
    delete_pause_processing_flag()
    log.info("Resumed processing of scheduled tasks.")
