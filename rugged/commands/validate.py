import click
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.lib.validate import (
    validate_metadata,
    validate_keys
)


config = get_config()
log = get_logger()


@click.command("validate")
@click.argument('type', default='all')
def validate_cmd(type) -> None:
    """Validate TUF repository metadata, keys and targets.

    TYPE argument can be: all, metadata, keys. (Default 'all')
    """
    if type in ['metadata', 'all']:
        validate_metadata()
    if type in ['keys', 'all']:
        validate_keys()
