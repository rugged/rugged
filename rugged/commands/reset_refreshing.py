import click
from rugged.lib.logger import get_logger
from rugged.lib.semaphores.refresh import reset_stale_refreshing_dir

log = get_logger()


@click.command("reset-refreshing")
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Force reset of refreshing-expiry semaphore, even if it is not stale.",
)
def reset_refreshing_cmd(force) -> None:
    """ Reset the refreshing-expiry semaphore if it has become stale. """

    if force:
        log.info("Forcing reset of refreshing-expiry semaphore.")
    else:
        log.info("Resetting stale refreshing-expiry semaphore.")
    reset_stale_refreshing_dir(force)
    log.info("Reset stale refreshing-expiry semaphore.")
