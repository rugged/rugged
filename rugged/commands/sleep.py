import click
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task

log = get_logger()


@click.command("sleep", hidden=True)
@click.argument('interval', default=1)
def sleep_cmd(interval) -> None:
    """ Have the test-worker wait several seconds. """

    log.info(f"Sending test-worker to sleep for {interval} seconds...")
    response = run_task(
        'test-worker',
        'sleep',
        [interval],
    )
    log.info(response)
