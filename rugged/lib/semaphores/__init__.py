import os, time
from rugged.lib.config import get_config

config = get_config()


def get_post_to_tuf_path():
    """ Return the path to the 'post-to-tuf' directory. """
    return config["post_to_tuf_path"].get()

def get_post_to_tuf_dir_contents():
    """ Return a list of files and directories in the post-to-tuf directory. """
    return listdir_nohidden(get_post_to_tuf_path())

def listdir_nohidden(path):
    """ List all non-hidden files and directories inside a given directory path. """
    files = []
    for f in os.listdir(path):
        if not f.startswith('.'):
            files.append(f)
    return sorted(files)

def get_age_of_oldest_post_to_tuf_dir_content():
    """ Return the age, in seconds, of the oldest item in the post-to-tuf directory. """
    now = time.time()
    oldest_age = 0
    with os.scandir(get_post_to_tuf_path()) as iterator:
        for item in iterator:
            # Skip hidden files
            if item.name.startswith('.'):
                continue
            item_info = item.stat()
            item_age = now - item_info.st_mtime
            if item_age > oldest_age:
                oldest_age = item_age
    return oldest_age
