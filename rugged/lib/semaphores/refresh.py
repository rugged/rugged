import os
import time
from rugged.exceptions.timeout_error import RuggedTimeoutError
from rugged.lib.config import get_config
from rugged.lib.constants import (
    RUGGED_MONITOR_TUF_REFRESHING_FLAG,
)
from rugged.lib.logger import get_logger
from rugged.lib.semaphores import get_post_to_tuf_path
from time import sleep

config = get_config()
log = get_logger()


def monitor_worker_is_refreshing():
    """ Determine whether the 'refresh-expiry' semaphore exists. """
    flag_path = get_refreshing_flag_path()
    if os.path.exists(flag_path):
        log.debug(f"Found refresh-expiry flag at: {flag_path}")
        log.debug("Detected currently processing scheduled 'refresh-expiry' task.")
        return True
    return False

def get_refreshing_flag_path() -> str:
    """ Return the path to the 'refresh-expiry' task semaphore directory. """
    return os.path.join(get_post_to_tuf_path(), RUGGED_MONITOR_TUF_REFRESHING_FLAG)

def refreshing_flag_is_stale() -> bool:
    """ Determine whether the 'refresh-expiry' task semaphore is stale. """
    # If the flag doesn't exist, then it also isn't stale.
    if not monitor_worker_is_refreshing():
        return False

    thresholds = config["stale_semaphore_age_thresholds"].get()
    now = time.time()
    changed = os.path.getmtime(get_refreshing_flag_path())
    age = now - changed
    return age > thresholds[RUGGED_MONITOR_TUF_REFRESHING_FLAG]

def reset_stale_refreshing_dir(force: bool = False) -> None:
    """ Delete stale refreshing flag. """
    path = get_refreshing_flag_path()
    # If the flag doesn't exist, then it also isn't stale.
    if not monitor_worker_is_refreshing():
        log.warning(f"No refreshing-expiry flag was found at: {path}")
        return
    if not refreshing_flag_is_stale():
        log.info(f"No stale refreshing-expiry flag was found at: {path}")
    if refreshing_flag_is_stale() or force:
        delete_refreshing_flag()

def create_refreshing_flag():
    """ Create 'refresh-expiry' task semaphore directory. """
    flag_path = get_refreshing_flag_path()
    log.debug(f"Creating 'refresh-expiry' task semaphore directory: {flag_path}")
    if not monitor_worker_is_refreshing():
        os.mkdir(flag_path)

def delete_refreshing_flag():
    """ Delete 'refresh-expiry' task semaphore directory. """
    if not monitor_worker_is_refreshing():
        return
    try:
        flag_path = get_refreshing_flag_path()
        log.debug(f"Deleting 'refresh-expiry' task semaphore directory: {flag_path}")
        os.rmdir(flag_path)
    except OSError as e:
        log_exception(e)
        log.debug(f"Error deleting 'refresh-expiry' task semaphore directory: {flag_path}")
        sys.exit("Check the logs for more detailed error reporting.")
    log.info(f"Deleted 'refresh-expiry' task semaphore directory: {flag_path}")

def wait_for_refreshing_task_to_complete(timeout: int = 0):
    """ Detect if there's a scheduled 'refresh-expiry' task processing, and wait for it to complete. """
    if monitor_worker_is_refreshing():
        log.info("Detected currently processing scheduled 'refresh-expiry' task.")
        log.info("Waiting for current processing task to complete.")
        seconds = 0
        while monitor_worker_is_refreshing():
            sleep(1)
            seconds += 1
            if seconds % 5 == 0:
                log.info(f"Waited {seconds} seconds for 'refresh-expiry' task processing to complete.")
            if timeout and seconds >= timeout:
                raise RuggedTimeoutError("Timeout expired waiting for 'refresh-expiry' task processing to complete.")
        log.info("The 'refresh-expiry' task is complete.")
