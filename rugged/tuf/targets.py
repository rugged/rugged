from rugged.lib.config import get_config
from rugged.lib.logger import (
    get_rugged_logger,
)
from rugged.tuf.repo import RuggedRepository
from typing import Any, Dict, Tuple

config = get_config()
log = get_rugged_logger()


def add_targets() -> Tuple[bool, Dict[str, Any]]:
    """ Add targets to a TUF repository. """
    repo = RuggedRepository()
    inbound_targets = repo.get_inbound_targets()
    if config['use_hashed_bins'].get():
        repo.load_metadata_for_hashed_bins_targets(inbound_targets)
    else:
        repo.load()
    added_targets = repo.add_targets()
    failed_targets = set(inbound_targets).difference(set(added_targets))
    if failed_targets:
        result = False
        data = {'failed_targets': list(failed_targets)}
    else:
        if config['use_hashed_bins'].get():
            for added_target in added_targets:
                bin_to_update = repo.find_hash_bin(added_target)
                repo.write_metadata(bin_to_update)
        else:
            repo.write_metadata('targets')
        result = True
        data = {'added_targets': added_targets}
    return (result, data)


def remove_targets(targets) -> Tuple[bool, Dict[str, Any]]:
    """ Remove targets from a TUF repository. """
    repo = RuggedRepository()
    if config['use_hashed_bins'].get():
        repo.load_metadata_for_hashed_bins_targets(targets)
    else:
        repo.load()
    removed_targets = repo.remove_targets(targets)
    failed_targets = set(targets).difference(set(removed_targets))
    if failed_targets:
        result = False
        data = {'failed_targets': list(failed_targets)}
    else:
        if config['use_hashed_bins'].get():
            for removed_target in removed_targets:
                bin_to_update = repo.find_hash_bin(removed_target)
                repo.write_metadata(bin_to_update)
        else:
            repo.write_metadata('targets')
        result = True
        data = {'removed_targets': removed_targets}
    return (result, data)
