from collections import OrderedDict
from datetime import datetime, timedelta, timezone
from functools import lru_cache
from glob import glob
from hashlib import sha256, sha512
from math import ceil
from os import chdir, listdir, makedirs, path, remove, rmdir
from re import sub
from shutil import move
from securesystemslib.signer import SSlibSigner
from securesystemslib.exceptions import StorageError
from typing import Any, Dict, Iterator, List, Tuple

from tuf.api.metadata import (
    SPECIFICATION_VERSION,
    TOP_LEVEL_ROLE_NAMES,
    DelegatedRole,
    Delegations,
    Key,
    Metadata,
    MetaFile,
    Root,
    Role,
    Snapshot,
    TargetFile,
    Targets,
    Timestamp,
)
from tuf.api.serialization.json import (
    JSONSerializer,
    DeserializationError,
    SerializationError,
)

from rugged.exceptions.key_error import RuggedKeyError
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.exceptions.repository_error import RuggedRepositoryError
from rugged.exceptions.storage_error import RuggedStorageError
from rugged.lib.config import get_config
from rugged.lib.constants import (
    RUGGED_KEY_INDEX_DELIMITER,
    RUGGED_MONITOR_TUF_READY_PREFIX,
    RUGGED_MONITOR_TUF_PROCESSING_PREFIX,
)
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.key_manager import KeyManager

config = get_config()
log = get_logger()


class RuggedRepository():
    """
    An opinionated TUF repository using the low-level TUF Metadata API.

    @TODO: Implement support for consistent snapshots. (#100)
    @TODO: Implement support for delegated targets. (#36)
    """

    def __init__(self):
        try:
            self._init_dirs()
            self._init_keys()
            self._init_roles()
            log.debug("Instantiated repository.")
        except Exception as e:
            log_exception(e)
            log.error("Failed to instantiate repository.")
            raise RuggedRepositoryError()

    def initialize(self):
        """ Initialize signed TUF metadata. """
        self.update_root(initializing=True)
        self.update_targets(initializing=True)
        self.update_snapshot(initializing=True)
        self.update_timestamp(initializing=True)

    def load(self):
        """ Load all metadata from storage. """
        try:
            for role_name in TOP_LEVEL_ROLE_NAMES:
                self.load_metadata(role_name)
            if config['use_hashed_bins'].get():
                self.load_hashed_bins_metadata()
        except (RuggedMetadataError, RuggedStorageError) as e:
            log_exception(e)
            error = f"Failed to load '{role_name}' metadata."
            log.error(error)
            raise RuggedMetadataError(error)
        log.debug("Loaded all metadata.")

    def load_hashed_bins_metadata(self):
        """ Load hashed bins metadata. """
        # We want to load from metadata from storage. However, if the
        # configured number of hashed bins has changed, the initialized roles
        # will be incorrect. So we delete those, load 'bins.json', then load
        # the individual 'bin_n' metadata from there.
        self.delete_initial_hashed_bin_roles()
        self.load_metadata('bins')
        current_bins = self.roles['bins'].signed.delegations.roles
        for bin_name, bin_n in current_bins.items():
            self.load_metadata(bin_name)

    def delete_initial_hashed_bin_roles(self):
        """ Delete initial hashed bin roles. """
        current_bins = self.roles['bins'].signed.delegations.roles
        for bin_name, bin_n in current_bins.items():
            self.delete_metadata(bin_name)

    def load_metadata_for_hashed_bins_targets(self, targets):
        """ Load only the metadata required for a given set of targets. """
        role_names = list(self.roles.keys())
        target_hashed_bins = [self.find_hash_bin(target) for target in targets]
        # Remove all hashed bins except those for the targets provided.
        for bin_n_name, bin_n_hash_prefixes in self.generate_hash_bins():
            if bin_n_name not in target_hashed_bins:
                role_names.remove(bin_n_name)
        for role_name in role_names:
            try:
                self.load_metadata(role_name)
            except (RuggedMetadataError, RuggedStorageError) as e:
                log_exception(e)
                error = f"Failed to load '{role_name}' metadata."
                log.error(error)
                raise RuggedMetadataError(error)
        log.debug("Loaded all metadata for targets.")

    def load_metadata(self, role_name: str):
        """ Load a role's metadata from storage. """
        try:
            path = self._get_metadata_path(role_name)
            self.roles[role_name] = load_metadata_from_file(role_name, path)
        except StorageError as e:
            log_exception(e)
            error = f"Error loading '{role_name}' metadata from storage."
            log.error(error)
            raise RuggedStorageError(error)

    def delete_metadata(self, role_name: str):
        """ Delete a role's metadata. """
        log.debug(f"Deleting '{role_name}' role from repository.")
        del(self.roles[role_name])

    def delete_metadata_file(self, role_name: str):
        """ Delete a role's metadata file from storage. """
        path = self._get_metadata_path(role_name)
        log.debug(f"Deleting metadata file: {path}")
        remove(path)

    def _get_metadata_path(self, role_name, new=False):
        """ Determine the path for a given role's metadata file. """
        metadata_dir = config['repo_metadata_path'].get()
        filename = self._get_metadata_filename(role_name, new)
        return path.join(metadata_dir, filename)

    def _get_metadata_filename(self, role_name, new=False):
        """ Determine the filename for a given role's metadata. """
        filename = f"{role_name}.json"
        # Timestamp metadata doesn't use consistent snapshots.
        if role_name == "timestamp":
            return filename
        # We need to special-case root here, since it should always have the
        # version prefix. This is what will allow clients to find and validate
        # the chain of root metadata that links the original (shipped) root to
        # its current version.
        if role_name == 'root' or config['consistent_snapshot'].get():
            if new:
                # Use the version in the role metadata that has already been incremented.
                filename = f"{self.roles[role_name].signed.version}.{filename}"
            else:
                # Otherwise, find the most recent one that exists in storage (eg. on disk).
                filename = self._get_versioned_metadata_filename(filename)
        return filename

    def _get_versioned_metadata_filename(self, filename):
        """ Determine the versioned filename for a given role's metadata. """
        metadata_dir = config['repo_metadata_path'].get()
        chdir(metadata_dir)
        files = glob(f"*.{filename}")
        # Sort numbers naturally. Based on magic from https://stackoverflow.com/a/33159707.
        files.sort(key=lambda f: int(sub(r'\D', '', f)))
        if not files:
            raise FileNotFoundError
        # Return the latest version.
        return files[-1]

    def write(self):
        """ Write all metadata to storage. """
        for role_name in self.roles.keys():
            result = self.write_metadata(role_name)
            if not result:
                return False
        return True

    def write_metadata(self, role_name: str):
        """ Write a role's signed metadata to storage. """
        path = self._get_metadata_path(role_name, new=True)
        metadata = self.roles[role_name]
        return write_metadata_to_file(role_name, metadata, path)

    def _key_index(self, role_name: str, key_name:str):
        """ Return a unique index based on a key's role and name. """
        return f"{role_name}{RUGGED_KEY_INDEX_DELIMITER}{key_name}"

    def _key_name(self, key_index):
        """ Return a key's name based on its index. """
        return key_index.split(RUGGED_KEY_INDEX_DELIMITER)[1]

    def sign_metadata(self, role_name: str):
        """ Sign a role's metadata. """
        # This needs to use KeyManager because the root role may not be defined yet.
        for key_name in KeyManager().find_keys_for_role(role_name):
            key_index = self._key_index(role_name, key_name)
            key = self.keys[key_index]
            try:
                signer = SSlibSigner(key)
                self.roles[role_name].sign(signer, append=True)
            except Exception as e:
                log_exception(e)
                error = f"Failed to sign '{role_name}' metadata with '{key_name}' key."
                log.error(error)
                raise RuggedMetadataError(error)
            log.debug(f"Signed '{role_name}' metadata with '{key_name}' key.")

    def add_targets(self):
        """ Add any inbound targets to the targets metadata. """
        inbound_targets = self.get_inbound_targets()
        added_targets = []
        for inbound_target in inbound_targets:
            self.add_target(inbound_target)
            added_targets.append(inbound_target)
        if added_targets:
            if config['use_hashed_bins'].get():
                hashed_bins_to_update = self.get_bins_for_updated_targets(added_targets)
                for bin_n_name in hashed_bins_to_update:
                    self.update_hashed_bin(bin_n_name)
                # All targets are delegated to hashed bins. As a result,
                # there's no need to update the version of the targets
                # metadata.
            else:
                self.update_targets()
        return added_targets

    def get_bins_for_updated_targets(self, updated_targets):
        """ Return a list of bin_n role names that have recently had targets added or removed. """
        bin_n_names = {}
        for updated_target in updated_targets:
            bin_n_name = self.find_hash_bin(updated_target)
            bin_n_names[bin_n_name] = 1
        return bin_n_names.keys()

    def add_target(self, target):
        """ Add a single target to the targets (or hashed bin) metadata. """
        try:
            moved_target_path = self._move_inbound_target_to_targets_dir(
                target
            )
            target_file_info = TargetFile.from_file(
                target,
                moved_target_path,
            )
            self.add_target_to_metadata(target, target_file_info)
            if config['delete_targets_after_signing'].get():
                self._delete_target_after_signing(moved_target_path)
        except Exception as e:
            log_exception(e)
            warning = f"Failed to add target '{target}' to the repository."
            log.warning(warning)

    def add_target_to_metadata(self, target, target_file_info):
        """ Add a target to the targets (or hashed bin) metadata. """
        targets_role = "targets"
        if config['use_hashed_bins'].get():
            targets_role = self.find_hash_bin(target)
        self.roles[targets_role].signed.targets[target] = target_file_info
        message = f"Added target '{target}' to '{targets_role}' role."
        log.info(message)

    def _delete_target_after_signing(self, target):
        """ Delete a given target file after it has been signed. """
        try:
            remove(target)
            log.debug(f"Deleted '{target}' after signing.")
        except Exception as e:
            log_exception(e)
            log.warning(f"Failed to delete target '{target}' after signing.")

    def get_inbound_targets(self):
        """ Scan the inbound directory for files to add to the repository. """
        inbound_targets_dir = config['inbound_targets_path'].get()
        message = f"Scanning for inbound targets in '{inbound_targets_dir}'"
        log.debug(message)
        chdir(inbound_targets_dir)
        inbound_targets = []
        for inbound_target in glob('**', recursive=True):
            if path.isdir(inbound_target):
                # We only want files, not intermediate directories.
                continue
            if inbound_target.startswith(RUGGED_MONITOR_TUF_PROCESSING_PREFIX):
                # Do not process targets that are still processing.
                continue
            if inbound_target.startswith(RUGGED_MONITOR_TUF_READY_PREFIX):
                # Do not process ready targets either.
                continue
            log.debug(f"Found target: {inbound_target}")
            inbound_targets.append(inbound_target)
        return inbound_targets

    def _move_inbound_target_to_targets_dir(self, inbound_target):
        """ Move an inbound target to the repo targets directory. """
        inbound_targets_path = config['inbound_targets_path'].get()
        inbound_target_path = path.join(inbound_targets_path, inbound_target)
        moved_target_path = path.join(
            config['repo_targets_path'].get(),
            inbound_target,
        )
        try:
            makedirs(path.dirname(moved_target_path), exist_ok=True)
            move(inbound_target_path, moved_target_path)
            message = f"Moved '{inbound_target_path}' to "\
                      f"'{moved_target_path}'"
            log.debug(message)
        except Exception as e:
            log_exception(e)
            warning = f"Failed to move target '{inbound_target}' to the "\
                      "targets directory."
            log.warning(warning)
        self._delete_empty_target_dirs(inbound_targets_path, inbound_target)
        message = f"Moved inbound target '{inbound_target}' to targets "\
                  "directory."
        log.info(message)
        return moved_target_path

    def _delete_empty_target_dirs(self, root_dir, target):
        """ Delete any intermediate (empty) directories for a target path. """
        if root_dir not in target:
            target = path.join(root_dir, target)
        target_dir = path.dirname(target)
        if target_dir == root_dir:
            return   # This target is the root directory, so stop.
        try:
            if listdir(target_dir):
                return   # We're only cleaning up empty directories.
            rmdir(target_dir)
            log.debug(f"Cleaned up empty directory '{target_dir}'.")
        except Exception as e:
            if config['delete_targets_after_signing'].get():
                # It's worth trying to delete the directory, even if this
                # config is set, in case it was configured after initializing
                # the repo. So there may be cruft. But if not, we can ignore
                # the error. See: https://gitlab.com/rugged/rugged/-/issues/179
                return
            log_exception(e)
            warning = f"Failed to clean up empty directory '{target_dir}'."
            log.warning(warning)
        finally:
            # Recurse until we hit the root directory.
            self._delete_empty_target_dirs(root_dir, target_dir)

    def remove_targets(self, targets):
        """ Remove given targets from targets (or hashed bin) metadata. """
        removed_targets = []
        for target in targets:
            if self.remove_target(target):
                removed_targets.append(target)
        if removed_targets:
            if config['use_hashed_bins'].get():
                hashed_bins_to_update = self.get_bins_for_updated_targets(removed_targets)
                for bin_n_name in hashed_bins_to_update:
                    self.update_hashed_bin(bin_n_name)
                # All targets are delegated to hashed bins. As a result,
                # there's no need to update the version of the targets
                # metadata.
            else:
                self.update_targets()
        return removed_targets

    def remove_target(self, target):
        """ Remove a single target from the targets (or hashed bin) metadata. """
        try:
            targets_role = "targets"
            if config['use_hashed_bins'].get():
                targets_role = self.find_hash_bin(target)
            del self.roles[targets_role].signed.targets[target]
            log.info(f"Removed target '{target}' from the '{targets_role}' role.")
            self._delete_removed_target(target)
            return True
        except Exception as e:
            log_exception(e)
            warning = f"Failed to remove target '{target}' from the "\
                      "repository."
            log.warning(warning)
        return False

    def _delete_removed_target(self, removed_target):
        """ Delete the file for the target that we removed from the repo. """
        repo_targets_path = config['repo_targets_path'].get()
        target_file = path.join(repo_targets_path, removed_target)
        try:
            remove(target_file)
            log.info(f"Deleted target file '{target_file}'.")
        except Exception as e:
            if config['delete_targets_after_signing'].get():
                # It's worth trying to delete the file, even if this config is
                # set, in case it was configured after initializing the repo.
                # So there may be cruft. But if not, we can ignore the error.
                # See: https://gitlab.com/rugged/rugged/-/issues/179
                return
            log_exception(e)
            log.warning(f"Failed to delete target file '{target_file}'.")
        finally:
            self._delete_empty_target_dirs(repo_targets_path, removed_target)

    def update_targets(self, initializing=False):
        """ Update targets to account for new targets, or rotated keys. """
        if config['use_hashed_bins'].get():
            self.update_hashed_bins(initializing=initializing)
        if not initializing:
            self.roles["targets"].signed.version += 1
        self.update_metadata_expiry("targets")
        self.roles["targets"].signatures.clear()
        self.sign_metadata("targets")
        log.info("Updated targets metadata.")

    def update_snapshot(self, initializing=False):
        """ Update snapshot to account for changed targets metadata, or rotated keys. """
        self.roles["snapshot"].signed.meta["targets.json"] = self._get_metafile_info("targets")
        if config['use_hashed_bins'].get():
            self.update_hashed_bin_versions_in_snapshot()
        if not initializing:
            self.roles["snapshot"].signed.version += 1
        self.update_metadata_expiry("snapshot")
        self.roles["snapshot"].signatures.clear()
        self.sign_metadata("snapshot")
        log.info("Updated snapshot metadata.")

    def _get_metafile_info(self, role):
        """ Return role metadata information. """
        serializer = _get_serializer()
        serialized_role = serializer.serialize(self.roles[role])
        file_info = MetaFile(
            version=self.roles[role].signed.version,
            length=len(serialized_role),
            # @TODO: Make hashes optional
            hashes={
                # @TODO: Make which hashes to include configurable.
                'sha256': sha256(serialized_role).hexdigest(),
                'sha512': sha512(serialized_role).hexdigest(),
            },
        )
        return file_info

    def update_timestamp(self, initializing=False):
        """ Update timestamp to account for changed snapshot metadata info, or rotated keys. """
        self.roles["timestamp"].signed.snapshot_meta = self._get_metafile_info("snapshot")
        if not initializing:
            self.roles["timestamp"].signed.version += 1
        self.update_metadata_expiry("timestamp")
        self.roles["timestamp"].signatures.clear()
        self.sign_metadata("timestamp")
        log.info("Updated timestamp metadata.")

    def update_root(self, initializing=False):
        """ Update root to account for newly added or removed keys. """
        if initializing:
            try:
                log.debug("Trying to initialize 'root' metadata from disk.")
                self.load_metadata("root")
                log.warn("Initialized 'root' metadata from disk.")
                log.info("If you did not intend to initialize with existing 'root' metadata then delete '1.root.json' and re-run this command.")
            except FileNotFoundError as e:
                log.debug("Did not find 'root' metadata on disk to load.")
                pass
            # If we've loaded signed root metadata from disk, don't overwrite it.
            if self.roles["root"].signatures:
                return
        if not initializing:
            self.roles["root"].signed.version += 1
        self.update_metadata_expiry("root")
        self.roles["root"].signatures.clear()
        self.sign_metadata("root")
        log.info("Updated root metadata.")

    def status(self):
        repo_status = {
            'roles': {},
        }
        if 'targets' in self.roles:
            targets = self.roles['targets'].signed.targets
            repo_status['targets'] = {
                'count': len(targets),
                'size': sum(target.length for target in targets.values()),
            }
        for role_name, role_info in self.roles.items():
            # Ignore hashed bin roles, since the code below assumes we're only
            # showing the status of roles signed by root.
            # @TODO: Figure out a better way to do this. Maybe skip delegated roles?
            # @TODO: Alternatively, figure out how to report the status of hashed bins succinctly.
            if role_name[:4] in ['bins', 'bin_']:
                continue
            repo_status['roles'][role_name] = {
                'signatures': len(role_info.signatures),
                'version': role_info.signed.version,
                'tuf_spec': role_info.signed.spec_version,
                'expires': role_info.signed.expires.replace(tzinfo=timezone.utc).isoformat(),
            }
            if 'root' in self.roles:
                threshold = self.roles['root'].signed.roles[role_name].threshold
                repo_status['roles'][role_name]['threshold'] = threshold
            repo_status['roles'][role_name]['keys'] = {}
            for key_index, key in self.keys.items():
                key_name = self._key_name(key_index)
                if key['keyid'] not in role_info.signatures.keys():
                    continue
                key_types = list(key['keyval'].keys())
                if 'private' in key_types:
                    key_path = KeyManager().get_key_path(key_name, role_name, 'signing')
                else:
                    key_path = KeyManager().get_key_path(key_name, role_name, 'verification')
                repo_status['roles'][role_name]['keys'][key_name] = {
                    'types': key_types,
                    'scheme': key['scheme'],
                    'key_path': key_path
                }
        return repo_status

    def get_keys_by_role(self):
        """ Return a dictionary of roles and their associated keys. """
        keys_by_role = {}
        for role_name, role in self.roles.items():
            keys_by_role[role_name] = []
            for keyid in role.signatures:
                for key_index, key in self.keys.items():
                    key_name = self._key_name(key_index)
                    if key['keyid'] == keyid:
                        keys_by_role[role_name].append(key_name)
        return keys_by_role

    def _init_dirs(self):
        """ Ensure all repository directories exist. """
        dirs = {
            config['repo_metadata_path'].get(): 0o755,
            config['repo_targets_path'].get(): 0o755,
        }
        for dir, mode in dirs.items():
            try:
                makedirs(dir, mode=mode, exist_ok=True)
            except PermissionError as e:
                log_exception(e)
                raise RuggedStorageError

    def _init_keys(self):
        """ Initialize a dictionary of keys. """
        self.keys: Dict[str, Dict[str, Any]] = {}
        for role_name, key_names in KeyManager().find_keys().items():
            for key_name in key_names:
                key_index = self._key_index(role_name, key_name)
                self.keys[key_index] = KeyManager().load_keys(key_name, role_name)

    def _init_roles(self):
        """ Initialize a dictionary of roles. """
        self.roles: Dict[str, Metadata] = {}
        self._init_top_level_roles()

    def _init_top_level_roles(self):
        """ Create all top-level metadata objects. """
        self._init_targets_role()
        self._init_snapshot_role()
        self._init_timestamp_role()
        self._init_root_role()

    def _init_targets_role(self):
        """ Create targets metadata object. """
        expiry = config['roles']['targets']['expiry'].get(float)
        log.debug(f"Setting 'targets' metadata expiry to {expiry}.")
        self.roles["targets"] = Metadata[Targets](
            signed=Targets(
                version=1,
                spec_version=get_spec_version_string(),
                expires=seconds_from_now(expiry),
                targets={},
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'targets' metadata.")
        if config['use_hashed_bins'].get():
            self._init_bin_roles()

    def _init_snapshot_role(self):
        """ Create snapshot metadata object. """
        expiry = config['roles']['snapshot']['expiry'].get(float)
        log.debug(f"Setting 'snapshot' metadata expiry to {expiry}.")
        self.roles["snapshot"] = Metadata[Snapshot](
            Snapshot(
                version=1,
                spec_version=get_spec_version_string(),
                expires=seconds_from_now(expiry),
                meta={"targets.json": MetaFile(version=1)},
            ),
            signatures=OrderedDict(),
        )
        if config['use_hashed_bins'].get():
            self._add_bin_roles_to_snapshot()

        log.debug("Initialized 'snapshot' metadata.")

    def _init_timestamp_role(self):
        """ Create timestamp metadata object. """
        expiry = config['roles']['timestamp']['expiry'].get(float)
        log.debug(f"Setting 'timestamp' metadata expiry to {expiry}.")
        self.roles["timestamp"] = Metadata[Timestamp](
            Timestamp(
                version=1,
                spec_version=get_spec_version_string(),
                expires=seconds_from_now(expiry),
                snapshot_meta=self._get_metafile_info('snapshot'),
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'timestamp' metadata.")

    def _init_root_role(self):
        """ Create root metadata object. """
        try:
            log.debug("Collecting keys for 'root' metadata.")
            keys = self._get_repo_keys_for_root_role()
            log.debug("Collecting roles for 'root' metadata.")
            roles = self._get_repo_roles_for_root_role()
            self.roles["root"] = Metadata[Root](
                signed=Root(
                    version=1,
                    spec_version=get_spec_version_string(),
                    keys=keys,
                    roles=roles,
                    consistent_snapshot=False,
                ),
                signatures=OrderedDict(),
            )
        except ValueError as e:
            log_exception(e)
            error = "Failed to initialize Root metadata during TUF "\
                    "repository initialization."
            raise RuggedMetadataError(error)

        self.update_metadata_expiry("root")
        log.debug("Initialized 'root' metadata.")

    def update_metadata_expiry(self, role_name: str, config_role: str = ''):
        """ Update expiry for a given role. """
        if config_role == '':
            config_role = role_name
        expiry = config['roles'][config_role]['expiry'].get(float)
        log.debug(f"Setting '{role_name}' metadata expiry to {expiry}.")
        self.roles[role_name].signed.expires = seconds_from_now(expiry)

    def _get_repo_keys_for_root_role(self):
        """ Return all known keys, keyed by ID. """
        repo_keys = {}
        for key in self.keys.values():
            try:
                log.debug(f"Loading key with ID: {key['keyid']}")
                repo_keys[key["keyid"]] = Key.from_securesystemslib_key(key)
            except TypeError as e:
                log_exception(e)
                error = "Failed to generate key metadata during TUF "\
                        "repository initialization."
                raise RuggedMetadataError(error)
        if not repo_keys:
            raise RuggedKeyError
        return repo_keys

    def _get_repo_roles_for_root_role(self):
        """ Return all known roles, keyed by ID. """
        repo_roles = {}
        for role_name, role_info in config['roles'].get().items():
            if role_name not in TOP_LEVEL_ROLE_NAMES:
                continue
            role_keys = []
            # This needs to use KeyManager because the root role isn't defined yet.
            keys = KeyManager().find_keys()
            if role_name in keys:
                for key_name in keys[role_name]:
                    key_index = self._key_index(role_name, key_name)
                    role_keys.append(self.keys[key_index]["keyid"])
            else:
                message = f"No keys found for '{role_name}'."
                log.warning(message)
                if role_name != Root.type:
                    raise RuggedKeyError(message)
            try:
                threshold = role_info['threshold']
                log.debug(f"Setting '{role_name}' signature threshold to {threshold}.")
                repo_roles[role_name] = Role(role_keys, threshold=threshold)
            except ValueError as e:
                log_exception(e)
                error = "Failed to generate role metadata during TUF "\
                        "repository initialization."
                raise RuggedMetadataError(error)
        return repo_roles

    ################
    # Key rotation #
    ################

    def rotate_keys(self, keys_to_add, keys_to_remove):
        """ Update keys and re-sign metadata. """
        added_keys = self._add_keys(keys_to_add)
        removed_keys = self._remove_keys(keys_to_remove)
        roles_to_regenerate_metadata = []
        for role, key in added_keys + removed_keys:
            if role not in roles_to_regenerate_metadata:
                roles_to_regenerate_metadata.append(role)
        # Rotating a 'targets' key requires regenerating all role metadata.
        if 'targets' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_targets()
            self.write_metadata('targets')
            self.update_snapshot()
            self.write_metadata('snapshot')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating a 'snapshot' key requires regenerating root, timestamp and snapshot metadata.
        elif 'snapshot' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_snapshot()
            self.write_metadata('snapshot')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating 'timestamp' key requires regenerating root and timestamp metadata.
        elif 'timestamp' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating 'root' key only requires regenerating root metadata.
        elif 'root' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
        return (added_keys, removed_keys)

    def _add_keys(self, keys_to_add):
        """ Generate keys and add them to roles in root metadata. """
        added_keys = []
        for role_name, key_name in keys_to_add:
            KeyManager().generate_keypair(key_name, role_name)
            self._add_key_to_keychain(role_name, key_name)
            self._add_key_to_root(role_name, key_name)
            added_keys.append((role_name, key_name))
        return added_keys

    def _add_key_to_keychain(self, role_name, key_name):
        """ Add a key to the keychain. """
        log.debug(f"Adding '{key_name}' key to keychain.")
        key_index = self._key_index(role_name, key_name)
        self.keys[key_index] = KeyManager().load_keys(key_name, role_name)

    def _add_key_to_root(self, role_name, key_name):
        """ Add a key to a role in root metadata. """
        log.debug(f"Adding '{key_name}' key to '{role_name}' role in root metadata.")
        key_index = self._key_index(role_name, key_name)
        key = Key.from_securesystemslib_key(self.keys[key_index])
        self.roles['root'].signed.add_key(key, role_name)

    def _remove_keys(self, keys_to_remove):
        """ Delete keys and remove them from roles in root metadata. """
        removed_keys = []
        for role_name, key_name in keys_to_remove:
            KeyManager().delete_keypair(key_name, role_name)
            self._remove_key_from_root(role_name, key_name)
            self._remove_key_from_keychain(role_name, key_name)
            removed_keys.append((role_name, key_name))
        return removed_keys

    def _remove_key_from_root(self, role_name, key_name):
        """ Remove a key from a role in root metadata. """
        log.debug(f"Removing '{key_name}' key from '{role_name}' role in root metadata.")
        key_index = self._key_index(role_name, key_name)
        key_id = self.keys[key_index]['keyid']
        self.roles['root'].signed.revoke_key(key_id, role_name)

    def _remove_key_from_keychain(self, role_name, key_name):
        """ Remove a key from the keychain. """
        log.debug(f"Removing '{key_name}' key from the keychain.")
        key_index = self._key_index(role_name, key_name)
        del self.keys[key_index]

    #########################
    # Hashed bin delegation #
    #########################

    # See: https://github.com/theupdateframework/python-tuf/blob/v2.1.0/examples/manual_repo/hashed_bin_delegation.py

    def update_hashed_bins(self, initializing=False):
        """ Update all hashed bin roles to account for rotated keys. """
        for bin_n_name, bin_n_role in self.roles["bins"].signed.delegations.roles.items():
            self.update_hashed_bin(bin_n_name, initializing=initializing)
        self.update_hashed_bin("bins", initializing=initializing)

    def update_hashed_bin(self, bin_name: str, initializing=False):
        """ Update a bin role to account for new or removed targets, or rotated keys. """
        if not initializing:
            new_version = self.roles[bin_name].signed.version + 1
            log.debug(f"Updating hashed bin '{bin_name}' metadata to version '{new_version}'.")
            self.roles[bin_name].signed.version = new_version
        self.update_metadata_expiry(bin_name, "targets")
        self.roles[bin_name].signatures.clear()
        self.sign_bin_metadata(bin_name)
        log.info(f"Updated hashed bins '{bin_name}' metadata.")

    def update_hashed_bin_versions_in_snapshot(self):
        """ Update snapshot metadata to reflect current hashed bin metafile info. """
        for bin_n_name, bin_n_role in self.roles["bins"].signed.delegations.roles.items():
            self.roles["snapshot"].signed.meta[f"{bin_n_name}.json"] = self._get_metafile_info(bin_n_name)
        self.roles["snapshot"].signed.meta["bins.json"] = self._get_metafile_info("bins")

    def sign_bin_metadata(self, bin_name: str):
        """ Sign bin role metadata. """
        key_name = self._hashed_bins_key_name()
        signer = self._hashed_bins_signer(key_name)
        try:
            self.roles[bin_name].sign(signer, append=True)
            log.debug(f"Signed '{bin_name}' metadata with '{key_name}' key.")
        except Exception as e:
            log_exception(e)
            error = f"Failed to sign '{bin_name}' metadata with '{key_name}' key."
            log.error(error)
            raise RuggedMetadataError(error)

    def _hashed_bins_signer(self, key_name) -> SSlibSigner:
        """ Return an SSlibSigner using the key specified by name. """
        key_index = self._key_index(key_name, key_name)
        key = self.keys[key_index]
        return SSlibSigner(key)

    def _hashed_bins_key_name(self) -> str:
        """ Return the name of the key to use for hashed bins. """
        return config['hashed_bins_key_name'].get()

    def _init_bin_roles(self):
        """ Create metadata objects for hashed bins. """
        self._init_bins_role()
        bin_count = 0
        for bin_n_name, bin_n_hash_prefixes in self.generate_hash_bins():
            self._init_bin_n_role(bin_n_name, bin_n_hash_prefixes)
            bin_count += 1
        number_of_bins = self.number_of_bins()
        if bin_count != number_of_bins:
            log.debug(f"Created {bin_count} bins, whereas 'number_of_bins: {number_of_bins}' was configured.")

    def _init_bins_role(self):
        """ Create metadata objects for 'bins' role. """
        # Create preliminary delegating targets role (bins) and add public key for
        # delegated targets (bin_n) to key store.
        expiry = config['roles']['targets']['expiry'].get(float)
        self.roles["bins"] = Metadata[Targets](
            signed=Targets(
                version=1,
                spec_version=get_spec_version_string(),
                expires=seconds_from_now(expiry),
                delegations=Delegations(
                    keys=self._bins_keys(),
                    roles={},
                )
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'bins' metadata.")
        # Update top-level targets role with delegation details for the "bins"
        # delegated targets role.
        self.roles["targets"].signed.delegations = Delegations(
            keys=self._bins_keys(),
            roles={
                "bins": DelegatedRole(
                    name="bins",
                    keyids=self._bins_keyids(),
                    threshold=1,
                    terminating=False,
                    # Delegate all target paths to 'bins'. From there we'll
                    # split it up by path hash prefixes.
                    paths=['*'],
                ),
            },
        )
        log.debug("Added top-level 'bins' delegated role to 'targets' metadata, in order to handle hashed bin "
                  "delegation.")

    def _init_bin_n_role(self, bin_n_name: str, bin_n_hash_prefixes: List[str]):
        """ Create metadata object for 'bin-n' role. """
        # Create delegated targets role (bin_n)
        expiry = config['roles']['targets']['expiry'].get(float)
        self.roles[bin_n_name] = Metadata[Targets](
            signed=Targets(
                version=1,
                spec_version=get_spec_version_string(),
                expires=seconds_from_now(expiry),
            ),
            signatures=OrderedDict(),
        )
        log.debug(f"Initialized '{bin_n_name}' metadata.")
        # Update delegating targets role (bins) with delegation details for this
        # delegated targets role (bin_n).
        self.roles["bins"].signed.delegations.roles[bin_n_name] = DelegatedRole(
            name=bin_n_name,
            keyids=self._bin_n_keyids(),
            threshold=1,
            terminating=True,
            path_hash_prefixes=bin_n_hash_prefixes,
        )
        log.debug("Delegated the following target file hash prefixes to "
                  f"'{bin_n_name}': {', '.join(bin_n_hash_prefixes)}.")

    def _bins_keys(self):
        """ Return the key(s) to use for the 'bins' role. """
        key_name = self._hashed_bins_key_name()
        bin_n_key = Key.from_securesystemslib_key(self.keys[f"{key_name}:{key_name}"])
        return {self.keys[f"{key_name}:{key_name}"]['keyid']: bin_n_key}

    def _bins_keyids(self):
        """ Return the key IDs to use for the 'bins' role. """
        return self._bin_n_keyids()

    def _bin_n_keyids(self):
        """ Return the key IDs to use for a 'bin-n' role. """
        key_name = self._hashed_bins_key_name()
        return [self.keys[f"{key_name}:{key_name}"]['keyid']]

    @lru_cache
    def bin_size(self) -> int:
        """ Return the size of each hashed bin. """
        number_of_bins = self.number_of_bins()
        number_of_prefixes = self.number_of_prefixes()
        bin_size = ceil(number_of_prefixes / number_of_bins)
        log.debug(f"Based on {number_of_bins} hashed bins, the calculated bin size is {bin_size}.")

        return bin_size

    @lru_cache
    def number_of_prefixes(self) -> int:
        """ Return the number of file hash prefixes to use with hashed bins. """
        hash_prefix_length = self.hash_prefix_length()
        number_of_prefixes = 16**hash_prefix_length
        log.debug(f"Based on a bin prefix length of {hash_prefix_length}, the calculated number of prefixes is "
                  f"{number_of_prefixes}.")

        return number_of_prefixes

    @lru_cache
    def hash_prefix_length(self) -> int:
        """ Return the minimum length prefix that will guarantee unique bin names. """
        number_of_bins = self.number_of_bins()
        hash_prefix_length = len(f"{(number_of_bins - 1):x}")
        log.debug(f"Based on {number_of_bins} hashed bins, the calculated bin prefix length is {hash_prefix_length}.")

        return hash_prefix_length

    @lru_cache
    def number_of_bins(self) -> int:
        """ Return the configured number of hashed bins to distribute targets between. """
        number_of_bins = config['number_of_bins'].get(int)
        log.debug(f"Hashed bins is configured to use {number_of_bins} bins.")

        return number_of_bins

    def generate_hash_bins(self) -> Iterator[Tuple[str, List[str]]]:
        """
        Returns generator for bin names and hash prefixes per bin.

        Iterates over the total number of hash prefixes in 'bin size'-steps to
        generate bin names and a list of hash prefixes served by each bin.

        It yields an ordered list of incremental hash bin names (ranges), plus
        the hash prefixes each bin is responsible for, e.g.:

        bin_n_name:  00-07  bin_n_hash_prefixes: 00 01 02 03 04 05 06 07
                     08-0f                       08 09 0a 0b 0c 0d 0e 0f
                     10-17                       10 11 12 13 14 15 16 17
                     ...                         ...
                     f8-ff                       f8 f9 fa fb fc fd fe ff
        """
        number_of_prefixes = self.number_of_prefixes()
        bin_size = self.bin_size()
        for low in range(0, number_of_prefixes, bin_size):
            hash_prefixes = self._get_hash_prefixes_for_bin(low)
            # If the configured number of bins is not a power of 2, the final
            # bin will contain fewer hash prefixes.
            actual_bin_size = len(hash_prefixes)
            high = low + actual_bin_size - 1
            bin_name = self._bin_name(low, high)
            yield bin_name, hash_prefixes

    def _get_hash_prefixes_for_bin(self, low: int) -> List[str]:
        """ Returns the hash prefixes for a given bin. """
        number_of_prefixes = self.number_of_prefixes()
        bin_size = self.bin_size()
        hash_prefix_length = self.hash_prefix_length()
        hash_prefixes = []
        for prefix in range(low, low + bin_size):
            if prefix >= number_of_prefixes:
                continue
            hash_prefixes.append(f"{prefix:0{hash_prefix_length}x}")

        return hash_prefixes

    def _bin_name(self, low: int, high: int) -> str:
        """Generates a bin name according to the hash prefixes the bin serves.
        The name is either a single hash prefix for bin size 1, or a range of hash
        prefixes otherwise. The prefix length is needed to zero-left-pad the
        hex representation of the hash prefix for uniform bin name lengths.
        """
        hash_prefix_length = self.hash_prefix_length()
        if low == high:
            return f"bin_{low:0{hash_prefix_length}x}"

        return f"bin_{low:0{hash_prefix_length}x}-{high:0{hash_prefix_length}x}"

    def _add_bin_roles_to_snapshot(self):
        """ Register bin roles with snapshot metadata. """
        self.roles["snapshot"].signed.meta["bins.json"] = MetaFile(version=1)
        for bin_n_name, bin_n_hash_prefixes in self.generate_hash_bins():
            self.roles["snapshot"].signed.meta[f"{bin_n_name}.json"] = MetaFile(version=1)

    def find_hash_bin(self, path: str) -> str:
        """ Returns the name of the bin for a target file based on its path hash. """
        # Generate hash digest of passed target path and take its prefix, given the
        # global prefix length for the given number of bins.

        hasher = sha256()
        hasher.update(path.encode("utf-8"))
        target_name_hash = hasher.hexdigest()
        log.debug(f"Hash of '{path}': {target_name_hash}")
        hash_prefix_length = self.hash_prefix_length()
        prefix = int(target_name_hash[:hash_prefix_length], 16)
        # Find lower and upper bounds for hash prefix given its numerical value and
        # the the general bin size for the given number of bins.
        bin_size = self.bin_size()
        low = prefix - (prefix % bin_size)
        high = low + bin_size - 1
        bin_name = self._bin_name(low, high)
        return bin_name


def seconds_from_now(seconds: float) -> datetime:
    """Adds 'seconds' to now and returns datetime object w/o microseconds."""
    return datetime.utcnow().replace(microsecond=0) + timedelta(seconds=seconds)

def load_metadata_from_file(role_name: str, path: str):
    """ Load a role's metadata from storage. """
    metadata = Metadata
    try:
        loaded_metadata = metadata.from_file(path)
        message = f"Loaded metadata for '{role_name}' role from "\
                  f"'{path}'."
        log.debug(message)
        return loaded_metadata
    except TypeError as e:
        log_exception(e)
        error = f"Failed to load metadata for '{role_name}' role from "\
                f"'{path}'."
        raise RuggedMetadataError(error)
    except DeserializationError as e:
        log_exception(e)
        error = f"Failed to deserialize data from '{path}'."
        raise RuggedMetadataError(error)

def write_metadata_to_file(role_name: str, metadata: Metadata, path: str):
    """ Write a role's metadata to a given file path. """
    try:
        metadata.to_file(path, serializer=_get_serializer())
    except SerializationError as e:
        log_exception(e)
        error = f"Failed to serialize '{role_name}' metadata."
        log.error(error)
        return False
    except Exception as e:
        log_exception(e)
        error = f"Failed to write '{role_name}' metadata to file '{path}'."
        log.error(error)
        return False
    log.debug(f"Wrote '{role_name}' metadata to file '{path}'.")
    return True

def _get_serializer():
    """ Return a serializer. """
    return JSONSerializer(compact=False, validate=True)

def get_spec_version_string():
    """ Return a stringified specification version. """
    return ".".join(SPECIFICATION_VERSION)
