""" Validator for Rugged TUF repository keys.

In the context of this validation, and "expected key" is a key defined in the
`keys` dictionary in config. An "unexpected key" is any key found on disk that
is *not* defined in config.

"""

from rugged.exceptions.key_error import RuggedKeyError
from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_KEY_INDEX_DELIMITER
from rugged.lib.logger import get_logger
from rugged.tuf.key_manager import KeyManager
from rugged.tuf.repo import RuggedRepository

config = get_config()
log = get_logger()


class KeyValidator():

    def validate_config_keys(self):
        """ Check that all configured keys exist on disk. """
        key_manager = KeyManager()
        for role_name, key_names in config['keys'].get().items():
            for key_name in key_names:
                if not key_manager.load_signing_key(key_name, role_name):
                    error = f"The '{key_name}' signing key configured for the '{role_name}' role was not found "\
                            "in storage."
                    raise RuggedKeyError(error)
                if not key_manager.load_verification_key(key_name, role_name):
                    error = f"The '{key_name}' verification key configured for the '{role_name}' role was not "\
                            "found in storage."
                    raise RuggedKeyError(error)
            log.debug(f"All configured keys for '{role_name}' role exist in storage.")

    def validate_storage_keys(self):
        """ Check that only configured keys exist on disk. """
        for role_name, key_names in KeyManager().find_keys().items():
            for key_name in key_names:
                if key_name not in config['keys'].get()[role_name]:
                    error = f"The '{key_name}' key for the '{role_name}' role found in storage does not exist in "\
                            "configuration."
                    raise RuggedKeyError(error)
            log.debug(f"Only configured keys for '{role_name}' role exist in storage.")

    def validate_repo_keys(self):
        """ Check that keys in the repository match the keys on disk. """
        repo = RuggedRepository()
        repo.load()
        keys_from_root_metadata = repo.roles['root'].signed.keys
        for key_index, key in repo.keys.items():
            key_index_parts = key_index.split(RUGGED_KEY_INDEX_DELIMITER)
            role_name = key_index_parts[0]
            key_name = key_index_parts[1]
            if key['keyid'] not in keys_from_root_metadata.keys():
                raise RuggedKeyError(f"Root metadata is missing '{key_name}' key for '{role_name}' role.")
            del keys_from_root_metadata[key['keyid']]
            log.debug(f"Root metadata contains the '{key_name}' key for the '{role_name}' role.")
        log.debug("Root metadata contains all configured keys.")
        if keys_from_root_metadata:
            for keyid in keys_from_root_metadata.keys():
                log.error(f"Root metadata contains unexpected key with ID: {keyid}")
            raise RuggedKeyError("Root metadata contains an unexpected key.")
        log.debug("Root metadata contains only configured keys.")
