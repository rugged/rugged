from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_MONITOR_WORKER

config = get_config()


def get_workers(workers={}, tuf_only: bool = False):
    """ Return a list of given workers; default to all workers. """
    workers = list(workers)
    # If workers have been specified, then just return that list.
    if workers:
        if tuf_only:
            return _only_tuf_workers(workers)
        return workers
    # if no workers were specified, look them up from config.
    for key, worker in config['workers'].get().items():
        workers.append(worker['name'])
    if tuf_only:
        return _only_tuf_workers(workers)
    return workers


def _only_tuf_workers(workers):
    """ Filter a list of workers to only those that have a TUF role. """
    # This is a list of workers that do not have a TUF role.
    rugged_workers = [
        RUGGED_MONITOR_WORKER,
    ]
    # Filter out any Rugged workers.
    return list(set(workers) - set(rugged_workers))
