import confuse
import os
import shutil
import sys
from rugged.exceptions.timeout_error import RuggedTimeoutError
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.lib.config import get_config
from rugged.lib.constants import (
    RUGGED_MONITOR_TUF_PAUSED_FLAG,
    RUGGED_MONITOR_TUF_PROCESSING_PREFIX,
    RUGGED_MONITOR_TUF_READY_PREFIX,
    RUGGED_MONITOR_TUF_REFRESHING_FLAG,
    RUGGED_MONITOR_WORKER,
    RUGGED_TUF_WORKERS,
)
from rugged.lib.logger import (
    get_rugged_logger,
    set_log_level_from_context,
    log_exception,
)
from rugged.lib.semaphores import (
    listdir_nohidden,
    get_post_to_tuf_dir_contents,
    get_age_of_oldest_post_to_tuf_dir_content,
)
from rugged.lib.semaphores.pause import (
    get_pause_processing_flag_path,
    monitor_worker_is_paused,
    pause_processing_flag_is_stale,
)
from rugged.lib.semaphores.processing import (
    get_processing_dir_path,
    monitor_worker_is_processing,
    processing_dir_is_stale,
    reset_stale_processing_dirs,
    wait_for_processing_task_to_complete,
)
from rugged.lib.semaphores.refresh import (
    create_refreshing_flag,
    delete_refreshing_flag,
    get_refreshing_flag_path,
    monitor_worker_is_refreshing,
    refreshing_flag_is_stale,
    reset_stale_refreshing_dir,
)
from rugged.lib.task_queue import TaskQueue, run_task
from rugged.workers.base_worker import BaseWorker
from typing import Dict, List

config = get_config()
log = get_rugged_logger()
queue = RUGGED_MONITOR_WORKER
worker = TaskQueue().get_task_queue()
workers = config['workers'].get()


class MonitorWorker(BaseWorker):
    """
    Rugged (Celery) worker for monitoring a directory and dispatching `add-targets` tasks.

    N.B. This worker should not have access to any keys.
    """

    @worker.on_after_configure.connect
    def schedule_periodic_tasks(sender, **kwargs):
        """ Schedule periodic tasks for the monitor-worker. """

        period = config["scheduler_scan_period"].get()
        if period > 0:
            log.info(f"Scheduling find-new-targets task to run every {period} seconds.")
            sender.add_periodic_task(period, MonitorWorker.find_new_targets_task)

        period = config["scheduler_refresh_period"].get()
        if period > 0:
            log.info(f"Scheduling refresh-expiry task to run every {period} seconds.")
            delete_refreshing_flag()
            sender.add_periodic_task(period, MonitorWorker.refresh_expiry_task)

        period = config["scheduler_reset_period"].get()
        if period > 0:
            log.info(f"Scheduling reset-semaphores task to run every {period} seconds.")
            sender.add_periodic_task(period, MonitorWorker.reset_semaphores_task)

    @worker.task(name='refresh_expiry_task', queue=queue)
    def refresh_expiry_task(**context):
        """ Task to refresh imminently expiring metadata. """
        if not context:
            context = {
                "log_level": config["scheduler_log_level"].get()
            }
        set_log_level_from_context(context)
        log.debug("Received refresh-expiry task.")

        if monitor_worker_is_paused():
            log.warning("The monitor worker is paused.")
            log.info("Skipping expiry refresh task.")
            return

        if monitor_worker_is_refreshing():
            flag_path = get_refreshing_flag_path()
            log.warning(f"A possibly stale refresh-expiry task semaphore was found at: {flag_path}")
            log.info("Skipping expiry refresh task.")
            return

        create_refreshing_flag()

        wait_for_processing_task_to_complete()

        log.info(f"Fetching expiring metadata.")
        metadata = _fetch_expiring_metadata()
        for tuf_worker, role_list in metadata.items():
            log.info(f"Expiring metadata from {tuf_worker}:")
            if not role_list:
                log.info("  No expiring metadata.")
            for role in role_list:
                log.info(f"  {role}.json")

        _refresh_expiring_metadata(metadata)

        delete_refreshing_flag()

    @worker.task(name='find_new_targets_task', queue=queue)
    def find_new_targets_task(**context):
        """ Task to check for new targets in post-to-tuf directory. """
        if not context:
            context = {
                "log_level": config["scheduler_log_level"].get()
            }
        set_log_level_from_context(context)
        log.debug("Received find-new-targets task.")

        if monitor_worker_is_paused():
            log.warning("The monitor worker is paused.")
            log.info("Skipping find-new-targets task.")
            return

        files = get_post_to_tuf_dir_contents()
        if not files:
            log.debug("No new content found.")
            return

        log.info("New content found in post-to-tuf directory.")

        log.info(f"Count of post-to-tuf content: {len(files)}")

        age = round(get_age_of_oldest_post_to_tuf_dir_content())
        log.info(f"Age of oldest post-to-tuf content: {age} seconds")
        log.debug("List of post-to-tuf content:")
        for file in files:
            log.debug(f"  {file}")

        if monitor_worker_is_refreshing():
            log.info("Monitor worker is refreshing expiry periods. Waiting for next scan to continue.")
            return

        ready_targets = _match_targets(files)
        if not ready_targets:
            log.info("No new targets found.")
            return

        if monitor_worker_is_processing():
            log.info("Monitor worker is already processing targets. Waiting for next scan to continue.")
            return

        log.info("New targets found in post-to-tuf directory:")
        for ready_target in ready_targets:
            log.info(f"  {ready_target}")

        ready_target = ready_targets[0]
        log.info(f"Processing: {ready_target}")

        ready_target_dir = _get_ready_target_dir(ready_target)
        processing_target = _get_processing_target(ready_target)
        processing_target_dir = _get_processing_target_dir(processing_target)
        log.debug(f"Renaming '{ready_target}' to '{processing_target}'.")
        os.rename(ready_target_dir, processing_target_dir)

        log.debug(f"Moving targets for '{processing_target}' from post-to-tuf directory to inbound directory.")
        _stage_target_dir(processing_target)
        log.debug(f"Renaming targets for '{processing_target}' within inbound directory.")
        _rename_target_files(processing_target)
        log.info(f"Dispatching 'add-targets' task for: {processing_target}")
        _dispatch_add_targets(processing_target)
        log.debug("Updating snapshot metadata")
        _dispatch_update_snapshot(processing_target)
        log.debug("Updating timestamp metadata")
        _dispatch_update_timestamp(processing_target)
        try:
            log.debug(f"Removing directory '{processing_target_dir}'")
            shutil.rmtree(processing_target_dir)
        except OSError as e:
            log_exception(e)
            log.error(f"Error removing directory '{processing_target_dir}'")
            sys.exit("Check the logs for more detailed error reporting.")

    @worker.task(name='reset_semaphores_task', queue=queue)
    def reset_semaphores_task(**context):
        """ Task to reset stale semaphores. """
        if not context:
            context = {
                "log_level": config["scheduler_log_level"].get()
            }
        set_log_level_from_context(context)
        log.debug("Received reset-semaphores task.")

        log.debug("Checking for stale processing directories.")
        if processing_dir_is_stale():
            log.warning("Found stale processing directory. Resetting.")
            reset_stale_processing_dirs()

        log.debug("Checking for stale expiry-refreshing flag.")
        if refreshing_flag_is_stale():
            log.warning("Found stale expiry-refreshing flag. Resetting.")
            reset_stale_refreshing_dir()

    @worker.task(name='check_monitor_task', queue=queue)
    def check_monitor_task(**context):
        """ Task to check that the monitor worker has proper filesystem permissions. """
        set_log_level_from_context(context)
        log.info("Received check-monitor task.")

        results = {
            'successes': [],
            'warnings': [],
            'failures': [],
        }

        _check_monitor_filesystem_access(results)
        _check_monitor_status(results)
        _check_monitor_semaphores(results)

        return results

def _check_monitor_filesystem_access(results: Dict[str, List[str]]) -> Dict[str, List[str]]:
    """ Check that the monitor-worker has proper filesystem permissions. """
    for check in _get_monitor_filesystem_checks():
        if os.access(check['path'], _get_access_mode(check['operation'])):
            message = f"Monitor worker has {check['operation']} access to "
            message += f"{check['name']} directory: `{check['path']}`"
            results['successes'].append(message)
            log.info(message)
        else:
            error = f"Monitor worker does not have {check['operation']} access to "
            error += f"{check['name']} directory: `{check['path']}`"
            results['failures'].append(error)
            log.error(error)
    return results

def _check_monitor_status(results: Dict[str, List[str]]) -> Dict[str, List[str]]:
    """ Check for monitor-worker statuses. """
    if monitor_worker_is_paused():
        message = "Monitor worker is paused."
        results['warnings'].append(message)
    if monitor_worker_is_refreshing():
        message = "Monitor worker is refreshing expiry periods."
        results['warnings'].append(message)
    if monitor_worker_is_processing():
        message = "Monitor worker is processing new targets."
        results['warnings'].append(message)
    return results

def _check_monitor_semaphores(results: Dict[str, List[str]]) -> Dict[str, List[str]]:
    """ Check for monitor-worker semaphores staleness. """
    semaphores = {
        RUGGED_MONITOR_TUF_PAUSED_FLAG: {
            'path': get_pause_processing_flag_path(),
            'command': 'resume-processing',
            'exists': monitor_worker_is_paused(),
            'stale': pause_processing_flag_is_stale(),
        },
        RUGGED_MONITOR_TUF_PROCESSING_PREFIX: {
            'path': get_processing_dir_path(),
            'command': 'reset-processing',
            'exists': monitor_worker_is_processing(),
            'stale': processing_dir_is_stale(),
        },
        RUGGED_MONITOR_TUF_REFRESHING_FLAG: {
            'path': get_refreshing_flag_path(),
            'command': 'reset-refreshing-expiry',
            'exists': monitor_worker_is_refreshing(),
            'stale': refreshing_flag_is_stale(),
        },
    }

    thresholds = config["stale_semaphore_age_thresholds"].get()
    for name, info in semaphores.items():
        if not info['exists']:
            continue
        if info['stale']:
            error = f"A semaphore ('{info['path']}') appears stale (more than {thresholds[name]} seconds since last change). "
            log.error(error)
            error += f"Remove it with 'rugged {info['command']}'."
            results['failures'].append(error)

    return results

def _get_ready_target_dir(ready_target):
    post_to_tuf_dir = config["post_to_tuf_path"].get()
    return os.path.join(post_to_tuf_dir, ready_target)

def _get_processing_target(ready_target):
    return ready_target.replace(RUGGED_MONITOR_TUF_READY_PREFIX, RUGGED_MONITOR_TUF_PROCESSING_PREFIX)

def _get_ready_target(processing_target):
    return processing_target.replace(RUGGED_MONITOR_TUF_PROCESSING_PREFIX, RUGGED_MONITOR_TUF_READY_PREFIX)

def _get_processing_target_dir(processing_target):
    post_to_tuf_dir = config["post_to_tuf_path"].get()
    return os.path.join(post_to_tuf_dir, processing_target)

def _get_access_mode(operation: str) -> str:
    """ Return the mode for a filesystem access check. """
    operations = {
        'read': os.R_OK,
        'write': os.W_OK,
        'execute': os.X_OK,
    }
    return operations[operation]

def _get_monitor_filesystem_checks():
    """ Return a list of filesystem checks. """
    return [
        {
            'operation': 'read',
            'name': 'incoming-targets',
            'path': config["inbound_targets_path"].get()
        },
        {
            'operation': 'write',
            'name': 'incoming-targets',
            'path': config["inbound_targets_path"].get()
        },
        {
            'operation': 'read',
            'name': 'post-to-tuf',
            'path': config["post_to_tuf_path"].get()
        },
        {
            'operation': 'write',
            'name': 'post-to-tuf',
            'path': config["post_to_tuf_path"].get()
        },
    ]

def _match_targets(files):
    targets = []
    for file in files:
        if file.startswith(RUGGED_MONITOR_TUF_READY_PREFIX):
            targets.append(file)
    return targets

def _dispatch_add_targets(processing_target):
    """ Dispatch `add-targets` task for the processing_target. """
    worker = workers['targets']['name']
    task = "add_targets_task"
    try:
        result, data = _run_remote_task(worker, task)
        if result:
            message = "Added the following targets to the repository:\n"
            message += "\n".join(data['added_targets'])
            message += "\nUpdated targets metadata."
            log.info(message)
        else:
            log.error("Failed to add one or more targets to TUF repository.")
            sys.exit("Check the logs for more detailed error reporting.")
    except RuggedTimeoutError:
        log.error(f"Timeout while dispatching '{task}' task to '{worker}' for '{processing_target}'.")
        _reset_processing_target(processing_target)

        # Stop further processing.
        sys.exit("Check the logs for more detailed error reporting.")

def _dispatch_update_snapshot(processing_target):
    """ Dispatch `update-snapshot` task for the processing_target. """
    worker = workers['snapshot']['name']
    task = "update_snapshot_task"
    try:
        result, message = _run_remote_task(worker, task)
        if result:
            log.info(message)
        else:
            log.error(message)
            log.error("Failed to update snapshot metadata.")
            sys.exit("Check the logs for more detailed error reporting.")
    except RuggedTimeoutError:
        log.error(f"Timeout while dispatching '{task}' task to '{worker}' for '{processing_target}'.")
        _reset_processing_target(processing_target)

        # Stop further processing.
        sys.exit("Check the logs for more detailed error reporting.")

def _dispatch_update_timestamp(processing_target):
    """ Dispatch `update-timestamp` task for the processing_target. """
    worker = workers['timestamp']['name']
    task = "update_timestamp_task"
    try:
        result, message = _run_remote_task(worker, task)
        if result:
            log.info(message)
        else:
            log.error(message)
            log.error("Failed to update timestamp metadata.")
            sys.exit("Check the logs for more detailed error reporting.")
    except RuggedTimeoutError:
        log.error(f"Timeout while dispatching '{task}' task to '{worker}' for '{processing_target}'.")
        _reset_processing_target(processing_target)

        # Stop further processing.
        sys.exit("Check the logs for more detailed error reporting.")

def _fetch_expiring_metadata():
    """ Fetch imminently expiring TUF metadata from workers. """
    log.info("Fetching expiring metadata from workers.")
    expiring_metadata = {}
    for worker in RUGGED_TUF_WORKERS:
        try:
            expiring_metadata[worker] = _dispatch_fetch_expiring_metadata(worker)
        except Exception as e:
            log_exception(e)
            log.error(f"Failure during attempt to fetch expiring metadata from {worker}.")
            sys.exit("Check the logs for more detailed error reporting.")
    return expiring_metadata

def _dispatch_fetch_expiring_metadata(worker, retries = 0):
    """ Fetch imminently expiring TUF metadata from a given worker. """
    try:
        log.debug(f"Fetching expiring metadata from {worker}.")
        result, metadata = _run_remote_task(worker, 'get_expiring_metadata_task')
        if result:
            log.debug(f"Fetched expiring metadata from {worker}.")
            return metadata
        else:
            error = f"Failed to fetch expiring metadata from {worker}."
            log.error(error)
            # Trigger a retry.
            raise RuntimeError(error)
    except (RuggedTimeoutError, RuntimeError) as e:
        # @TODO: consider making this configurable.
        retry_threshold = 2
        if retries < retry_threshold:
            log.warning(f"Task timed-out when fetching expiring metadata from {worker}.")
            retries += 1
            log.info(f"Retry {retries}/{retry_threshold}: Fetching expiring metadata from {worker}.")
            _dispatch_fetch_expiring_metadata(worker, retries)
        else:
            error = f"Tried to fetch expiring metadata from {worker} {retry_threshold} times, but failed."
            log.error(error)
            raise RuggedMetadataError(error)

def _refresh_expiring_metadata(metadata):
    """ Refresh and update the appropriate metadata. """
    try:
        if metadata['targets-worker']:
            worker = 'targets-worker'
            processing_target = 'refreshing Targets metadata'
            result = _dispatch_refresh_expiring_metadata('targets-worker', metadata['targets-worker'])
            _dispatch_update_snapshot(processing_target)
            _dispatch_update_timestamp(processing_target)
            return result
        if metadata['snapshot-worker']:
            worker = 'snapshot-worker'
            processing_target = 'refreshing Snapshot metadata'
            result = _dispatch_refresh_expiring_metadata('snapshot-worker')
            _dispatch_update_timestamp(processing_target)
            return result
        if metadata['timestamp-worker']:
            worker = 'timestamp-worker'
            result = _dispatch_refresh_expiring_metadata('timestamp-worker')
            return result
        log.info("No metadata expiry periods were refreshed.")
    except Exception as e:
        log_exception(e)
        log.error(f"Failure during attempt to refresh expiring metadata from {worker}.")
        sys.exit("Check the logs for more detailed error reporting.")

def _dispatch_refresh_expiring_metadata(worker, expiring_metadata=[], retries = 0):
    """ Dispatch a task to have a worker refresh its imminently expiring metadata. """
    try:
        log.info(f"Dispatching task to refresh expiring metadata on {worker}.")
        result, message = _run_remote_task(worker, 'refresh_expiry_task', expiring_metadata)
        if result:
            log.info(message)
            return result
        else:
            log.error(message)
            # Trigger a retry.
            raise RuntimeError(message)
    except (RuggedTimeoutError, RuntimeError) as e:
        # @TODO: consider making this configurable.
        retry_threshold = 2
        if retries < retry_threshold:
            log.warning(f"Task timed-out when refreshing expiring metadata from {worker}.")
            retries += 1
            log.info(f"Retry {retries}/{retry_threshold}: Refreshing expiring metadata from {worker}.")
            _dispatch_refresh_expiring_metadata(worker, expiring_metadata, retries)
        else:
            error = f"Tried to refresh expiring metadata from {worker} {retry_threshold} times, but failed."
            log.error(error)
            raise RuggedMetadataError(error)

def _run_remote_task(worker, task, args=[]):
    """ Run a task on a worker, or force a timeout for testing. """
    if _network_instability_resiliency_test_mode_enabled_for_task(task):
        log.debug(f"Diverting '{task}' task from '{worker}' to 'sleep' on test worker, to force a timeout.")
        return run_task('test-worker', 'sleep', [5])
    elif args:
        return run_task(worker, task, [args])
    else:
        return run_task(worker, task)

def _network_instability_resiliency_test_mode_enabled_for_task(task):
    """ Determine whether test mode is enabled for a given task. """
    log.debug(f"Checking test mode for '{task}' task.")
    if not config["monitor_enable_network_instability_resiliency_test_mode"].get(confuse.Optional(False)):
        return False
    post_to_tuf_dir = config["post_to_tuf_path"].get()
    flag_filepath = os.path.join(post_to_tuf_dir, f"{task}.test-flag")
    log.debug(f"Checking for existence of test flag file at '{flag_filepath}'.")
    return os.path.exists(flag_filepath)

def _reset_processing_target(processing_target):
    """ Move processing target back to ready, so that it can be retried. """
    ready_target = _get_ready_target(processing_target)
    log.warning(f"Resetting '{processing_target}' to '{ready_target}'.")

    ready_target_dir = _get_ready_target_dir(ready_target)
    processing_target_dir = _get_processing_target_dir(processing_target)
    try:
        log.debug(f"Renaming '{processing_target_dir}' to '{ready_target_dir}'.")
        os.rename(processing_target_dir, ready_target_dir)
    except OSError as e:
        log_exception(e)
        log.error(f"error renaming '{processing_target_dir}' to '{ready_target_dir}'.")
        sys.exit("Check the logs for more detailed error reporting.")
    log.info(f"Renamed '{processing_target_dir}' to '{ready_target_dir}'.")

def _stage_target_dir(target_dir):
    """ Move target directory contents across filesystem boundary. """
    post_to_tuf_dir = config["post_to_tuf_path"].get()
    incoming_targets_dir = config["inbound_targets_path"].get()
    src_dir = os.path.join(post_to_tuf_dir, target_dir)
    dest_dir = os.path.join(incoming_targets_dir, target_dir)
    try:
        log.debug(f"Creating destination directory: {dest_dir}")
        os.mkdir(dest_dir)
    except OSError as e:
        log_exception(e)
        log.error(f"Error creating destination directory: {dest_dir}")
        sys.exit("Check the logs for more detailed error reporting.")
    for file in listdir_nohidden(src_dir):
        src = os.path.join(src_dir, file)
        dest = os.path.join(dest_dir, file)
        try:
            log.debug(f"Copying '{src}' to '{dest}'.")
            if os.path.isdir(src):
                shutil.copytree(src, dest)
            else:
                shutil.copy(src, dest)
        except OSError as e:
            log_exception(e)
            log.error(f"Error copying '{src}' to '{dest}'.")
            sys.exit("Check the logs for more detailed error reporting.")

def _rename_target_files(target_dir):
    """ Prepare target files for signing. """
    incoming_targets_dir = config["inbound_targets_path"].get()
    processing_dir = os.path.join(incoming_targets_dir, target_dir)
    files = listdir_nohidden(processing_dir)
    for file in files:
        src = os.path.join(processing_dir, file)
        dest = os.path.join(incoming_targets_dir, file)
        try:
            log.debug(f"Renaming '{src}' to '{dest}'.")
            os.rename(src, dest)
        except OSError as e:
            log_exception(e)
            log.error(f"Error renaming '{src}' to '{dest}'.")
            sys.exit("Check the logs for more detailed error reporting.")
    try:
        log.debug(f"Removing directory '{processing_dir}'.")
        shutil.rmtree(processing_dir)
    except OSError as e:
        log_exception(e)
        log.error(f"Error removing directory '{processing_dir}'.")
        sys.exit("Check the logs for more detailed error reporting.")

