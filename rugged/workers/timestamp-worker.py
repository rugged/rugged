from rugged.lib.expiry import (
    get_expiring_metadata,
    refresh_expiry,
)
from rugged.lib.logger import (
    get_rugged_logger,
    set_log_level_from_context,
)
from rugged.lib.task_queue import TaskQueue
from rugged.tuf.repo import RuggedRepository
from rugged.workers.base_worker import BaseWorker

log = get_rugged_logger()
queue = 'timestamp-worker'
worker = TaskQueue().get_task_queue()


class TimestampWorker(BaseWorker):
    """ Rugged (Celery) worker that fulfills the TUF 'timestamp' role. """

    @staticmethod
    @worker.task(name='update_timestamp_task', queue=queue)
    def update_timestamp_task(**context):
        """ Task to update timestamp metadata for a TUF repository. """
        set_log_level_from_context(context)
        log.info("Received update-timestamp task.")
        repo = RuggedRepository()
        repo.load()
        repo.update_timestamp()
        result = repo.write_metadata('timestamp')
        if result:
            message = "Updated timestamp metadata."
            log.info(message)
        else:
            message = "Failed to timestamp snapshot metadata."
            log.error(message)
        return (result, message)

    @worker.task(name='get_expiring_metadata_task', queue=queue)
    def get_expiring_metadata_task(**context):
        """ Task to return a list of imminently expiring metadata. """
        set_log_level_from_context(context)
        log.info("Received get-expiring-metadata task.")
        return get_expiring_metadata('timestamp')

    @worker.task(name='refresh_expiry_task', queue=queue)
    def refresh_expiry_task(**context):
        """ Task to refresh timestamp metadata expiry period. """
        set_log_level_from_context(context)
        log.info("Received refresh-expiry task.")
        return refresh_expiry('timestamp')
