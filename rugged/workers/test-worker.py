from rugged.lib.task_queue import TaskQueue
from rugged.workers.base_worker import BaseWorker
from rugged.lib.logger import get_rugged_logger
from time import sleep

worker = TaskQueue().get_task_queue()
queue = 'test-worker'
log = get_rugged_logger()


class TestWorker(BaseWorker):
    """
    Rugged (Celery) worker for testing of base/common functionality.

    N.B. This worker has access to the targets signing key.

    This worker should not be active in production environments. It is only
    intended for development and testing.
    """

    @staticmethod
    @worker.task(name='sleep', queue=queue)
    def echo(interval, **context):
        """ Dummy task to test task timeouts. """
        log.info(f"Test-worker received 'sleep' task with interval of '{interval}'.")
        sleep(interval)
        return f"Test worker went to sleep for '{interval}' seconds."
