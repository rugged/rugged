OPENSSL_VERSION=3.0.12

echo "Beginning installation of OpenSSL v$OPENSSL_VERSION."

echo "Installing system dependencies."
sudo apt install build-essential checkinstall zlib1g-dev -y

echo "Downloading $OPENSSL_VERSION release of OpenSSL."
cd /usr/local/src/
sudo wget https://github.com/openssl/openssl/releases/download/openssl-$OPENSSL_VERSION/openssl-$OPENSSL_VERSION.tar.gz

echo "Unpacking $OPENSSL_VERSION release of OpenSSL."
sudo tar xvf openssl-$OPENSSL_VERSION.tar.gz

echo "Configuring $OPENSSL_VERSION release of OpenSSL."
cd openssl-$OPENSSL_VERSION
sudo ./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared zlib
sudo make
sudo make test

echo "Installing $OPENSSL_VERSION release of OpenSSL."
sudo make install
cd /etc/ld.so.conf.d/
echo "/usr/local/ssl/lib64" | sudo tee openssl-$OPENSSL_VERSION.conf > /dev/null
sudo ldconfig -v

echo "Verifying $OPENSSL_VERSION release of OpenSSL."
/usr/local/ssl/bin/openssl version -a
