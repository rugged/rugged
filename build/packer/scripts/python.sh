#!/bin/sh -eux

# Install python and related utilities.

export DEBIAN_FRONTEND=noninteractive

apt remove -yqq python2*

apt autoremove -yqq

apt install -yqq \
  python3-apt \
  python3-minimal \
  python3-pip \
  python3-yaml \
  python3-jinja2 \
> /dev/null
update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Allow Pip & Pipenv calls from Ansible.
mkdir ~/.config/pip -p
echo "[install]" > ~/.config/pip/pip.conf
echo "break-system-packages = true" >> ~/.config/pip/pip.conf
