#!/bin/sh -eux

export DEBIAN_FRONTEND=noninteractive
apt-get -yqq update
apt-get -yqq install \
  apache2 \
  mysql-client \
  php-common \
  php-dev \
  php-fpm \
  php-curl \
  php-mbstring \
  php-dom \
  php-mysql \
  php-cli \
  php-gd \
  php-xml \
  php-bcmath \
  php-imap \
  php-json \
  php-opcache \
  php-sqlite3 \
  php-apcu \
  php-zip \
  libapache2-mod-php \
> /dev/null
