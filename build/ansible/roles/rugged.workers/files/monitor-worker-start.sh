#!/bin/bash
set -x
set -o errexit nounset pipefail

# @TODO add healthcheck?

# Ensure the mounted post to TUF repository directory is owned by 'rugged'.
sudo chown rugged:rugged -R /opt/post_to_tuf

# Ensure the mounted "inbox" directory is owned by 'rugged'.
sudo chown rugged:rugged -R /var/rugged/incoming_targets

exec /usr/bin/supervisord -n
