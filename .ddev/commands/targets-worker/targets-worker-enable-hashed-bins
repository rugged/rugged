#!/bin/bash
set -o errexit nounset pipefail

## Description: Enable hashed bins on the targets-worker.
## Usage: targets-worker-enable-hashed-bins
## Example: "ddev targets-worker-enable-hashed-bins"

CURRENT_DIR="$(cd "$(dirname "$0")" && pwd)"
RUGGED_WORKER=targets-worker
RUGGED_CONFIG_PATH=/etc/rugged/config.yaml
RUGGED_CONFIG_BACKUP_PATH=$RUGGED_CONFIG_PATH.bak

# Take a backup, if one doesn't already exist.
if [ ! -f $RUGGED_CONFIG_BACKUP_PATH ]; then
    cp $RUGGED_CONFIG_PATH $RUGGED_CONFIG_BACKUP_PATH;
fi

hashed_bins_are_enabled() {
    grep --quiet "use_hashed_bins: true" /etc/rugged/config.yaml
    return $?
}

if hashed_bins_are_enabled ; then
    echo "Hashed bins are already enabled on $RUGGED_WORKER";
else
    echo "use_hashed_bins: true" >> $RUGGED_CONFIG_PATH;
    $CURRENT_DIR/$RUGGED_WORKER-restart-worker
    echo "Enabled hashed bins on $RUGGED_WORKER";
fi
