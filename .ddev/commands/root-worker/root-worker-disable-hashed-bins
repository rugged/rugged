#!/bin/bash
set -o errexit nounset pipefail

## Description: Disable hashed bins on the root-worker.
## Usage: root-worker-disable-hashed-bins
## Example: "ddev root-worker-disable-hashed-bins"

CURRENT_DIR="$(cd "$(dirname "$0")" && pwd)"
RUGGED_WORKER=root-worker
RUGGED_CONFIG_PATH=/etc/rugged/config.yaml
RUGGED_CONFIG_BACKUP_PATH=$RUGGED_CONFIG_PATH.bak

if [ ! -f $RUGGED_CONFIG_BACKUP_PATH ]; then
    cp $RUGGED_CONFIG_PATH $RUGGED_CONFIG_BACKUP_PATH;
fi

hashed_bins_are_enabled() {
    grep --quiet "use_hashed_bins: true" /etc/rugged/config.yaml
    return $?
}

if hashed_bins_are_enabled; then
    cp $RUGGED_CONFIG_BACKUP_PATH $RUGGED_CONFIG_PATH;
    $CURRENT_DIR/$RUGGED_WORKER-restart-worker;
    echo "Disabled hashed bins on $RUGGED_WORKER";
else
    echo "Hashed bins are already disabled on $RUGGED_WORKER";
fi

