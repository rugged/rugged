.PHONY: dev packaging-pipeline $(RUGGED_WORKERS) redeploy clean-upstream fix-upstream

RUGGED_WORKERS = monitor-worker root-worker snapshot-worker targets-worker test-worker timestamp-worker

# Override the deployed 'Rugged' package with the current code on disk.
# This results in changes to code being reflected immediately on the packaging pipeline.
# However, workers still need to be restarted in order for updated code to be running.
RUGGED_DEPLOY_PACKAGE_CMD = cd /opt/rugged; sudo pip3 install -e . -qq

dev: clean-fixtures
dev: packaging-pipeline
dev: $(RUGGED_WORKERS)
dev: ##@rugged Override the deployed 'Rugged' package with the latest code.

benchmarks:
	@$(behat) --stop-on-failure --profile=benchmarks

update-dependencies:
	@$(ddev) exec "cd /opt/rugged; sudo pipenv update"

packaging-pipeline:
	@$(ECHO) "$(YELLOW)Redeploying code to $@.$(RESET)"
	@$(ddev) exec "$(RUGGED_DEPLOY_PACKAGE_CMD)"

$(RUGGED_WORKERS):
	@$(ECHO) "$(YELLOW)Redeploying code to $@.$(RESET)"
	@$(ddev) -s $@ exec "$(RUGGED_DEPLOY_PACKAGE_CMD)"
	@$(ECHO) "$(YELLOW)Restarting $@.$(RESET)"
	@$(ddev) $@-restart-worker

redeploy:
	@for worker in $(RUGGED_WORKERS); do \
          echo -e "$(YELLOW)Restarting $$worker.$(RESET)" ; \
          $(ddev) $$worker-restart-worker; \
        done

clean-upstream:
	rm composer-integration php-tuf
fix-upstream: composer-integration php-tuf
composer-integration:
	ln -s d9-site/vendor/php-tuf/composer-integration .
	cd composer-integration; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/composer-tuf-integration.git
	cd composer-integration; git remote add github git@github.com:ergonlogic/composer-integration.git || /bin/true
php-tuf:
	ln -s d9-site/vendor/php-tuf/php-tuf .
	cd php-tuf; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/php-tuf-client.git
	cd php-tuf; git remote add github git@github.com:ergonlogic/php-tuf.git || /bin/true

lint:
	$(ddev) pipenv run flake8
	$(ddev) pipenv run bandit --severity-level all --confidence-level all --recursive rugged
	$(ddev) pyre
