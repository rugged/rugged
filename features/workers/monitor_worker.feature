@rugged @workers @monitor-worker
Feature: A worker to dispatch `add-targets` tasks based on monitoring a directory for target files.
  In order to allow a packaging pipeline not to hold credentials for the task queue,
  As a TUF administrator
  I need to ensure that a monitor worker can check a shared directory and dispatch `add-targets` tasks.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
     When I run the Rugged command "rugged echo --worker=monitor-worker --timeout=1"
     Then I should get:
          """
          Sending monitor-worker Ping!...
          Done. Response was: monitor-worker PONG: Ping!
          """

  Scenario: Retrieve worker logs.
    Given I run the Rugged command "rugged echo --worker=monitor-worker --timeout=1"
     When I run the Rugged command "rugged logs --worker=monitor-worker"
     Then I should get:
          """
          INFO (base_worker.echo): monitor-worker received echo task: Ping!
          """

  # @TODO: These have to run from the monitor-worker container. But that would
  # require installing Behat and all it's dependencies. We don't want those on
  # production containers, so perhaps they could be installed during `make
  # dev`.
  # For now, we're just going to disable them.
  @disabled
  Scenario: The worker has proper permissions for `incoming_targets` directory.
    Given I run "ls -la /var/rugged/incoming_targets"
    # Test that a file/directory is not there (via ls, etc.)
    # Create (touch) a test file
    # Run `ls` again, ensure that the test file is there
    # Remove the file
    # Run `ls` again, ensure test file is no longer there

  # @TODO: see above.
  @disabled
  Scenario: The worker has proper permissions for `post_to_tuf` directory.
    Given I run "ls -la /opt/post_to_tuf"
    # Test that a file/directory is not there (via ls, etc.)
    # Create (touch) a test file
    # Run `ls` again, ensure that the test file is there
    # Remove the file
    # Run `ls` again, ensure test file is no longer there

  Scenario: Verify that monitor-worker has proper filesystem permissions.
     When I run the Rugged command "rugged check-monitor"
     Then I should get:
          """
          Monitor worker has read access to incoming-targets directory: `/var/rugged/incoming_targets`
          Monitor worker has write access to incoming-targets directory: `/var/rugged/incoming_targets`
          Monitor worker has read access to post-to-tuf directory: `/opt/post_to_tuf`
          Monitor worker has write access to post-to-tuf directory: `/opt/post_to_tuf`
          """
     When I run the Rugged command "rugged logs --worker=monitor-worker"
     Then I should get:
          """
          INFO (monitor-worker.check_monitor_task): Received check-monitor task.
          INFO (monitor-worker._check_monitor_filesystem_access): Monitor worker has read access to incoming-targets directory: `/var/rugged/incoming_targets`
          INFO (monitor-worker._check_monitor_filesystem_access): Monitor worker has write access to incoming-targets directory: `/var/rugged/incoming_targets`
          INFO (monitor-worker._check_monitor_filesystem_access): Monitor worker has read access to post-to-tuf directory: `/opt/post_to_tuf`
          INFO (monitor-worker._check_monitor_filesystem_access): Monitor worker has write access to post-to-tuf directory: `/opt/post_to_tuf`
          """

  #@TODO: Add a scenario where the the above check fails.
