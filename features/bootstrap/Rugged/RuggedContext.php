<?php
namespace Rugged;

use Consensus\BehatTerminalContext\Context\TerminalContext;

/**
 * Defines application features from the specific context.
 */
class RuggedContext extends TerminalContext {

  const METADATA_PATH = "/var/rugged/tuf_repo/metadata/";

  // An array of expiry timestamps keyed by metadata filename.
  // Used to ensure that timestamps are updated consistently.
  private $metadata_expiry_timestamps = [];

  /**
   * Run a series of steps on the command line as the rugged user.
   */
  protected function runStepsAsRugged($steps) {
    foreach ($steps as $command) {
      $this->succeed('sudo sudo -u rugged ' . $command);
    }
  }

  /**
   * @When I run the Rugged command :command
   */
  public function iRunRuggedCommand($command)
  {
    return $this->iRun('sudo sudo -u rugged ' . $command);
  }

  /**
   * @When I fail to run the Rugged command :command
   */
  public function iFailRuggedCommand($command)
  {
    return $this->iFail('sudo sudo -u rugged ' . $command);
  }

  /**
   * @When I try to run the Rugged command :command
   */
  public function iTryRuggedCommand($command)
  {
    return $this->iTry('sudo sudo -u rugged ' . $command);
  }

  /**
   * @Given I rebuild fixtures
   */
  public function iRebuildFixtures()
  {
    $this->runSteps([
      'make clean-fixtures',
      'make fixtures',
    ]);
  }

  /**
   * @Given I reset Rugged
   */
  public function iResetRugged()
  {
    $this->runSteps([
      'make reset-rugged',
    ]);
  }

  /**
   * @Given I initialize a Rugged repo
   */
  public function iInitializeRuggedRepo()
  {
    $this->runStepsAsRugged([
      'rugged logs --truncate',
      'rugged generate-keys --local',
      'rugged initialize --local',
    ]);
  }

  /**
   * @Given I generate a complete set of Rugged keys
   */
  public function iGenerateCompleteSetOfKeys()
  {
    $this->runStepsAsRugged([
      'openssl genpkey -algorithm ED25519 -out /var/rugged/tuf_repo/tmp/root_private.pem',
      'openssl pkey -in /var/rugged/tuf_repo/tmp/root_private.pem -pubout -out /var/rugged/tuf_repo/tmp/root_public.pem',
      'openssl genpkey -algorithm ED25519 -out /var/rugged/tuf_repo/tmp/root1_private.pem',
      'openssl pkey -in /var/rugged/tuf_repo/tmp/root1_private.pem -pubout -out /var/rugged/tuf_repo/tmp/root1_public.pem',
      'rugged generate-keys --local --role=snapshot',
      'rugged generate-keys --local --role=targets',
      'rugged generate-keys --local --role=timestamp',
    ]);
  }

  /**
   * @Given I generate complete partial root metadata
   */
  public function iGenerateCompletePartialRootMetadata()
  {
    $this->runStepsAsRugged([
      'rugged initialize-partial-root-metadata',
      'rugged add-verification-key root /var/rugged/tuf_repo/tmp/root_public.pem --key-type=pem',
      'rugged add-verification-key root /var/rugged/tuf_repo/tmp/root1_public.pem --key-type=pem',
      'rugged add-verification-key snapshot /var/rugged/verification_keys/snapshot/snapshot.pub',
      'rugged add-verification-key targets /var/rugged/verification_keys/targets/targets.pub',
      'rugged add-verification-key timestamp /var/rugged/verification_keys/timestamp/timestamp.pub',
    ]);
  }

  /**
   * @Given I generate signatures of partial root metadata
   */
  public function iGenerateSignaturesOfPartialRootMetadata()
  {
    $this->runStepsAsRugged([
      'openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root_private.pem -out /var/rugged/tuf_repo/tmp/root_signature.bin',
      'openssl pkeyutl -verify -sigfile /var/rugged/tuf_repo/tmp/root_signature.bin -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -inkey /var/rugged/tuf_repo/tmp/root_public.pem -pubin',
      'openssl pkeyutl -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -sign -inkey /var/rugged/tuf_repo/tmp/root1_private.pem -out /var/rugged/tuf_repo/tmp/root1_signature.bin',
      'openssl pkeyutl -verify -sigfile /var/rugged/tuf_repo/tmp/root1_signature.bin -in /var/rugged/tuf_repo/partial/signable-1.root.json -rawin -inkey /var/rugged/tuf_repo/tmp/root1_public.pem -pubin',
    ]);
  }

  /**
   * @When I time the Rugged command :command
   */
  public function iTimeRuggedCommand($command)
  {
    $start = new \DateTime();
    $this->iRunRuggedCommand($command);
    $end = new \DateTime();
    $interval = $start->diff($end);
    print_r("Rugged command took: " . $interval->format("%H:%I:%S:%F"));
  }

  /**
   * @When I time :command
   */
  public function iTimeCommand($command)
  {
    $start = new \DateTime();
    $this->iRun($command);
    $end = new \DateTime();
    $interval = $start->diff($end);
    print_r("Command took: " . $interval->format("%H:%I:%S:%F"));
  }

  /**
   * @Given I publish TUF Metadata
   */
  public function iPublishTufMetadata()
  {
    $this->runSteps([
      'make publish-tuf-metadata',
    ]);
  }

  /**
   * @Given I register the Rugged repo with Composer
   */
  public function iRegisterRuggedRepoWithComposer()
  {
    $this->runSteps([
      'make register-tuf-repo',
    ]);
  }

  /**
   * @Given I use the :composer_file Composer file
   */
  public function iUseComposerFile($composer_file)
  {
    $this->runSteps([
      "cd d9-site; rm -f composer.lock && ln -sf {$composer_file} composer.json",
    ]);
  }

  /**
   * @Given file :file contains :content
   */
  public function fileContains($file, $content)
  {
    $this->runStepsAsRugged([
      "sh -c \"echo '${content}' > ${file}\"",  // Create the file as 'rugged'
    ]);
  }

  /**
   * @Given the directory :directory exists
   */
  public function directoryExists($directory)
  {
    $this->runStepsAsRugged([
      "mkdir -p ${directory}",  // Create the directory as 'rugged'
    ]);
  }

  /**
   * @Given I wait :seconds seconds
   */
  public function iWaitSeconds($seconds)
  {
    sleep($seconds);
  }

  /**
   * @When I wait (up to :timeout seconds) for the file :filepath to change
   */
  public function iWaitForFileToChange($timeout, $filepath)
  {
    $seconds = 0;
    while ($seconds < $timeout) {
      if ($this->file_hashes[$filepath] !== $this->getFileHash($filepath)) {
        # Give the file another second to finish syncing.
        sleep(1);
        return;
      }
      sleep(1);
      $seconds++;
    }
    throw new \Exception("The file ${filepath} was expected to have changed, but has not changed after waiting ${seconds} seconds.");
  }

  /**
   * @When I wait (up to :timeout seconds) for the file :filepath to contain :text
   */
  public function iWaitForFileToContain($timeout, $filepath, $text)
  {
    $seconds = 0;
    while ($seconds < $timeout) {
      if ($this->fileContainsText($filepath, $text)) {
        return;
      }
      sleep(1);
      $seconds++;
    }
    throw new \Exception("The file ${filepath} was expected to contain '${text}', but it does not after waiting ${seconds} seconds.");
  }

  /**
   * Determine whether a file contains a string.
   */
  protected function fileContainsText($file, $text)
  {
    clearstatcache(true);
    $file = realpath($file);
    $contents = file_get_contents($file);
    return strpos($contents, $text) !== FALSE;
  }

  /**
   * Return the data (as an array) from a given metadata file.
   */
  private function getMetadataFileContents($filename) {
    $file_path = self::METADATA_PATH . $filename;
    $this->succeed("sudo cat ${file_path}");
    return json_decode($this->getOutput(), TRUE);
  }

  /**
   * Return the expiry timestamp from a given metadata file.
   */
  private function getMetadataFileExpiryTimestamp($filename) {
    return $this->getMetadataFileContents($filename)['signed']['expires'];
  }

  /**
   * @Given I record the expiry timestamp from :filename
   */
  public function recordExpiryTimestamp($filename)
  {
    $this->metadata_expiry_timestamps[$filename] = $this->getMetadataFileExpiryTimestamp($filename);
  }

  /**
   * @Then the expiry timestamp from :filename has not changed
   */
  public function ExpiryTimestampHasNotChanged($filename)
  {
    if ($this->metadata_expiry_timestamps[$filename] !== $this->getMetadataFileExpiryTimestamp($filename)) {
      throw new \Exception("The expiry timestamp from ${filename} was not expected to have changed, but has changed.");
    }
  }

  /**
   * @Then the expiry timestamp from :filename has changed
   */
  public function ExpiryTimestampHasChanged($filename)
  {
    if ($this->metadata_expiry_timestamps[$filename] === $this->getMetadataFileExpiryTimestamp($filename)) {
      throw new \Exception("The expiry timestamp from ${filename} was expected to have changed, but has not changed.");
    }
  }

  /**
   * @Then the metadata version in :filename is :version
   */
  public function theMetadataVersionIs($filename, $version)
  {
    $metadata_version = $this->getMetadataFileContents($filename)['signed']['version'];
    if ($metadata_version != $version) {
      throw new \Exception("The expected version of the '${filename}' metadata was ${version}. The actual version in '${filename}' was ${metadata_version}.");
    }
  }

  /**
   * @Then the snapshot metadata version for :filename is :version
   */
  public function theSnapshotMetadataVersionIs($filename, $version)
  {
    $snapshot_metadata = $this->getMetadataFileContents('snapshot.json');
    if (!array_key_exists($filename, $snapshot_metadata['signed']['meta'])) {
      throw new \Exception("The '${filename}' role was not found in snapshot metadata.");
    }
    $metadata_version = $snapshot_metadata['signed']['meta'][$filename]['version'];
    if ($metadata_version != $version) {
      throw new \Exception("The expected version of the '${filename}' snapshot metadata was ${version}. The actual version in 'snapshot.json' was ${metadata_version}.");
    }
  }

}
