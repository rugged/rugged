@rugged @hashed-bins @refresh-expiry @gitlab-155
Feature: Command to refresh metadata expiry periods.
  In order to keep a TUF systems operating and performant,
  As a TUF administrator
  I need to ensure that hashed bin metadata expiry periods can be refreshed.

 #@TODO: Add scenario where no metadata will immimently expire.
 #@TODO: Add scenario for `--force` option.

  Background:
    Given I reset Rugged

  Scenario: Expiring hashed bins metadata gets refreshed.
    Given I run "sudo cp features/fixtures/config/expiry/short_targets_expiry_with_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
     # FYI: We're initializing the repo locally here, so that it will pick up
     #      our local config (deployed in the previous step). Otherwise, we
     #      would have to add additional Behat profiles, and related tooling,
     #      to deploy the config to workers, and then restart them. This cannot
     #      happen from within a running test, since (by design) we don't have
     #      access to restarting services on the workers.
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I record the expiry timestamp from "targets.json"
      And I record the expiry timestamp from "bins.json"
      And I record the expiry timestamp from "bin_0.json"
      And I record the expiry timestamp from "bin_f.json"
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'targets' role is valid.
          Metadata for the 'bins' role is valid.
          Metadata for the 'bin_0' role is valid.
          Metadata for the 'bin_f' role is valid.
          """
    Given I wait "5" seconds
     When I try to run "sudo sudo -u rugged rugged validate"
     Then I should get:
          """
          error: ExpiredMetadataError thrown in validate_targets: targets.json is expired
          error: Metadata for the 'targets' role is not valid.
          """
      And the expiry timestamp from "targets.json" has not changed
      And the expiry timestamp from "bins.json" has not changed
      And the expiry timestamp from "bin_0.json" has not changed
      And the expiry timestamp from "bin_f.json" has not changed
     When I run the Rugged command "rugged --debug refresh-expiry"
     Then I should get:
          """
          Preparing to refresh metadata expiry periods.
          Fetching expiring metadata from workers.
          debug: Fetching expiring metadata from targets-worker.
          debug: Fetched expiring metadata from targets-worker.
          debug: Fetching expiring metadata from snapshot-worker.
          debug: Fetched expiring metadata from snapshot-worker.
          debug: Fetching expiring metadata from timestamp-worker.
          debug: Fetched expiring metadata from timestamp-worker.
          Expiring metadata from targets-worker:
            targets.json
            bins.json
            bin_0.json
            bin_f.json
          Refreshed targets metadata expiry period.
          Refreshed bins metadata expiry period.
          Refreshed bin_0 metadata expiry period.
          Refreshed bin_f metadata expiry period.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     # FYI: While the metadata expiry period has been extended, it will use the
     #      config on the relevant worker, instead of the one we used to
     #      initialize the repo. This should be fine, since all we're concerned
     #      about here is forcing a quick expiration of the metadata, so that
     #      we can show that's it has been refreshed.
     Then the expiry timestamp from "targets.json" has changed
     Then the expiry timestamp from "bins.json" has changed
     Then the expiry timestamp from "bin_0.json" has changed
     Then the expiry timestamp from "bin_f.json" has changed
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'targets' role is valid.
          Metadata for the 'bins' role is valid.
          Metadata for the 'bin_0' role is valid.
          Metadata for the 'bin_f' role is valid.
          """
