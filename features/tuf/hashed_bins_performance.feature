@rugged @cli @hashed-bins
Feature: Hashed bins are reasonably performant.
  In order to speed up TUF repo operations when using hashed bins,
  As a Rugged operator,
  I want to reduce how many hashed bins metadata files are handled.

  Background:
    Given I reset Rugged
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"

  # N.B. These tests require deploying config to the targets-worker.
  #      Run: `make -s enable-hashed-bins` before running these tests.

  @gitlab-141
  Scenario: Only relevant hashed-bin metadata files get re-written when adding targets.
     Then the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "1"
    Given I record the timestamp of "/var/rugged/tuf_repo/metadata/bin_0.json"
      And I record the timestamp of "/var/rugged/tuf_repo/metadata/bin_2.json"
      And I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
      # Wait for network filesystem to sync
      And I wait "2" seconds
     When I run the Rugged command "rugged --debug add-targets"
     Then the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "2"
      And file "/var/rugged/tuf_repo/metadata/bin_0.json" has not been updated
      And file "/var/rugged/tuf_repo/metadata/bin_2.json" has been updated

  @gitlab-141
  Scenario: Only relevant hashed-bin metadata files get re-written when removing targets.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
     When I run the Rugged command "rugged --debug add-targets"
     Then the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "2"
    Given I record the timestamp of "/var/rugged/tuf_repo/metadata/bin_0.json"
      And I record the timestamp of "/var/rugged/tuf_repo/metadata/bin_2.json"
      # Wait for network filesystem to sync
      And I wait "2" seconds
     When I run the Rugged command "rugged --debug remove-targets test0.txt"
     Then the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "3"
      And file "/var/rugged/tuf_repo/metadata/bin_0.json" has not been updated
      And file "/var/rugged/tuf_repo/metadata/bin_2.json" has been updated

  @gitlab-143
  Scenario: Only relevant hashed-bin metadata files get loaded when adding targets.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
      # Wait for network filesystem to sync
      And I wait "2" seconds
      And I run the Rugged command "rugged logs --worker=targets-worker --truncate"
     When I run the Rugged command "rugged --debug add-targets"
      And I run the Rugged command "rugged logs --worker=targets-worker --limit 0"
      And I should get:
          """
          Loaded metadata for 'snapshot' role from '/var/rugged/tuf_repo/metadata/snapshot.json'.
          Loaded metadata for 'timestamp' role from '/var/rugged/tuf_repo/metadata/timestamp.json'.
          Loaded metadata for 'root' role from '/var/rugged/tuf_repo/metadata/1.root.json'.
          Loaded metadata for 'bins' role from '/var/rugged/tuf_repo/metadata/bins.json'.
          Loaded metadata for 'bin_2' role from '/var/rugged/tuf_repo/metadata/bin_2.json'.
          Loaded all metadata for targets.
          """
      And I should not get:
          """
          Loaded metadata for 'bin_0' role from '/var/rugged/tuf_repo/metadata/bin_0.json'.
          Loaded metadata for 'bin_1' role from '/var/rugged/tuf_repo/metadata/bin_1.json'.
          Loaded metadata for 'bin_3' role from '/var/rugged/tuf_repo/metadata/bin_3.json'.
          Loaded metadata for 'bin_4' role from '/var/rugged/tuf_repo/metadata/bin_4.json'.
          Loaded metadata for 'bin_5' role from '/var/rugged/tuf_repo/metadata/bin_5.json'.
          Loaded metadata for 'bin_6' role from '/var/rugged/tuf_repo/metadata/bin_6.json'.
          Loaded metadata for 'bin_7' role from '/var/rugged/tuf_repo/metadata/bin_7.json'.
          Loaded metadata for 'bin_8' role from '/var/rugged/tuf_repo/metadata/bin_8.json'.
          Loaded metadata for 'bin_9' role from '/var/rugged/tuf_repo/metadata/bin_9.json'.
          Loaded metadata for 'bin_a' role from '/var/rugged/tuf_repo/metadata/bin_a.json'.
          Loaded metadata for 'bin_b' role from '/var/rugged/tuf_repo/metadata/bin_b.json'.
          Loaded metadata for 'bin_c' role from '/var/rugged/tuf_repo/metadata/bin_c.json'.
          Loaded metadata for 'bin_d' role from '/var/rugged/tuf_repo/metadata/bin_d.json'.
          Loaded metadata for 'bin_e' role from '/var/rugged/tuf_repo/metadata/bin_e.json'.
          Loaded metadata for 'bin_f' role from '/var/rugged/tuf_repo/metadata/bin_f.json'.
          """
      And the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "2"

  @gitlab-143
  Scenario: Only relevant hashed-bin metadata files get loaded when removing targets.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
      And I run the Rugged command "rugged --debug add-targets"
      # Wait for network filesystem to sync
      And I wait "2" seconds
      And I run the Rugged command "rugged logs --worker=targets-worker --truncate"
     When I run the Rugged command "rugged --debug remove-targets test0.txt"
      And I run the Rugged command "rugged logs --worker=targets-worker --limit 0"
      And I should get:
          """
          Loaded metadata for 'snapshot' role from '/var/rugged/tuf_repo/metadata/snapshot.json'.
          Loaded metadata for 'timestamp' role from '/var/rugged/tuf_repo/metadata/timestamp.json'.
          Loaded metadata for 'root' role from '/var/rugged/tuf_repo/metadata/1.root.json'.
          Loaded metadata for 'bins' role from '/var/rugged/tuf_repo/metadata/bins.json'.
          Loaded metadata for 'bin_2' role from '/var/rugged/tuf_repo/metadata/bin_2.json'.
          Loaded all metadata for targets.
          """
      And I should not get:
          """
          Loaded metadata for 'bin_0' role from '/var/rugged/tuf_repo/metadata/bin_0.json'.
          Loaded metadata for 'bin_1' role from '/var/rugged/tuf_repo/metadata/bin_1.json'.
          Loaded metadata for 'bin_3' role from '/var/rugged/tuf_repo/metadata/bin_3.json'.
          Loaded metadata for 'bin_4' role from '/var/rugged/tuf_repo/metadata/bin_4.json'.
          Loaded metadata for 'bin_5' role from '/var/rugged/tuf_repo/metadata/bin_5.json'.
          Loaded metadata for 'bin_6' role from '/var/rugged/tuf_repo/metadata/bin_6.json'.
          Loaded metadata for 'bin_7' role from '/var/rugged/tuf_repo/metadata/bin_7.json'.
          Loaded metadata for 'bin_8' role from '/var/rugged/tuf_repo/metadata/bin_8.json'.
          Loaded metadata for 'bin_9' role from '/var/rugged/tuf_repo/metadata/bin_9.json'.
          Loaded metadata for 'bin_a' role from '/var/rugged/tuf_repo/metadata/bin_a.json'.
          Loaded metadata for 'bin_b' role from '/var/rugged/tuf_repo/metadata/bin_b.json'.
          Loaded metadata for 'bin_c' role from '/var/rugged/tuf_repo/metadata/bin_c.json'.
          Loaded metadata for 'bin_d' role from '/var/rugged/tuf_repo/metadata/bin_d.json'.
          Loaded metadata for 'bin_e' role from '/var/rugged/tuf_repo/metadata/bin_e.json'.
          Loaded metadata for 'bin_f' role from '/var/rugged/tuf_repo/metadata/bin_f.json'.
          """
	  And the metadata version in "bin_0.json" is "1"
      And the metadata version in "bin_2.json" is "3"
