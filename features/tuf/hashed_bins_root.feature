@rugged @cli @hashed-bins @gitlab-99
Feature: Hashed bins using the root-worker.
  In order to reduce the size of generated metadata files,
  As a Rugged operator,
  I need to be able to configure Rugged to use hashed bins.

  Background:
    Given I reset Rugged

  # This scenario would require changing config on the root-worker, then
  # restarting it.  This cannot be done from inside the packaging-pipeline
  # (by design).
  # If we ever want to try implementing it, look at the corresponding scenario
  # in `hashed_bins.feature`
#  Scenario: Hashed bins are not used by default.

  Scenario: Hashed bins can be enabled on a new repo.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys"
     When I run the Rugged command "rugged --debug initialize"
      And I run the Rugged command "rugged logs --worker=root-worker --limit=0"
     Then I should get:
       """
       Initialized 'bins' metadata.
       Added top-level 'bins' delegated role to 'targets' metadata, in order to handle hashed bin delegation.
       Hashed bins is configured to use 16 bins.
       Based on 16 hashed bins, the calculated bin prefix length is 1.
       Based on a bin prefix length of 1, the calculated number of prefixes is 16.
       Based on 16 hashed bins, the calculated bin size is 1.
       Initialized 'bin_0' metadata.
       Delegated the following target file hash prefixes to 'bin_0': 0.
       Initialized 'bin_1' metadata.
       Delegated the following target file hash prefixes to 'bin_1': 1.
       Initialized 'bin_f' metadata.
       Delegated the following target file hash prefixes to 'bin_f': f.
       """
      And the following files should exist:
       """
       /var/rugged/tuf_repo/metadata/bins.json
       /var/rugged/tuf_repo/metadata/bin_0.json
       /var/rugged/tuf_repo/metadata/bin_1.json
       /var/rugged/tuf_repo/metadata/bin_f.json
       """
     When I run "ls /var/rugged/tuf_repo/metadata/bin_*.json | wc -w"
     Then I should get:
       """
       16
       """
      And the file "/var/rugged/tuf_repo/metadata/targets.json" contains:
       """
       "signed": {
        "_type": "targets",
        "delegations": {
         "keys": {
         },
         "roles": [
          {
           "keyids": [
           ],
           "name": "bins",
           "paths": [
            "*"
           ],
           "terminating": false,
           "threshold": 1
          }
         ]
        },
        "version": 1
       }
       """
      And the file "/var/rugged/tuf_repo/metadata/bins.json" contains:
       """
       "signatures": [
       "signed": {
       "_type": "targets",
       "delegations": {
       "keys": {
       "roles": [
       "keyids": [
       "name": "bin_0",
       "path_hash_prefixes": [
       "0"
       "terminating": true,
       "threshold": 1
       """
      And the file "/var/rugged/tuf_repo/metadata/bins.json" does not contain:
       """
       "signatures": [],
       """
      And the file "/var/rugged/tuf_repo/metadata/bin_0.json" contains:
       """
       "signatures": [
       "signed": {
       "_type": "targets",
       "targets": {},
       """
      And the file "/var/rugged/tuf_repo/metadata/bin_0.json" does not contain:
       """
       "signatures": [],
       """
      And the file "/var/rugged/tuf_repo/metadata/snapshot.json" contains:
       """
       "bins.json": {
       "bin_0.json": {
       "bin_1.json": {
       "bin_f.json": {
       """

  # This scenario would require changing config on the root-worker, then
  # restarting it.  This cannot be done from inside the packaging-pipeline
  # (by design).
  # If we ever want to try implementing it, look at the corresponding scenario
  # in `hashed_bins.feature`
#  Scenario: The number of hashed bins can be configured on a new repo.

  # This scenario would require changing config on the root-worker, then
  # restarting it.  This cannot be done from inside the packaging-pipeline
  # (by design).
  # If we ever want to try implementing it, look at the corresponding scenario
  # in `hashed_bins.feature`
#  Scenario: An unusual number of hashed bins can be configured on a new repo.

  Scenario: Hashed bins role metadata can be validated.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys"
      And I run the Rugged command "rugged --debug initialize"
     When I run the Rugged command "rugged --debug validate"
     Then I should not get:
       """
       error: UnsignedMetadataError thrown in validate_hashed_bins: bins was signed by 0/1 keys
       RuggedMetadataError: Failed to validate hashed-bins roles' metadata.
       error: Metadata for the 'bins' role is not valid.
       """
      And I should get:
       """
       Metadata for the 'targets' role is valid.
       Metadata for the 'bins' role is valid.
       Metadata for the 'bin_0' role is valid.
       Metadata for the 'bin_1' role is valid.
       Metadata for the 'bin_f' role is valid.
       """

  Scenario: The 'targets' key is used by default to sign hashed bins metadata.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys"
     When I run the Rugged command "rugged --debug initialize"
      And I run the Rugged command "rugged logs --worker=root-worker --limit=0"
     Then I should get:
       """
       Signed 'bins' metadata with 'targets' key.
       Signed 'bin_0' metadata with 'targets' key.
       Signed 'bin_1' metadata with 'targets' key.
       Signed 'bin_f' metadata with 'targets' key.
       """

  # This scenario would require changing config on the root-worker, then
  # restarting it.  This cannot be done from inside the packaging-pipeline
  # (by design).
  # If we ever want to try implementing it, look at the corresponding scenario
  # in `hashed_bins.feature`
  #Scenario: A configured key can be used to sign hashed bins metadata.

  # @TODO: this will require deploying custom config to the targets-worker.
  #        See: https://gitlab.com/rugged/rugged/-/issues/136
  # @TODO: This currently requires manual setup, and so will fail under normal
  # circumstances.

  Scenario Outline: Target files end up in the proper hashed bin metadata.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
     When I run the Rugged command "rugged --debug initialize --local"
     Then the metadata version in "<bin_name>.json" is "1"
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" contains:
       """
       "targets": {},
       """
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
     When I run the Rugged command "rugged --debug add-targets"
      And I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should get:
       """
       DEBUG (repo.find_hash_bin): Hash of '<filename>': <hash>
       INFO (repo.add_target_to_metadata): Added target '<filename>' to '<bin_name>' role.
       DEBUG (repo.update_hashed_bin): Updating hashed bin '<bin_name>' metadata to version '2'.
       """
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" does not contain:
       """
       "targets": {},
       """
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" contains:
       """
       "targets": {
       "<filename>"
       """
     Examples:
       | filename  | contents | bin_name | hash |
       | test0.txt | test0    | bin_2    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
       | test2.txt | test2    | bin_1    | 13e29841e34bc141f8f31eead2f50ce21fd3d09dcf406776d844f6e65b80527c |
       | test3.txt | test3    | bin_3    | 36e6fe047847ce478368a46cc151c5762ff461fddd68e2c2bb0250c499be3e97 |
       | test4.txt | test4    | bin_2    | 28abdea4bd1be487ef90bc9fba5667abea260d2009ca8ae5fd788757ded9ce39 |

  Scenario Outline: Target files are removed from the proper hashed bin metadata.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I am in the "/var/rugged/incoming_targets" directory
      And file "<filename>" contains "<contents>"
      And I run the Rugged command "rugged add-targets"
      And I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" contains:
       """
       "targets": {
       "<filename>"
       """
     When I run the Rugged command "rugged --debug remove-targets <filename>"
      And I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should get:
       """
       DEBUG (repo.find_hash_bin): Hash of '<filename>': <hash>
       INFO (repo.remove_target): Removed target '<filename>' from the '<bin_name>' role.
       DEBUG (repo.update_hashed_bin): Updating hashed bin '<bin_name>' metadata to version '3'.
       DEBUG (repo.sign_bin_metadata): Signed '<bin_name>' metadata with 'targets' key.
       INFO (repo.update_hashed_bin): Updated hashed bins '<bin_name>' metadata.
       """
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" should contain:
       """
       "targets": {},
       """
      And the file "/var/rugged/tuf_repo/metadata/<bin_name>.json" does not contain:
       """
       "<filename>"
       """
     Examples:
       | filename  | contents | bin_name | hash |
       | test0.txt | test0    | bin_2    | 23d5e8cadf33c45706d0572fd8b966388e18f5218848ba15cc4c7891c40e26c3 |
       | test1.txt | test1    | bin_5    | 53387ed7b30d46708e40d0022cd0c0c0c8c812d77236c45e5c29a312e92ad848 |
       | test2.txt | test2    | bin_1    | 13e29841e34bc141f8f31eead2f50ce21fd3d09dcf406776d844f6e65b80527c |
       | test3.txt | test3    | bin_3    | 36e6fe047847ce478368a46cc151c5762ff461fddd68e2c2bb0250c499be3e97 |
       | test4.txt | test4    | bin_2    | 28abdea4bd1be487ef90bc9fba5667abea260d2009ca8ae5fd788757ded9ce39 |

  Scenario: Status command works with hashed bins enabled.
    Given I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys"
     When I run the Rugged command "rugged initialize"
      And I run the Rugged command "rugged status"

## (Optional) Future enhancements

  @wip
  Scenario: Hashed bins can be enabled on an existing repo.

  @wip
  Scenario: Hashed bins can be disabled on an existing repo.

  @wip
  Scenario: Hashed bins count can be changed on an existing repo.
