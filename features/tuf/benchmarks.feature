@rugged @cli @hashed-bins @benchmarks @gitlab-143
Feature: Hashed bins are reasonably performant.
  In order to speed up TUF repo operations when using hashed bins,
  As a Rugged operator,
  I want to reduce how many hashed bins metadata files are handled.

  Background:
    Given I reset Rugged

  Scenario Outline: Benchmark loading hashed-bin metadata files when adding targets.
    Given I run "sudo cp features/fixtures/config/benchmarks/<BINS>_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I time the Rugged command "rugged initialize --local"
     Examples:
       | BINS | 
       | 16   |
       | 32   |
       | 64   |
       | 128  |
       | 256  |
       | 512  |
       | 1024 |
       | 2048 |
       | 4096 |

  Scenario Outline: Benchmark loading hashed-bin metadata files when adding targets.
    Given I run "sudo cp features/fixtures/config/benchmarks/<BINS>_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged initialize --local"
      And I time "for i in $(seq -f '%03g' 0 99); do sudo fallocate -l 1 /var/rugged/incoming_targets/bar$i.zip ; sudo sudo -u rugged rugged add-targets --local ; done;"
   	 Examples:
       | BINS | 
	   | 16   |
	   | 32   |
	   | 64   |
	   | 128  |
	   | 256  |
	   | 512  |
	   | 1024 |
       | 2048 |
       | 4096 |
 
