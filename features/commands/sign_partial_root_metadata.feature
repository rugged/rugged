@rugged @command @local @sign-partial-root-metadata @gitlab-160
Feature: Commands to generate signable root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to sign signable root metadata.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/tmp"

  Scenario: Sign root metadata.
  Scenario: Add signature to root metadata.
  Scenario: Initialize repository using externally-signed root metadata.
