@rugged @command @generate-keys @local
Feature: Command to generate keys for Rugged TUF locally.
  In order to sign and validate TUF metadata
  As an administrator
  I need to generate encryption keypairs locally.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged ls /var/rugged/signing_keys/targets"
      And I should not get:
          """
          targets
          """
      And I run "sudo sudo -u rugged ls /var/rugged/verification_keys"
      And I should not get:
          """
          targets.pub
          """

  Scenario: The 'generate-keys' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged generate-keys --help"
     Then I should not get:
          """
          Error: No such command 'generate-keys'.
          """
      And I should get:
          """
          Usage: rugged generate-keys [OPTIONS]

            Generate a keypair for verification and signing of TUF metadata.

          Options:
            --role TEXT  The specific role for which to generate a keypair. Can be
                         passed multiple times.
            --local      Generate keys locally, rather than delegating to the root
                         worker.
            --force      Overwrite existing keys.
            --help       Show this message and exit.
          """

  Scenario: Generate keypair locally.
     When I run the Rugged command "rugged --debug generate-keys --local --role targets"
     Then I should get:
          """
          Generating 'targets' keypair for 'targets' role.
          debug: Generating keypair at /tmp/
          debug: Copying signing key to /var/rugged/signing_keys/targets/targets.
          debug: Copying verification key to /var/rugged/verification_keys/targets/targets.pub.
          Signing key:       /var/rugged/signing_keys/targets/targets
          Verification key:  /var/rugged/verification_keys/targets/targets.pub
          """
     When I try to run "sudo sudo -u nobody ls /var/rugged/signing_keys"
     Then I should get:
          """
          Permission denied
          """
     When I run "sudo ls /var/rugged/signing_keys"
     Then I should get:
          """
          targets
          """
     When I try to run "sudo sudo -u nobody ls /var/rugged/verification_keys/targets"
     Then I should get:
          """
          Permission denied
          """
     When I run "sudo ls /var/rugged/verification_keys/targets"
     Then I should get:
          """
          targets.pub
          """

  Scenario: Don't inadvertently overwrite existing keys locally.
    Given I run the Rugged command "rugged generate-keys --local --role targets"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
     When I fail to run the Rugged command "rugged generate-keys --local --role targets"
     Then I should get:
          """
          warning: verification key already exists at '/var/rugged/verification_keys/targets/targets.pub'.
          error: Key (targets) already exists for role 'targets'.
          error: Use --force to overwrite it.
          """
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
     When I run the Rugged command "rugged generate-keys --local --role targets --force"
     Then I should get:
          """
          warning: verification key already exists at '/var/rugged/verification_keys/targets/targets.pub'.
          Overwriting existing keys since --force flag option was provided.
          Generating 'targets' keypair for 'targets' role.
          """
      And file "/var/rugged/verification_keys/targets/targets.pub" has changed

  Scenario: Generate keypairs locally for all roles by default.
     When I run the Rugged command "rugged generate-keys --local"
     Then I should get:
          """
          Generating 'root' keypair for 'root' role.
          Generating 'root1' keypair for 'root' role.
          Generating 'snapshot' keypair for 'snapshot' role.
          Generating 'targets' keypair for 'targets' role.
          Generating 'timestamp' keypair for 'timestamp' role.
          """
     When I run "sudo ls /var/rugged/signing_keys/root"
     Then I should get:
          """
          root
          root1
          """
     When I run "sudo ls /var/rugged/verification_keys/root"
     Then I should get:
          """
          root.pub
          root1.pub
          """

  Scenario: Provide useful error and debugging messages when key generation fails locally.
    Given I run "sudo chmod 000 /var/rugged/signing_keys/targets"
      And I run "sudo chmod 000 /var/rugged/verification_keys/targets"
     When I fail to run the Rugged command "rugged --debug generate-keys --local --role targets"
     Then I should not get:
          """
          debug: Moving signing key to /var/rugged/signing_keys/targets.
          debug: Moving verification key to /var/rugged/verification_keys/targets.pub.
          Signing key:       /var/rugged/signing_keys/targets
          Verification key:  /var/rugged/verification_keys/targets.pub
          """
      And I should get:
          """
          Generating 'targets' keypair for 'targets' role.
          debug: Generating keypair at /tmp/
          debug: Copying signing key to /var/rugged/signing_keys/targets/targets.
          error: PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/signing_keys/targets/targets'
          debug: Copying verification key to /var/rugged/verification_keys/targets/targets.pub.
          error: PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/verification_keys/targets/targets.pub'
          Failed to generate key pair. Check the logs for more detailed error reporting.
          Signing key:       Failed to generate signing key.
          Verification key:  Failed to generate verification key.
          """
     When I run the Rugged command "rugged logs --local"
     Then I should get:
          """
          INFO (generate_keys.generate_keys_cmd): Generating 'targets' keypair for 'targets' role.
          DEBUG (key_manager.generate_keypair): Generating keypair at /tmp/
          DEBUG (key_manager._copy_key): Copying signing key to /var/rugged/signing_keys/targets/targets.
          ERROR (logger.log_exception): PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/signing_keys/targets/targets'
          DEBUG (key_manager._copy_key): Copying verification key to /var/rugged/verification_keys/targets/targets.pub.
          ERROR (logger.log_exception): PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/verification_keys/targets/targets.pub'
          ERROR (generate_keys.generate_keys_cmd): Failed to generate key pair. Check the logs for more detailed error reporting.
          """
