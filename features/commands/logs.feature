@rugged @command @logs @root-worker @snapshot-worker @targets-worker @test-worker @timestamp-worker
Feature: Command to print Rugged logs.
  In order to monitor Rugged operations
  As an administrator
  I need to read Rugged's logs.

  Background:
    Given I reset Rugged

  Scenario: The 'logs' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged logs --help"
     Then I should not get:
          """
          Error: No such command 'logs'.
          """
      And I should get:
          """
          Usage: rugged logs [OPTIONS]

            Print the logs for a TUF repository.

          Options:
            --local          Print the local logs. Defaults to true, unless a worker is
                             specified.
            --worker TEXT    The specific worker from which to retrieve logs. Can be
                             passed multiple times.
            --limit INTEGER  The number of lines to print for each log.
            --truncate       Instead of printing logs, empty log file(s).
            --help           Show this message and exit.
          """

  Scenario: Print local logs.
     When I run the Rugged command "rugged --debug echo"
     When I run the Rugged command "rugged --debug logs --local"
     Then I should get:
          """
          === Log for local operations: /var/log/rugged/rugged.log ===
          DEBUG (cli.rugged_cli): rugged_cli invoked
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """

  Scenario: Print only the last line from local logs.
     When I run the Rugged command "rugged echo"
     When I run the Rugged command "rugged logs --local --limit 1"
     Then I should get:
          """
          === Log for local operations: /var/log/rugged/rugged.log ===
          INFO (echo.echo_cmd): Done. Response was: timestamp-worker PONG: Ping!
          """
     Then I should not get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          """

  Scenario: Print a worker's logs.
     When I run the Rugged command "rugged echo"
     When I run the Rugged command "rugged --debug logs --worker=test-worker"
     Then I should not get:
          """
          === Log for local operations: /var/log/rugged/rugged.log ===
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """
     Then I should get:
          """
          === Log for test-worker: /var/log/rugged/rugged.log ===
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Print multiple workers' logs.
     When I run the Rugged command "rugged echo"
     When I run the Rugged command "rugged --debug logs --worker=test-worker --worker=root-worker"
     Then I should get:
          """
          === Log for test-worker: /var/log/rugged/rugged.log ===
          INFO (base_worker.echo): test-worker received echo task: Ping!
          === Log for root-worker: /var/log/rugged/rugged.log ===
          INFO (base_worker.echo): root-worker received echo task: Ping!
          """
     Then I should not get:
          """
          === Log for snapshot-worker: /var/log/rugged/rugged.log ===
          === Log for targets-worker: /var/log/rugged/rugged.log ===
          === Log for timestamp: /var/log/rugged/rugged.log ===
          """

  Scenario: Truncate local logs.
     When I run the Rugged command "rugged echo"
     When I run the Rugged command "rugged --debug logs --local --truncate"
     Then I should get:
          """
          Truncated local operations log at: /var/log/rugged/rugged.log
          """
     When I run the Rugged command "rugged --debug logs --local"
     Then I should get:
          """
          === Log for local operations: /var/log/rugged/rugged.log ===
          """
     Then I should not get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """

  Scenario: Truncate worker logs.
    Given I run the Rugged command "rugged echo"
     When I run the Rugged command "rugged --debug logs --worker=test-worker --truncate"
     Then I should get:
          """
          Truncated test-worker log at: /var/log/rugged/rugged.log
          """
     When I run the Rugged command "rugged --debug logs --worker=test-worker"
     Then I should get:
          """
          === Log for test-worker: /var/log/rugged/rugged.log ===
          """
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """
