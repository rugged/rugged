@rugged @command @config @root-worker @snapshot-worker @targets-worker @test-worker @timestamp-worker
Feature: Command to print Rugged config.
  In order to ensure that Rugged is properly configured
  As an administrator
  I need to read the configuration of Rugged's components.

  Background:
    Given I reset Rugged

  Scenario: The 'config' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged config --help"
     Then I should not get:
          """
          Error: No such command 'config'.
          """
      And I should get:
          """
          Usage: rugged config [OPTIONS]

            Print the config for a TUF repository.

          Options:
            --local        Print the local config. Defaults to true, unless a worker is
                           specified.
            --worker TEXT  The specific worker from which to retrieve config. Can be
                           passed multiple times.
            --help         Show this message and exit.
          """

  Scenario: Print config.
     When I run the Rugged command "rugged config"
     Then I should get:
          """
          === Configuration for local operations ===
          broker_connection_string
          REDACTED
          log_file
          /var/log/rugged/rugged.log
          log_format
          %(asctime)s %(levelname)s (%(module)s.%(funcName)s): %(message)s
          consistent_snapshot
          False
          === Configuration for root-worker ===
          === Configuration for snapshot-worker ===
          === Configuration for targets-worker ===
          === Configuration for test-worker ===
          === Configuration for timestamp-worker ===
          """

  Scenario: Print local config.
     When I run the Rugged command "rugged config --local"
     Then I should get:
          """
          === Configuration for local operations ===
          """
     Then I should not get:
          """
          === Configuration for root-worker ===
          === Configuration for snapshot-worker ===
          === Configuration for targets-worker ===
          === Configuration for test-worker ===
          === Configuration for timestamp-worker ===
          """

  Scenario: Print a worker's config.
     When I run the Rugged command "rugged config --worker=test-worker"
     Then I should get:
          """
          === Configuration for test-worker ===
          """
     Then I should not get:
          """
          === Configuration for local operations ===
          === Configuration for root-worker ===
          === Configuration for snapshot-worker ===
          === Configuration for targets-worker ===
          === Configuration for timestamp-worker ===
          """

  Scenario: Print multiple workers' configs.
     When I run the Rugged command "rugged config --worker=test-worker --worker=targets-worker"
     Then I should get:
          """
          === Configuration for test-worker ===
          === Configuration for targets-worker ===
          """
     Then I should not get:
          """
          === Configuration for local operations ===
          === Configuration for root-worker ===
          === Configuration for snapshot-worker ===
          === Configuration for timestamp-worker ===
          """
