@rugged @command @generate-keys @root-worker
Feature: Command to generate keys for Rugged TUF on the root worker.
  In order to sign and validate TUF metadata
  As an administrator
  I need to generate encryption keypairs on the root worker.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged ls /var/rugged/signing_keys/targets"
      And I should not get:
          """
          targets
          """
      And I run "sudo sudo -u rugged ls /var/rugged/verification_keys/targets"
      And I should not get:
          """
          targets.pub
          """

  Scenario: Generate keypair on the root worker.
     When I run the Rugged command "rugged --debug generate-keys --role targets"
     Then I should get:
          """
          Generating 'targets' keypair for 'targets' role.
          Signing key:       /var/rugged/signing_keys/targets/targets
          Verification key:  /var/rugged/verification_keys/targets/targets.pub
          """
     When I try to run "sudo sudo -u nobody ls /var/rugged/signing_keys"
     Then I should get:
          """
          Permission denied
          """
     When I run "sudo ls /var/rugged/signing_keys/targets"
     Then I should get:
          """
          targets
          """
     When I try to run "sudo sudo -u nobody ls /var/rugged/verification_keys/targets"
     Then I should get:
          """
          Permission denied
          """
     When I run "sudo ls /var/rugged/verification_keys/targets"
     Then I should get:
          """
          targets.pub
          """

  Scenario: Don't inadvertently overwrite existing keys on the root worker.
    Given I run the Rugged command "rugged generate-keys --role targets"
      And I record a reference hash of "/var/rugged/verification_keys/targets/targets.pub"
     When I fail to run the Rugged command "rugged generate-keys --role targets"
     Then I should get:
          """
          warning: verification key already exists at '/var/rugged/verification_keys/targets/targets.pub'.
          error: Key (targets) already exists for role 'targets'.
          error: Use --force to overwrite it.
          """
      And file "/var/rugged/verification_keys/targets/targets.pub" has not changed
     When I run the Rugged command "rugged generate-keys --role targets --force"
     Then I should get:
          """
          warning: verification key already exists at '/var/rugged/verification_keys/targets/targets.pub'.
          Overwriting existing keys since --force flag option was provided.
          Generating 'targets' keypair for 'targets' role.
          """
      And file "/var/rugged/verification_keys/targets/targets.pub" has changed

  Scenario: Generate keypairs for all roles by default on the root worker.
     When I run the Rugged command "rugged generate-keys"
     Then I should get:
          """
          Generating 'root' keypair for 'root' role.
          Generating 'root1' keypair for 'root' role.
          Generating 'snapshot' keypair for 'snapshot' role.
          Generating 'targets' keypair for 'targets' role.
          Generating 'timestamp' keypair for 'timestamp' role.
          """
     When I run "sudo ls /var/rugged/signing_keys/root"
     Then I should get:
          """
          root
          root1
          """
     When I run "sudo ls /var/rugged/verification_keys/root"
     Then I should get:
          """
          root.pub
          root1.pub
          """

  Scenario: Provide useful error and debugging messages when key generation fails on the root worker.
    Given I run "sudo chmod 000 /var/rugged/signing_keys/targets"
      And I run "sudo chmod 000 /var/rugged/verification_keys/targets"
     When I fail to run the Rugged command "rugged --debug generate-keys --role targets"
     Then I should not get:
          """
          Signing key:       /var/rugged/signing_keys/targets/targets
          Verification key:  /var/rugged/verification_keys/targets/targets.pub
          """
      And I should get:
          """
          Generating 'targets' keypair for 'targets' role.
          Signing key:       Failed to generate signing key.
          Verification key:  Failed to generate verification key.
          """
     When I run the Rugged command "rugged logs --worker=root-worker"
     Then I should get:
          """
          DEBUG (root-worker.generate_keypair_task): Received 'generate_keypair' task.
          INFO (root-worker.generate_keypair_task): Generating 'targets' keypair for 'targets' role.
          DEBUG (key_manager.generate_keypair): Generating keypair at /tmp/
          DEBUG (key_manager._copy_key): Copying signing key to /var/rugged/signing_keys/targets/targets.
          ERROR (logger.log_exception): PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/signing_keys/targets/targets'
          DEBUG (key_manager._copy_key): Copying verification key to /var/rugged/verification_keys/targets/targets.pub.
          ERROR (logger.log_exception): PermissionError thrown in _copy_key: [Errno 13] Permission denied: '/var/rugged/verification_keys/targets/targets.pub'
          """
