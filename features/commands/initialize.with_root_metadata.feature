@rugged @command @initialize @local @root-metadata @gitlab-161
Feature: Command to initialize Rugged TUF repository w/ existing root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to initialize a TUF repo with existing root metadata.

  Scenario: Initialize repository using existing root metadata.
    Given I reset Rugged
          # Copy fixture keys into place
      And I run "sudo sudo -u rugged cp -r features/fixtures/hsm_support/verification_keys/* /var/rugged/verification_keys/"
      And I run "sudo sudo -u rugged cp -r features/fixtures/hsm_support/signing_keys/* /var/rugged/signing_keys/"
          # Copy fixture root metadata into place
      And I run "sudo sudo -u rugged cp -r features/fixtures/hsm_support/tuf_repo/* /var/rugged/tuf_repo/"
          # Fix permissions
      And I run "sudo chmod -R 600 /var/rugged/signing_keys/snapshot/snapshot"
      And I run "sudo chmod -R 600 /var/rugged/signing_keys/targets/targets"
      And I run "sudo chmod -R 600 /var/rugged/signing_keys/timestamp/timestamp"
          # Take a hash of the root metadata file
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/1.root.json"
          # Initialize the repo
     When I run the Rugged command "rugged --debug initialize --local"
          # Check for expected output.
      And I should get:
          """
          debug: Loaded 'root' verification key for 'root' role.
          debug: Loaded 'root1' verification key for 'root' role.
          debug: Trying to initialize 'root' metadata from disk.
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/metadata/1.root.json'.
          warning: Initialized 'root' metadata from disk.
          If you did not intend to initialize with existing 'root' metadata then delete '1.root.json' and re-run this command.
          """
      And I should not get:
          """
          debug: Loaded 'root' signing key for 'root' role.
          debug: Loaded 'root1' signing key for 'root' role.
          debug: Did not find 'root' metadata on disk to load.
          """
          # Check that root metadata hasn't changed
     Then file "/var/rugged/tuf_repo/metadata/1.root.json" has not changed
          # Check that a new root metadata file has not been created
      And the file "/var/rugged/tuf_repo/metadata/2.root.json" does not exist
          # Validate that the repo is valid
     When I run the Rugged command "rugged validate metadata"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
     And I should not get:
          """
          error: Metadata for the 'root' role is not valid.
          error: Metadata for the 'timestamp' role is not valid.
          error: Metadata for the 'snapshot' role is not valid.
          error: Metadata for the 'targets' role is not valid.
          """
