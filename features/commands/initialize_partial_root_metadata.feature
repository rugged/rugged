@rugged @command @local @initialize-partial-root-metadata @gitlab-160
Feature: Initialize partial root metadata for HSM signing.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to initialize signed root metadata.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/partial /var/rugged/tuf_repo/tmp"
      And the file "/var/rugged/tuf_repo/partial/1.root.json" does not exist
      And the file "/var/rugged/tuf_repo/partial/signable-1.root.json" does not exist

  Scenario: The 'initialize-partial-root-metadata' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged initialize-partial-root-metadata --help"
     Then I should not get:
          """
          Error: No such command 'initialize-partial-root-metadata'.
          """
      And I should get:
          """
          Usage: rugged initialize-partial-root-metadata [OPTIONS]

            Initialize a new Root metadata.

          Options:
            --help  Show this message and exit.
          """

  Scenario: Do not overwrite existing partial root metadata.
    Given I run "sudo sudo -u rugged touch /var/rugged/tuf_repo/partial/1.root.json"
     When I fail to run the Rugged command "rugged --debug initialize-partial-root-metadata"
     Then I should get:
          """
          debug: Checking for existing file at: /var/rugged/tuf_repo/partial/1.root.json
          error: A file already exists at the desired path: /var/rugged/tuf_repo/partial/1.root.json
          """

  Scenario: Initialize empty root metadata.
     When I run the Rugged command "rugged --debug initialize-partial-root-metadata"
     Then I should get:
          """
          Initializing partial root metadata at: /var/rugged/tuf_repo/partial/1.root.json
          debug: Setting 'root' signature threshold to 1.
          debug: Setting 'timestamp' signature threshold to 1.
          debug: Setting 'snapshot' signature threshold to 1.
          debug: Setting 'targets' signature threshold to 1.
          debug: Wrote 'root' metadata to file '/var/rugged/tuf_repo/partial/1.root.json'.
          """
      And the file "/var/rugged/tuf_repo/partial/1.root.json" exists
      And the file "/var/rugged/tuf_repo/partial/signable-1.root.json" exists
      And the file "/var/rugged/tuf_repo/partial/signable-1.root.json" does not contain:
          """
          signatures
          signed
          """
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          """
      And I should not get:
          """
          signed by:
          root (ed25519)
          """
