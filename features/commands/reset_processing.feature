@rugged @command @reset-processing @gitlab-216
Feature: Command to reset processing of "stuck" add-targets tasks.
  In order to recover from certain failed operations
  As an administrator
  I need to remove stale semaphores and perform other clean-up/reset tasks.

  Background:
    Given I reset Rugged
      And the "/opt/post_to_tuf/tuf_paused" directory should not exist

  Scenario: The 'reset-processing' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged reset-processing --help"
     Then I should not get:
          """
          Error: No such command 'reset-processing'.
          """
      And I should get:
          """
          Usage: rugged reset-processing [OPTIONS]

            Reset stale add-targets processing directories.

          Options:
            --force  Force reset of any processing directories, even if they are not
                     stale.
            --help   Show this message and exit.
          """

  Scenario: Report if there are no stale processing directories to reset.
     When I run the Rugged command "rugged reset-processing"
     Then I should get:
          """
          Resetting stale processing directories to ready state.
          No stale processing directories were found at: /opt/post_to_tuf/tuf_processing_
          Reset any stale processing directories to ready state.
          """

  Scenario: Reset only stale processing directories by default.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_processing_TEST"
     When I run the Rugged command "rugged --debug reset-processing"
     Then I should get:
          """
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TEST
          No stale processing directories were found at: /opt/post_to_tuf/tuf_processing_
          """
    Given I run the Rugged command "touch -d yesterday /opt/post_to_tuf/tuf_processing_TEST"
     When I run the Rugged command "rugged --debug reset-processing"
     Then I should get:
          """
          Resetting stale processing directories to ready state.
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TEST
          warning: Resetting processing directory ('/opt/post_to_tuf/tuf_processing_TEST') to ready state.
          debug: Renaming '/opt/post_to_tuf/tuf_processing_TEST' to '/opt/post_to_tuf/tuf_ready_TEST'.
          Renamed '/opt/post_to_tuf/tuf_processing_TEST' to '/opt/post_to_tuf/tuf_ready_TEST'.
          Reset any stale processing directories to ready state.
          """
      And I should not get:
          """
          No stale processing directories were found at: /opt/post_to_tuf/tuf_processing_
          """
      And the "/opt/post_to_tuf/tuf_processing_TEST" directory should not exist
      And the "/opt/post_to_tuf/tuf_ready_TEST" directory should exist

  Scenario: Force reset of all processing directories.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_processing_TEST"
     When I run the Rugged command "rugged --debug reset-processing --force"
     Then I should get:
          """
          Forcing reset of all processing directories to ready state.
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TEST
          Renamed '/opt/post_to_tuf/tuf_processing_TEST' to '/opt/post_to_tuf/tuf_ready_TEST'.
          Reset any stale processing directories to ready state.
          """
      And I should not get:
          """
          No stale processing directories were found at: /opt/post_to_tuf/tuf_processing_
          """
      And the "/opt/post_to_tuf/tuf_processing_TEST" directory should not exist
      And the "/opt/post_to_tuf/tuf_ready_TEST" directory should exist

  Scenario: The reset-processing Rugged command cleans up multiple processing directories.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_processing_TEST1"
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_processing_TEST2"
     When I run the Rugged command "rugged --debug reset-processing --force"
     Then I should get:
          """
          Forcing reset of all processing directories to ready state.
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TEST1
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TEST2
          Renamed '/opt/post_to_tuf/tuf_processing_TEST1' to '/opt/post_to_tuf/tuf_ready_TEST1'.
          Renamed '/opt/post_to_tuf/tuf_processing_TEST2' to '/opt/post_to_tuf/tuf_ready_TEST2'.
          Reset any stale processing directories to ready state.
          """
      And I should not get:
          """
          No stale processing directories were found at: /opt/post_to_tuf/tuf_processing_
          """
      And the "/opt/post_to_tuf/tuf_processing_TEST1" directory should not exist
      And the "/opt/post_to_tuf/tuf_ready_TEST1" directory should exist
      And the "/opt/post_to_tuf/tuf_processing_TEST2" directory should not exist
      And the "/opt/post_to_tuf/tuf_ready_TEST2" directory should exist
