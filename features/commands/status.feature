@rugged @command @status @test-worker
Feature: Command to print repo status.
  In order to ensure that the repo is properly configured and working as expected
  As an administrator
  I need to print the status of the repo from the perspective of each of the workers.

  Background:
    Given I rebuild fixtures

  Scenario: The 'status' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged status --help"
     Then I should not get:
          """
          Error: No such command 'status'.
          """
      And I should get:
          """
          Usage: rugged status [OPTIONS]

            Print a status message for a TUF repository.

          Options:
            --local        Print the local repo status. Defaults to true, unless a
                           worker is specified.
            --worker TEXT  The specific worker from which to retrieve repo status. Can
                           be passed multiple times.
            --help         Show this message and exit.
          """

  Scenario: Print status of a non-functional repo.
    Given I reset Rugged
     When I fail to run the Rugged command "rugged --debug status"
     Then I should get:
          """
          debug: Collecting keys for 'root' metadata.
          debug: RuggedKeyError: Error loading TUF keys.
          error: RuggedKeyError thrown in __init__: Error loading TUF keys.
          error: Failed to instantiate repository.
          """

  Scenario: Print local status.
     When I run the Rugged command "rugged status --local"
     Then I should get:
          """
          === Repository status for local operations ===
            Targets  Total Size
          ---------  ------------
                  4  231.3 kB

          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  -------------------
          targets    Signing       1 / 1                 2  1.0.31      6 days, 23 hours
          snapshot   Signing       1 / 1                 2  1.0.31      6 days, 23 hours
          timestamp  Signing       1 / 1                 2  1.0.31      23 hours
          root       Signing       2 / 1                 1  1.0.31      364 days, 23 hours

          Key name    Role       Key type(s)      Scheme    Path
          ----------  ---------  ---------------  --------  --------------------------------------------
          targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
          snapshot    snapshot   public, private  ed25519   /var/rugged/signing_keys/snapshot/snapshot
          timestamp   timestamp  public, private  ed25519   /var/rugged/signing_keys/timestamp/timestamp
          root        root       public, private  ed25519   /var/rugged/signing_keys/root/root
          root1       root       public, private  ed25519   /var/rugged/signing_keys/root/root1
          """
     Then I should not get:
          """
          === Repository status for root-worker ===
          === Repository status for targets-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for test-worker ===
          === Repository status for timestamp-worker ===
          """

  Scenario: Print a worker's status.
     When I run the Rugged command "rugged status --worker=test-worker"
     Then I should get:
          """
          === Repository status for test-worker ===
            Targets  Total Size
          ---------  ------------
                  4  231.3 kB

          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  -------------------
          targets    Signing       1 / 1                 2  1.0.31      6 days, 23 hours
          snapshot   Verification  1 / 1                 2  1.0.31      6 days, 23 hours
          timestamp  Verification  1 / 1                 2  1.0.31      23 hours
          root       Verification  2 / 1                 1  1.0.31      364 days, 23 hours

          Key name    Role       Key type(s)      Scheme    Path
          ----------  ---------  ---------------  --------  -------------------------------------------
          targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
          snapshot    snapshot   public           ed25519   /var/rugged/verification_keys/snapshot/snapshot.pub
          timestamp   timestamp  public           ed25519   /var/rugged/verification_keys/timestamp/timestamp.pub
          root        root       public           ed25519   /var/rugged/verification_keys/root/root.pub
          root1       root       public           ed25519   /var/rugged/verification_keys/root/root1.pub
          """
     Then I should not get:
          """
          === Repository status for local operations ===
          === Repository status for root-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          === Repository status for timestamp-worker ===
          """

  Scenario: Print multiple workers' statuses.
     When I run the Rugged command "rugged status --worker=test-worker --worker=targets-worker"
     Then I should get:
          """
          === Repository status for test-worker ===
          === Repository status for targets-worker ===
          """
     Then I should not get:
          """
          === Repository status for local operations ===
          === Repository status for root-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for timestamp-worker ===
          """

  Scenario: Print overall status.
     When I run the Rugged command "rugged status"
     Then I should get:
          """
          === Repository status for local operations ===
          === Repository status for root-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          === Repository status for test-worker ===
          === Repository status for timestamp-worker ===
          """
