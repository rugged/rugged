@rugged @command @update-hashed-bins-count
Feature: Command to change the number of hashed bins in a running repo.
  In order to optimize performance for client downloads
  As a rugged administrator
  I need to change the number of hashed bins in a running repo.

# More tests for related functionlity can be found in `features/tuf/update_hashed_bins_count.feature`.
# We cannot continue here (with the @command tag) as these will not be run during @hashed-bin features.
# Further functionality require hashed bins to be enabled in config.

  Scenario: Start with a fresh repo.
    Given I reset Rugged
      And I run "sudo cp features/fixtures/config/enable_hashed_bins.yaml /var/rugged/.config/rugged/config.yaml"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged --debug initialize --local"

  Scenario: The 'update-hashed-bins-count' Rugged command exists.
     When I run the Rugged command "rugged update-hashed-bins-count --help"
     Then I should not get:
          """
          Error: No such command 'update-hashed-bins-count'.
          """
      And I should get:
          """
          Usage: rugged update-hashed-bins-count [OPTIONS]

            Re-write the repository to use a new number of hashed bins.

          Options:
            --confirm-backup                Confirm that a recent backup was taken.
            --skip-pause-checks             Skip checks to confirm that the repository
                                            is paused. (Not recommended)
            --skip-consistency-check        Skip validation of repository consistency.
                                            (Not recommended)
            --skip-current-bin-count-confirmation
                                            Skip confirmation of the current bin count.
                                            (Not recommended)
            --skip-desired-bin-count-confirmation
                                            Skip confirmation of the desired bin count.
                                            (Not recommended)
            --local                         (For testing) Re-sign targets, snapshot and
                                            timestamp metadata with locally available
                                            signing keys, rather than delegating to the
                                            respective workers.
            --help                          Show this message and exit.
          """

  Scenario: Confirm that a recent backup was taken.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count"
     Then I should get:
          """
          Preparing to re-write the repository to use a new number of hashed bins.
          Starting safety checks.
          warning: If this operation is interrupted before completing, it can leave the TUF repository in an inconsistent state, and thus make it unusable.
          warning: Taking a backup is highly recommended. See: https://rugged.works/how-to/maintenance/backup_restore/)
          Please confirm that you have taken a recent backup. [y/N]
          Aborted!
          """
      And I should not get:
          """
          debug: --confirm-backup flag received. Skipping prompt.
          Recent backup confirmed.
          """

  Scenario: Skip confirmation that a recent backup was taken.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup"
     Then I should get:
          """
          Preparing to re-write the repository to use a new number of hashed bins.
          Starting safety checks.
          warning: If this operation is interrupted before completing, it can leave the TUF repository in an inconsistent state, and thus make it unusable.
          warning: Taking a backup is highly recommended. See: https://rugged.works/how-to/maintenance/backup_restore/)
          debug: --confirm-backup flag received. Skipping prompt.
          Recent backup confirmed.
          """
      And I should not get:
          """
          Please confirm that you have taken a recent backup. [y/N]
          Aborted!
          """

  Scenario: Check that the repository is paused.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup"
     Then I should get:
          """
          Checking that the monitor-worker is paused.
          error: The monitor-worker appears not to be paused.
          Re-run the command with --debug for more information.
          Check status of repository.
          """
      And I should not get:
          """
          The monitor-worker is paused.
          """
    Given I run the Rugged command "touch /opt/post_to_tuf/tuf_paused"
      And I run the Rugged command "touch /opt/post_to_tuf/tuf_refreshing_expiry"
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup"
     Then I should get:
          """
          The monitor-worker is paused.
          The monitor-worker is not processing targets.
          error: The monitor-worker appears to be refreshing expiry periods.
          """
      And I should not get:
          """
          The monitor-worker is not refreshing expiry periods.
          """
    Given I run the Rugged command "touch /opt/post_to_tuf/tuf_processing_TEST"
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup"
     Then I should get:
          """
          error: The monitor-worker appears to be processing targets.
          """
      And I should not get:
          """
          The monitor-worker is not processing targets.
          """
    Given I run the Rugged command "rm /opt/post_to_tuf/tuf_processing_TEST"
      And I run the Rugged command "rm /opt/post_to_tuf/tuf_refreshing_expiry"
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup"
     Then I should get:
          """
          Checking that the monitor-worker is paused.
          The monitor-worker is paused.
          The monitor-worker is not processing targets.
          The monitor-worker is not refreshing expiry periods.
          """

  Scenario: Skip checks that the repository is paused.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks"
     Then I should get:
          """
          debug: --skip-pause-checks flag received. Skipping checks.
          """
      And I should not get:
          """
          Checking that the monitor-worker is paused.
          Checking that the monitor-worker is not processing targets.
          Checking that the monitor-worker is not refreshing expiry periods.
          """

  Scenario: Validate that the repository is consistent.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks"
     Then I should get:
          """
          Validating the consistency of the repository.
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
      And I should not get:
          """
          error:
          """
    Given I run "sudo cp features/fixtures/metadata/invalid_targets.json fixtures/tuf_repo/metadata/targets.json"
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks"
     Then I should get:
          """
          Validating the consistency of the repository.
          error: Metadata for the 'targets' role is not valid.
          """
      And I should not get:
          """
          Metadata for the 'targets' role is valid.
          """

  Scenario: Skip check that the repository is valid/consistent.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check"
     Then I should get:
          """
          debug: --skip-consistency-check flag received. Skipping check.
          Skipped validation of repository consistency.
          """
      And I should not get:
          """
          Validating the consistency of the repository.
          """

  Scenario: Check that no collisions are found between new and old hashed bin names.
    Given I run "sudo cp features/fixtures/config/number_of_bins.yaml /var/rugged/.config/rugged/config.yaml"
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then I should get:
          """
          Current bin count: 16
          Desired bin count: 32
          No duplicates were found between the current bins names and desired bin names.
          """
      And I should not get:
          """
          debug: The following duplicates were found between the current bins names and desired bin names:
          error: Duplicates were found between the current bins names and desired bin names.
          Cannot update to desired hashed bins count
          """

  Scenario: Exit with an error if collisions are found between new and old hashed bin names.
     When I try to run the Rugged command "rugged --debug update-hashed-bins-count --confirm-backup --skip-pause-checks --skip-consistency-check --skip-current-bin-count-confirmation --skip-desired-bin-count-confirmation --local"
     Then I should get:
          """
          Current bin count: 32
          Desired bin count: 32
          debug: The following duplicates were found between the current bins names and desired bin names:
          error: Duplicates were found between the current bins names and desired bin names.
          Cannot update to desired hashed bins count (32).
          """
      And I should not get:
          """
          No duplicates were found between the current bins names and desired bin names.
          """
