@rugged @command @local @initialize-partial-root-metadata-for-update @gitlab-163
Feature: Initialize partial root metadata for update HSM signing.
  In order to update TUF metadata using an HSM-based root key
  As an administrator
  I need to initialize updated partial root metadata.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/partial /var/rugged/tuf_repo/tmp"
      And the file "/var/rugged/tuf_repo/partial/2.root.json" does not exist
      And the file "/var/rugged/tuf_repo/partial/signable-2.root.json" does not exist

  Scenario: The 'initialize-partial-root-metadata-for-update' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged initialize-partial-root-metadata-for-update --help"
     Then I should not get:
          """
          Error: No such command 'initialize-partial-root-metadata-for-update'.
          """
      And I should get:
          """
          Usage: rugged initialize-partial-root-metadata-for-update [OPTIONS]

            Initialize a new partial Root metadata file from production Root metadata.

          Options:
            --help  Show this message and exit.
          """

  @gitlab-215
  Scenario: Emit useful error when partial root metadata has not been initialized.
    Given I reset Rugged
     When I fail to run the Rugged command "rugged --debug initialize-partial-root-metadata-for-update"
     Then I should get:
          """
          error: No production root metadata file exists at the expected path: /var/rugged/tuf_repo/metadata/1.root.json
          Consider using the 'initialize' command to generate valid root metadata.
          """

  Scenario: Do not overwrite existing partial root metadata.
    Given I run "sudo sudo -u rugged touch /var/rugged/tuf_repo/partial/2.root.json"
     When I fail to run the Rugged command "rugged --debug initialize-partial-root-metadata-for-update"
     Then I should get:
          """
          debug: Checking for existing file at: /var/rugged/tuf_repo/partial/2.root.json
          error: A file already exists at the desired path: /var/rugged/tuf_repo/partial/2.root.json
          """

  Scenario: Initialize new root metadata version.
     When I run the Rugged command "rugged --debug initialize-partial-root-metadata-for-update"
     Then I should get:
          """
          Initializing partial root metadata for update at: /var/rugged/tuf_repo/partial/2.root.json
          debug: Wrote 'root' metadata to file '/var/rugged/tuf_repo/partial/2.root.json'.
          """
      And the file "/var/rugged/tuf_repo/partial/2.root.json" exists
      And the file "/var/rugged/tuf_repo/partial/signable-2.root.json" exists
      And the file "/var/rugged/tuf_repo/partial/signable-2.root.json" does not contain:
          """
          signatures
          signed
          """
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 2 (2.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 1/2:
            type: ed25519
            keyid: 
          root 2/2:
            type: ed25519
            keyid: 
          snapshot 1/1:
            type: ed25519
            keyid: 
          targets 1/1:
            type: ed25519
            keyid: 
          timestamp 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """
      And I should not get:
          """
          signed by:
          """

  Scenario: Initialize new root metadata version with changed thresholds in config.
    Given I run "sudo cp features/fixtures/config/signature_thresholds.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged --debug initialize-partial-root-metadata-for-update"
     Then I should get:
          """
          Initializing partial root metadata for update at: /var/rugged/tuf_repo/partial/2.root.json
          debug: Setting 'root' signature threshold to 2.
          debug: Setting 'timestamp' signature threshold to 2.
          debug: Setting 'snapshot' signature threshold to 2.
          debug: Setting 'targets' signature threshold to 2.
          debug: Wrote 'root' metadata to file '/var/rugged/tuf_repo/partial/2.root.json'.
          """
