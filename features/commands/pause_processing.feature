@rugged @command @pause-processing @gitlab-183
Feature: Command to pause processing of scheduled tasks.
  In order to cleanly run certain operations
  As an administrator
  I need to pause processing of scheduled tasks on the monitor-worker.

  Background:
    Given I reset Rugged
      And the "/opt/post_to_tuf/tuf_paused" directory should not exist

  Scenario: The 'pause-processing' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged pause-processing --help"
     Then I should not get:
          """
          Error: No such command 'pause-processing'.
          """
     Then I should get:
          """
          Usage: rugged pause-processing [OPTIONS]

            Pause processing of scheduled tasks on the monitor-worker.

          Options:
            --force                       Exit with success, even if waiting for other
                                          tasks to complete times out.
            --processing-timeout INTEGER  (Default: 30) Time (in seconds) to wait for
                                          'find-new-targets' task to complete
                                          processing. Set to '0' to skip wait.
            --refreshing-timeout INTEGER  (Default: 15) Time (in seconds) to wait for
                                          'refresh-expiry' task to complete processing.
                                          Set to '0' to skip wait.
            --help                        Show this message and exit.
          """

  Scenario: The pause-processing Rugged command creates a semaphore.
     When I run the Rugged command "rugged --debug pause-processing --refreshing-timeout=0 --processing-timeout=0"
     Then the "/opt/post_to_tuf/tuf_paused" directory should exist
      And I should get:
          """
          Pausing processing of scheduled tasks.
          debug: Creating 'pause-processing' semaphore directory: /opt/post_to_tuf/tuf_paused
          Paused processing of scheduled tasks.
          """

  Scenario: The pause-processing Rugged command warns if a semaphore already exists.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_paused"
     When I run the Rugged command "rugged --debug pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I should get:
          """
          Pausing processing of scheduled tasks.
          debug: Detected pause-processing flag at: /opt/post_to_tuf/tuf_paused
          warning: Detected an existing pause-processing flag at: /opt/post_to_tuf/tuf_paused
          Paused processing of scheduled tasks.
          """

  Scenario: The pause-processing Rugged command waits for add-targets processing tasks to complete before returning.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_processing_TIMESTAMP"
     When I fail to run the Rugged command "rugged --debug pause-processing --refreshing-timeout=0 --processing-timeout=1"
     Then the "/opt/post_to_tuf/tuf_paused" directory should exist
      And I should get:
          """
          Pausing processing of scheduled tasks.
          debug: Creating 'pause-processing' semaphore directory: /opt/post_to_tuf/tuf_paused
          debug: Detected processing directory at: /opt/post_to_tuf/tuf_processing_TIMESTAMP
          Waiting for currently processing 'add-targets' task to complete.
          debug: RuggedTimeoutError: Timeout expired waiting for currently processing 'add-targets' task to complete.
          error: RuggedTimeoutError thrown in pause_processing_cmd: Timeout expired waiting for currently processing 'add-targets' task to complete.
          warning: Left pause-processing flag in place at: /opt/post_to_tuf/tuf_paused
          """
      And I should not get:
          """
          Paused processing of scheduled tasks.
          """

  Scenario: The pause-processing Rugged command waits for refresh-expiry tasks to complete before returning.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_refreshing_expiry"
     When I fail to run the Rugged command "rugged --debug pause-processing --refreshing-timeout=1 --processing-timeout=0"
     Then the "/opt/post_to_tuf/tuf_paused" directory should exist
      And I should get:
          """
          Pausing processing of scheduled tasks.
          debug: Creating 'pause-processing' semaphore directory: /opt/post_to_tuf/tuf_paused
          Detected currently processing scheduled 'refresh-expiry' task.
          Waiting for current processing task to complete.
          debug: RuggedTimeoutError: Timeout expired waiting for 'refresh-expiry' task processing to complete.
          error: RuggedTimeoutError thrown in pause_processing_cmd: Timeout expired waiting for 'refresh-expiry' task processing to complete.
          warning: Left pause-processing flag in place at: /opt/post_to_tuf/tuf_paused
          """
      And I should not get:
          """
          Paused processing of scheduled tasks.
          """
