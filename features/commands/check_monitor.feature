@rugged @command @check-monitor @monitor-worker
Feature: Command to print monitor-worker status.
  In order to ensure that the monitor-worker is working properly
  As an administrator
  I need to check the monitor-worker's filesystem access and semaphores.

  Background:
    Given I rebuild fixtures

  Scenario: The 'check-monitor' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged check-monitor --help"
     Then I should not get:
          """
          Error: No such command 'check-monitor'.
          """
      And I should get:
          """
          Usage: rugged check-monitor [OPTIONS]

            Report on the status of the monitor-worker.

          Options:
            --help  Show this message and exit.
          """

  Scenario: Check the monitor-worker's access to required filesystem resources.
     When I run the Rugged command "rugged check-monitor"
     Then I should get:
          """
          Monitor worker has read access to incoming-targets directory: `/var/rugged/incoming_targets`
          Monitor worker has write access to incoming-targets directory: `/var/rugged/incoming_targets`
          Monitor worker has read access to post-to-tuf directory: `/opt/post_to_tuf`
          Monitor worker has write access to post-to-tuf directory: `/opt/post_to_tuf`
          """

  @gitlab-199
  Scenario Outline: Check the monitor-worker's status and related filesystem semaphores.
     When I run the Rugged command "rugged check-monitor"
     Then I should not get:
          """
          warning: <WARNING>
          """
    Given I run the Rugged command "mkdir <SEMAPHORE>"
     When I run the Rugged command "rugged check-monitor"
     Then I should get:
          """
          warning: <WARNING>
          """
    Examples:
        | SEMAPHORE                              | WARNING                                      |
        | /opt/post_to_tuf/tuf_paused            | Monitor worker is paused.                    |
        | /opt/post_to_tuf/tuf_refreshing_expiry | Monitor worker is refreshing expiry periods. |
        | /opt/post_to_tuf/tuf_processing_TEST   | Monitor worker is processing new targets.    |

  @gitlab-199
  Scenario Outline: Check the monitor-worker for stale filesystem semaphores.
     When I run the Rugged command "rugged check-monitor"
     Then I should not get:
          """
          error: A semaphore ('<SEMAPHORE>') appears stale (more than <TIMEOUT> seconds since last change). Remove it with 'rugged <COMMAND>'.
'.
          """
    Given I run the Rugged command "mkdir <SEMAPHORE>"
      And I run the Rugged command "touch -d yesterday <SEMAPHORE>"
     When I fail to run the Rugged command "rugged check-monitor"
     Then I should get:
          """
          error: A semaphore ('<SEMAPHORE>') appears stale (more than <TIMEOUT> seconds since last change). Remove it with 'rugged <COMMAND>'.
          """
    Examples:
        | SEMAPHORE                              | COMMAND                 | TIMEOUT |
        | /opt/post_to_tuf/tuf_paused            | resume-processing       | 3600    |
        | /opt/post_to_tuf/tuf_refreshing_expiry | reset-refreshing-expiry | 60      |
        | /opt/post_to_tuf/tuf_processing_TEST   | reset-processing        | 300     |
