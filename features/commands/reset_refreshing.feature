@rugged @command @reset-refreshing @gitlab-216
Feature: Command to reset the refreshing expiring flag.
  In order to resume processing of scheduled tasks on the monitor-worker
  As an administrator
  I need to remove stale refreshing-expiry semaphores.

  Background:
    Given I reset Rugged
      And the "/opt/post_to_tuf/tuf_refreshing_expiry" directory should not exist

  Scenario: The 'reset-refreshing' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged reset-refreshing --help"
     Then I should not get:
          """
          Error: No such command 'reset-refreshing'.
          """
     Then I should get:
          """
          Usage: rugged reset-refreshing [OPTIONS]

            Reset the refreshing-expiry semaphore if it has become stale.

          Options:
            --force  Force reset of refreshing-expiry semaphore, even if it is not
                     stale.
            --help   Show this message and exit.
          """

  Scenario: Report if there is no stale refreshing-expiry semaphore to reset.
     When I run the Rugged command "rugged reset-refreshing"
     Then I should get:
          """
          Resetting stale refreshing-expiry semaphore.
          warning: No refreshing-expiry flag was found at: /opt/post_to_tuf/tuf_refreshing_expiry
          Reset stale refreshing-expiry semaphore.
          """

  Scenario: Reset only stale refreshing-expiry semaphores by default.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_refreshing_expiry"
     When I run the Rugged command "rugged --debug reset-refreshing"
     Then I should get:
          """
          debug: Found refresh-expiry flag at: /opt/post_to_tuf/tuf_refreshing_expiry
          No stale refreshing-expiry flag was found at: /opt/post_to_tuf/tuf_refreshing_expiry
          """
    Given I run the Rugged command "touch -d yesterday /opt/post_to_tuf/tuf_refreshing_expiry"
     When I run the Rugged command "rugged --debug reset-refreshing"
     Then I should get:
          """
          Resetting stale refreshing-expiry semaphore.
          debug: Found refresh-expiry flag at: /opt/post_to_tuf/tuf_refreshing_expiry
          debug: Deleting 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refreshing_expiry
          Deleted 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refreshing_expiry
          Reset stale refreshing-expiry semaphore.
          """
      And I should not get:
          """
          No stale refreshing-expiry flag was found at: /opt/post_to_tuf/tuf_refreshing_expiry
          """
      And the "/opt/post_to_tuf/tuf_refreshing_expiry" directory should not exist

  Scenario: Force reset of all refreshing-expiry semaphores.
    Given I run the Rugged command "mkdir /opt/post_to_tuf/tuf_refreshing_expiry"
     When I run the Rugged command "rugged --debug reset-refreshing --force"
     Then I should get:
          """
          Forcing reset of refreshing-expiry semaphore.
          debug: Found refresh-expiry flag at: /opt/post_to_tuf/tuf_refreshing_expiry
          No stale refreshing-expiry flag was found at: /opt/post_to_tuf/tuf_refreshing_expiry
          debug: Deleting 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refreshing_expiry
          Deleted 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refreshing_expiry
          Reset stale refreshing-expiry semaphore.
          """
      And I should not get:
          """
          Resetting stale refreshing-expiry semaphore.
          """
      And the "/opt/post_to_tuf/tuf_refreshing_expiry" directory should not exist
