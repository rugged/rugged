@rugged @command @add-verification-key @local @gitlab-160
Feature: Commands to generate signed root metadata.
  In order to host TUF metadata using an HSM-based root key
  As an administrator
  I need to add verification keys to partial root metadata.

  Background:
    Given I reset Rugged
      And I run "sudo sudo -u rugged mkdir -p /var/rugged/tuf_repo/tmp"
      And I run the Rugged command "rugged initialize-partial-root-metadata"
      And I generate a complete set of Rugged keys

  Scenario: Add PEM key to root metadata.
          # Generate a keypair to emulate an HSM.
          # See: features/misc/openssl.feature
    Given I record a reference hash of "/var/rugged/tuf_repo/partial/1.root.json"
      And I record a reference hash of "/var/rugged/tuf_repo/partial/signable-1.root.json"
     When I run the Rugged command "rugged --debug add-verification-key root /var/rugged/tuf_repo/tmp/root_public.pem --key-type=pem"
     Then I should get:
          """
          debug: Adding 'pem' key for 'root' role from: /var/rugged/tuf_repo/tmp/root_public.pem
          debug: Loading PEM-encoded public key from: /var/rugged/tuf_repo/tmp/root_public.pem
          debug: Loaded PEM-encoded public key from: /var/rugged/tuf_repo/tmp/root_public.pem
          """
      And file "/var/rugged/tuf_repo/partial/1.root.json" has changed
      And file "/var/rugged/tuf_repo/partial/signable-1.root.json" has changed
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 0
            keyids: No keyids found
          """
      And I should not get:
          """
          signed by:
          root 1/2
          root 2/2
          snapshot 1/1
          targets 1/1
          timestamp 1/1
          """

  Scenario: Add both PEM and TUF keys to root metadata.
    Given I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root_public.pem --key-type=pem"
      And I run the Rugged command "rugged add-verification-key root /var/rugged/tuf_repo/tmp/root1_public.pem --key-type=pem"
      And I run the Rugged command "rugged add-verification-key snapshot /var/rugged/verification_keys/snapshot/snapshot.pub"
      And I run the Rugged command "rugged add-verification-key targets /var/rugged/verification_keys/targets/targets.pub"
      And I run the Rugged command "rugged add-verification-key timestamp /var/rugged/verification_keys/timestamp/timestamp.pub"
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 364 days, 23 hours
          === SIGNATURES ===
          Signatures: 0
          Threshold: 1
          === KEYS ===
          root 1/2:
            type: ed25519
            keyid: 
          root 2/2:
            type: ed25519
            keyid: 
          snapshot 1/1:
            type: ed25519
            keyid: 
          targets 1/1:
            type: ed25519
            keyid: 
          timestamp 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """
      And I should not get:
          """
          signed by:
          """
