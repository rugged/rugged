@rugged @command @show-partial-root-metadata @gitlab-160
Feature: Command to show information about partial root metadata.
  In order to progressively build root metadata
  As an administrator
  I need to see information about partial root metadata.

  Background:
    Given I reset Rugged
      And I run the Rugged command "mkdir -p /var/rugged/tuf_repo/partial /var/rugged/tuf_repo/tmp"

  Scenario: The 'show-partial-root-metadata' Rugged command exists.
     When I try to run "sudo sudo -u rugged rugged show-partial-root-metadata --help"
     Then I should not get:
          """
          Error: No such command 'show-partial-root-metadata'.
          """
      And I should get:
          """
          Usage: rugged show-partial-root-metadata [OPTIONS]

            Print information about partial Root metadata.

          Options:
            --help  Show this message and exit.
          """

  @gitlab-214
  Scenario: Emit useful error when partial root metadata has not been initialized.
     When I fail to run the Rugged command "rugged --debug show-partial-root-metadata"
     Then I should get:
          """
          error: No partial root metadata file exists at the expected path: /var/rugged/tuf_repo/partial/1.root.json
          Consider using the 'initialize-partial-root-metadata' or 'initialize-partial-root-metadata-for-update' command to generate partial root metadata.
          """

  Scenario: Show partial root metadata.
    Given I run the Rugged command "cp features/fixtures/hsm_support/tuf_repo/metadata/1.root.json /var/rugged/tuf_repo/partial/"
     When I run the Rugged command "rugged --debug show-partial-root-metadata"
     Then I should get:
          """
          debug: rugged_cli invoked
          debug: Loading metadata for 'root' from /var/rugged/tuf_repo/partial/1.root.json.
          debug: Loaded metadata for 'root' role from '/var/rugged/tuf_repo/partial/1.root.json'.
          Retrieved partial Root metadata for version 1 (1.root.json).
          === METADATA ===
          Expires in 
          === SIGNATURES ===
          Signatures: 2
          Threshold: 1
          Signature 1 of 2: signed by root 1/2 -- VALID (keyid:
          Signature 2 of 2: signed by root 2/2 -- VALID (keyid:
          === KEYS ===
          root 1/2:
            type: ed25519
            keyids: 
          root 2/2:
            type: ed25519
            keyids: 
          snapshot 1/1:
            type: ed25519
            keyids: 
          targets 1/1:
            type: ed25519
            keyids: 
          timestamp 1/1:
            type: ed25519
            keyids: 
          === ROLES ===
          root:
            Keys required (to meet signature threshold): 1
            Keys provided: 2
            keyids: 
          snapshot:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          timestamp:
            Keys required (to meet signature threshold): 1
            Keys provided: 1
            keyids: 
          """

  Scenario: Show partial root metadata with below-threshold number of signatures.
    Given I run the Rugged command "rugged initialize-partial-root-metadata"
     When I run the Rugged command "rugged --debug show-partial-root-metadata"
     Then I should get:
          """
          === METADATA ===
          Metadata is not valid for deployment
          === SIGNATURES ===
          Signatures: 0
          Does not meet threshold
          Threshold: 1
          """

  Scenario: Show partial root metadata with valid (meets threshold) number of signatures.
    Given I generate a complete set of Rugged keys
      And I generate complete partial root metadata
      And I generate signatures of partial root metadata
      And I run the Rugged command "rugged add-root-signature --key-type=pem /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin"
     When I run the Rugged command "rugged --debug show-partial-root-metadata"
     Then I should get:
          """
          === METADATA ===
          Metadata is valid for deployment
          === SIGNATURES ===
          Signatures: 1
          Meets threshold
          Threshold: 1
          """

  @gitlab-107
  Scenario: Show partial root metadata with too few keys to satisfy signature thresholds.
    Given I generate a complete set of Rugged keys
      And I generate complete partial root metadata
     When I run the Rugged command "rugged remove-verification-key --yes `cat /var/rugged/tuf_repo/partial/1.root.json | jq .signed.roles.targets.keyids | jq 'nth(0)' -r`"
      And I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          === METADATA ===
          Metadata is not valid for deployment
          === ROLES ===
          targets: 
            Keys required (to meet signature threshold): 1
            Keys provided: 0 (Does not meet threshold)
            keyids: No keyids found
          """
      And I should not get:
          """
          targets 1/1:
          """

  @gitlab-107
  Scenario: Show partial root metadata with sufficient keys to satisfy signature thresholds.
    Given I generate a complete set of Rugged keys
      And I generate complete partial root metadata
      And I generate signatures of partial root metadata
      And I run the Rugged command "rugged add-root-signature --key-type=pem /var/rugged/tuf_repo/tmp/root_public.pem /var/rugged/tuf_repo/tmp/root_signature.bin"
     When I run the Rugged command "rugged show-partial-root-metadata"
     Then I should get:
          """
          === METADATA ===
          Metadata is valid for deployment
          === KEYS ===
          targets 1/1:
            type: ed25519
            keyid: 
          === ROLES ===
          targets:
            Keys required (to meet signature threshold): 1
            Keys provided: 1 (Meets threshold)
            keyids: 
          """
