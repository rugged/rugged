@rugged @cli @clean-up-targets
Feature: Delete targets once they have been signed.
  In order to reduce required file storage,
  As a Rugged operator,
  I need to be able to configure Rugged to delete targets once they have been signed.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo

  Scenario: Rugged preserves targets after signing, by default.
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
      And I run the Rugged command "rugged logs --worker=targets-worker"
     Then I should not get:
       """
       DEBUG (repo._delete_target_after_signing): Deleted '/var/rugged/tuf_repo/targets/test0.txt' after signing.
       """
     When I try to run "ls -la /var/rugged/tuf_repo/targets/test0.txt"
     Then I should get:
       """
       test0.txt
       """
      And I should not get:
       """
       ls: cannot access '/var/rugged/tuf_repo/targets/test0.txt': No such file or directory
       """

  #@TODO: Figure out how to test this. We're just testing the default behaviour
  #       again here. See note on next scenario for details.
  Scenario: Rugged can be configured to preserve targets after signing.
    Given I run "sudo cp features/fixtures/config/preserve_targets.yaml /var/rugged/.config/rugged/config.yaml"
      And I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
      And I run the Rugged command "rugged logs --worker=targets-worker"
     Then I should not get:
       """
       DEBUG (repo._delete_target_after_signing): Deleted '/var/rugged/tuf_repo/targets/test0.txt' after signing.
       """
     When I try to run "ls -la /var/rugged/tuf_repo/targets/test0.txt"
     Then I should get:
       """
       test0.txt
       """
      And I should not get:
       """
       ls: cannot access '/var/rugged/tuf_repo/targets/test0.txt': No such file or directory
       """

  Scenario: Rugged can be configured to delete targets after signing.
    Given I run "sudo cp features/fixtures/config/delete_targets.yaml /var/rugged/.config/rugged/config.yaml"
      And I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
     When I run the Rugged command "rugged --debug add-targets --local"
     Then I should get:
          """
          Added the following targets to the repository:
          test0.txt
          Updated targets metadata.
          """
     When I run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run the Rugged command "rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (repo._delete_target_after_signing): Deleted '/var/rugged/tuf_repo/targets/test0.txt' after signing.
          """
     When I try to run "ls -la /var/rugged/tuf_repo/targets/test0.txt"
     Then I should get:
          """
          ls: cannot access '/var/rugged/tuf_repo/targets/test0.txt': No such file or directory
          """
