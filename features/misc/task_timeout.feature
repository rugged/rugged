@rugged @config @monitor-worker @cli
Feature: Configurable task timeouts.
  In order to for Rugged to support different operating environments
  As an administrator
  I need to need to be able to configure task timeouts.

  Background:
    Given I reset Rugged

  Scenario: Default timeout config.
     When I run the Rugged command "rugged config --local |grep task_timeout"
     Then I should get:
          """
          task_timeout
          10
          """
     When I run the Rugged command "rugged config --worker=monitor-worker |grep task_timeout"
     Then I should get:
          """
          task_timeout
          10
          """

  Scenario: Customized timeout config.
    Given I run "sudo cp features/fixtures/config/task_timeout.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged config --local |grep task_timeout"
     Then I should get:
          """
          task_timeout
          5
          """
     When I run the Rugged command "rugged config --worker=monitor-worker |grep task_timeout"
     Then I should get:
          """
          task_timeout
          10
          """

  @slow
  Scenario: Tasks timeout after configured (5 seconds) interval.
    Given I run "sudo cp features/fixtures/config/task_timeout.yaml /var/rugged/.config/rugged/config.yaml"
     When I run the Rugged command "rugged sleep 4"
     Then I should get:
          """
          Sending test-worker to sleep for 4 seconds...
          Test worker went to sleep for '4' seconds.
          """
      And I should not get:
          """
          warning: The operation timed out. Check status of test-worker.
          """
     When I fail to run the Rugged command "rugged sleep 6"
     Then I should get:
          """
          Sending test-worker to sleep for 6 seconds...
          warning: The operation timed out. Check status of test-worker.
          """
      And I should not get:
          """
          Test worker went to sleep for
          """
