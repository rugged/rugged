@rugged @backup @restore
Feature: Basic manual backup and restore procedures.
  In order to improve reliability or the Rugged repo
  As a rugged administrator
  I need to backup and restore TUF metadata

  Background:
    Given I rebuild fixtures
      And the "fixtures/tuf_repo/metadata.backup/" directory should not exist

  Scenario: Send a ping/echo message, specifying a valid worker.
    Given I run the Rugged command "rugged validate"
      And I run the Rugged command "rugged pause-processing"
     When I run the Rugged command "cp -r fixtures/tuf_repo/metadata/ fixtures/tuf_repo/metadata.backup/"
     Then the following files should exist:
          """
          fixtures/tuf_repo/metadata.backup/1.root.json
          fixtures/tuf_repo/metadata.backup/snapshot.json
          fixtures/tuf_repo/metadata.backup/targets.json
          fixtures/tuf_repo/metadata.backup/timestamp.json
          """
     When I run the Rugged command "sed -i 's/token/intentionally_invalid/g' fixtures/tuf_repo/metadata/targets.json"
      And I fail to run the Rugged command "rugged validate"
     Then I should get:
          """
          error: Metadata for the 'targets' role is not valid.
          """
     When I run the Rugged command "cp -r fixtures/tuf_repo/metadata.backup/* fixtures/tuf_repo/metadata/"
      And I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'targets' role is valid.
          """
