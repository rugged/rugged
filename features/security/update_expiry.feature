@rugged @security @metadata-expiry
Feature: Metadata expiry timestamp is updated consistently.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to ensure that metadata is updated consistently.

  # For expiry behaviour when hashed bins are enabled, see:
  #   features/tuf/hashed_bins.expiry.feature

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo

  # The step definition that checks whether the expiry timestamp has been
  # updated requires the filename to be consistent. It uses an array internally
  # keyed to the filename. Since the filename for root metadata changes, we
  # create a symlink so that the initial root metadata (`1.root.json`) and the
  # updated one (`2.roots.json`) can be compared.
  Scenario: Root metadata expiry gets updated appropriately.
    Given I run "sudo ln -s /var/rugged/tuf_repo/metadata/1.root.json /var/rugged/tuf_repo/metadata/root.json"
      And I record the expiry timestamp from "root.json"
      And file "/var/rugged/incoming_targets/test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
     Then the expiry timestamp from "root.json" has not changed
    Given I run "sudo cp features/fixtures/key_rotation/add_new_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I wait "1" seconds
     When I run the Rugged command "rugged --debug rotate-keys"
      And I run "sudo ln -sf /var/rugged/tuf_repo/metadata/2.root.json /var/rugged/tuf_repo/metadata/root.json"
     Then the expiry timestamp from "root.json" has changed

  Scenario: Snapshot metadata expiry gets updated appropriately.
    Given I record the expiry timestamp from "snapshot.json"
      And I run "sudo cp features/fixtures/key_rotation/add_new_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I wait "1" seconds
     When I run the Rugged command "rugged rotate-keys"
     Then the expiry timestamp from "snapshot.json" has not changed
      And file "/var/rugged/incoming_targets/test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
     Then the expiry timestamp from "snapshot.json" has changed

  Scenario: Targets metadata expiry gets updated appropriately.
    Given I record the expiry timestamp from "targets.json"
      And I run "sudo cp features/fixtures/key_rotation/add_new_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I wait "1" seconds
     When I run the Rugged command "rugged rotate-keys"
     Then the expiry timestamp from "targets.json" has not changed
      And file "/var/rugged/incoming_targets/test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
     Then the expiry timestamp from "targets.json" has changed

  Scenario: Timestamp metadata expiry gets updated appropriately.
    Given I record the expiry timestamp from "timestamp.json"
      And I run "sudo cp features/fixtures/key_rotation/add_new_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I wait "1" seconds
     When I run the Rugged command "rugged rotate-keys"
     Then the expiry timestamp from "timestamp.json" has not changed
      And file "/var/rugged/incoming_targets/test0.txt" contains "test0"
     When I run the Rugged command "rugged add-targets"
     Then the expiry timestamp from "timestamp.json" has changed
