@composer-plugin @php-tuf @satis @tuf @rugged @tuf-metadata @slow @composer-install
Feature: The TUF Composer plugin validates TUF metadata.
  In order to securely install PHP codebases
  As a site builder
  I need Composer to validate TUF metadata.

# Some of the output that we're expecting is not being detected. From visual
# inspection, it does appear in the output. This is possibly a failure of the
# "Then I should get:" test step. It works in many other cases. I suspect the
# sheer volume of out from running Comporser with `-vvv` may be the source of
# the issue.
#
# At any rate, I've commented-out that stuff, for now.
#
# @TODO: Figure out what's going on here.


  Background:
    Given I rebuild fixtures
      And I register the Rugged repo with Composer
      # For development, use the following Composer file. It will look for local instances of the `php-tuf-client` and `composer-integration` projects.
      #And I use the "rugged-dev-composer.json" Composer file
      And I use the "rugged-composer.json" Composer file
      And I am in the "d9-site" directory
      And I run "rm -rf web/modules/contrib/token/"
      And I run "composer clear-cache"
      And I run "rm -rf vendor/composer/tuf/http---packages.ddev.site/"

  @composer-install
  Scenario: When a repo is TUF-enabled, it will not emit a warning that TUF will not be verified.
     When I try to run "composer install -vvv"
     Then I should get:
          """
          TUF integration enabled.
          """
#          [TUF] Root metadata for http://packages.ddev.site loaded from d9-site/tuf/packages.ddev.site.json.
#          [TUF] Packages from http://packages.ddev.site are verified with base URL http://packages.ddev.site/targets
      And I should not get:
          """
          Authenticity of packages from http://packages.ddev.site are not verified by TUF.
          """
     When I use the "tuf-off-composer.json" Composer file
      And I try to run "composer install -vvv"
     Then I should get:
          """
          TUF integration enabled.
          Authenticity of packages from http://packages.ddev.site are not verified by TUF.
          """
      And I should not get:
          """
          [TUF] Root metadata for http://packages.ddev.site loaded from persistent storage.
          [TUF] Packages from http://packages.ddev.site are verified with base URL http://packages.ddev.site/targets
          """

  @composer-install
  Scenario: When a project uses a TUF-enabled repo, it must have initial root metadata available in the project.
    Given the file "tuf/packages.ddev.site.json" exists
     When I try to run "composer install -vvv"
     Then I should not get:
          """
          No TUF root metadata was found for repository http://packages.ddev.site
          """
      And the following files should exist:
          """
          web/modules/contrib/token/token.info.yml
          """

  @composer-install
  Scenario: When a TUF-enabled project does not have initial root metadata, the Composer TUF plugin refuses to download packages during install.
     When I run "rm tuf/packages.ddev.site.json"
      And I run "composer clear-cache"
          # @TODO: Shouldn't this be cleared during a clear-cache?
      And I run "rm -rf vendor/composer/tuf/http---packages.ddev.site/"
      And I try to run "composer install -vvv"
     Then I should get:
          """
          No TUF root metadata was found for repository http://packages.ddev.site
          """
      And the following files should not exist:
          """
          web/modules/contrib/token/token.info.yml
          """

  @composer-install @wip
  Scenario Outline: When any TUF metadata is missing, Composer TUF plugin refuses to download packages during install.
    Given I run "rm /var/rugged/pkg_repo/metadata/<metadata_file>"
          # @TODO: Shouldn't this be cleared during a clear-cache?
      And I run "rm -rf vendor/composer/tuf/http---packages.ddev.site/"
     When I try to run "composer install -vvv"
     Then I should not get:
          """
          Authenticity of packages from http://packages.ddev.site are not verified by TUF.
          """
      And I should get:
          """
          [Tuf\Exception\RepoFileNotFound]
          <error_msg>
          """
      And the following files should not exist:
          """
          web/modules/contrib/token/token.info.yml
          """
     Examples:
       | metadata_file  | error_msg                          |
       # N.B. the root.json is only updated if a new version is found.
       # So removing the initial one has no effect.
       | timestamp.json | /metadata/timestamp.json not found |
       | snapshot.json  | /metadata/snapshot.json not found  |
       | targets.json   | /metadata/targets.json not found   |

# @TODO Add a test that ensures the client handles key rotation properly.

  @composer-install
  Scenario: When all TUF metadata is present, Composer TUF plugin does not emit errors during install.
     When I run "composer install -vvv"
     Then I should get:
          """
          TUF integration enabled.
          """
#          [TUF] Root metadata for http://packages.ddev.site loaded from persistent storage.
#          [TUF] Packages from http://packages.ddev.site are verified with base URL http://packages.ddev.site/targets
#          [TUF] Target 'packages.json' limited to 192 bytes.
#          [TUF] Target 'packages.json' validated.
     Then I should not get:
          """
          [Tuf\Exception\NotFoundException]
          Target not found: include/all
          No TUF root metadata was found for repository http://packages.ddev.site
          [Tuf\Exception\RepoFileNotFound]
          /metadata/timestamp.json not found
          /metadata/snapshot.json not found
          /metadata/targets.json not found
          """

  @composer-install
  Scenario: When all TUF metadata is present, Composer TUF plugin downloads packages during install.
     When I run "composer install -vvv"
#     Then I should get:
#          """
#          [TUF] Target 'drupal/token/1.11.0.0' validated.
#          """
      And the following files should exist:
          """
          web/modules/contrib/token/token.info.yml
          """
