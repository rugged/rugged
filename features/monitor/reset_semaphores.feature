@rugged @monitor @scheduler @reset-semaphores @gitlab-204
Feature: Stale sempahores are reset automatically.
  In order to keep a TUF systems running smoothly,
  As a TUF administrator
  I need to ensure that stale semaphores are reset automatically.

  Background:
    Given I reset Rugged
      And I run "sudo rm -rf /opt/post_to_tuf/*"

  Scenario: Ensure that monitor worker has necessary config for testing 'reset-semaphores'.
     When I run the Rugged command "rugged config --worker=monitor-worker"
     Then I should get:
          """
          scheduler_log_level                           DEBUG
          task_timeout                                  3
          scheduler_reset_period                        5.0
          """

  Scenario: Ensure that monitor worker is polling for stale semaphores.
    # We have to wait for 6 second to ensure that the scheduled task has run at least once.
    Given I wait "6" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.reset_semaphores_task): Received reset-semaphores task.
          """

  Scenario: Fresh tuf-processing semaphores are not reset automatically.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
     When I wait "6" seconds
     Then the "/opt/post_to_tuf/tuf_processing_TIMESTAMP" directory should exist
      And the "/opt/post_to_tuf/tuf_ready_TIMESTAMP" directory should not exist
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.reset_semaphores_task): Checking for stale processing directories.
          """
      And I should not get:
          """
          WARNING (monitor-worker.reset_semaphores_task): Found stale processing directory. Resetting.
          """

  Scenario: Fresh refreshing-expiry semaphores are not reset automatically.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_refreshing_expiry/"
     When I wait "6" seconds
     Then the "/opt/post_to_tuf/tuf_refreshing_expiry" directory should exist
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.reset_semaphores_task): Checking for stale expiry-refreshing flag.
          """
      And I should not get:
          """
          WARNING (monitor-worker.reset_semaphores_task): Found stale expiry-refreshing flag. Resetting.
          """

  Scenario: Stale tuf-processing semaphores are reset automatically.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
      And I run the Rugged command "touch -d yesterday /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
     When I wait "6" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.reset_semaphores_task): Checking for stale processing directories.
          WARNING (monitor-worker.reset_semaphores_task): Found stale processing directory. Resetting.
          WARNING (processing.reset_processing_dir): Resetting processing directory ('/opt/post_to_tuf/tuf_processing_TIMESTAMP') to ready state.
          DEBUG (processing.reset_processing_dir): Renaming '/opt/post_to_tuf/tuf_processing_TIMESTAMP' to '/opt/post_to_tuf/tuf_ready_TIMESTAMP'.
          INFO (processing.reset_processing_dir): Renamed '/opt/post_to_tuf/tuf_processing_TIMESTAMP' to '/opt/post_to_tuf/tuf_ready_TIMESTAMP'.
          """

  Scenario: Stale refreshing-expiry semaphores are reset automatically.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_refreshing_expiry/"
      And I run the Rugged command "touch -d yesterday /opt/post_to_tuf/tuf_refreshing_expiry/"
     When I wait "6" seconds
     Then the "/opt/post_to_tuf/tuf_refreshing_expiry" directory should not exist
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.reset_semaphores_task): Checking for stale expiry-refreshing flag.
          WARNING (monitor-worker.reset_semaphores_task): Found stale expiry-refreshing flag. Resetting.
          DEBUG (refresh.delete_refreshing_flag): Deleting 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refr
eshing_expiry
          INFO (refresh.delete_refreshing_flag): Deleted 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refres
hing_expiry
          """
