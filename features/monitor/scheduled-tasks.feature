@rugged @workers @monitor-worker
Feature: Scheduled tasks work well together.
  In order to allow unattended operation of a TUF server
  As a TUF administrator
  I need to ensure that scheduled tasks work reliably together.

  Background:
    Given I reset Rugged
      And I run "sudo rm -rf /opt/post_to_tuf/*"

####
#
# To keep our local logs from filling up the disk, we default to a log-level of
# INFO. For these tests to pass, we need the log-level to be DEBUG. Also we
# default to a 5-second period between scans for new target files.
#
# To speed up development and testing that depends on the scheduler, we can
# reduce this period.
#
# To accomplish this in a local environment, run `make enable-debug-on-monitor`.
#
# To return to the default behaviour, run: `make disable-debug-on-monitor`.
#
# We've disabled the below tasks tagged with "@scheduler", since they won't
# work without the changes described above. To run these, run:
# `ddev behat --tags=scheduler`
#
####

  @scheduler @tuf-refreshing-dir
  Scenario: Ensure that scheduled add-targets tasks wait until a `tuf_refreshing_expiry` directory (in `post-to-tuf`) is removed.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_refreshing_expiry/"
      And I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run the Rugged command "rugged logs --truncate"
      And I wait "1" seconds
     When I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP /opt/post_to_tuf/tuf_ready_TIMESTAMP"
      And I wait "1" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          INFO (monitor-worker.find_new_targets_task): Count of post-to-tuf content: 2
          INFO (monitor-worker.find_new_targets_task): Age of oldest post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task): List of post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          DEBUG (monitor-worker.find_new_targets_task):   tuf_refreshing_expiry
          DEBUG (refresh.monitor_worker_is_refreshing): Found refresh-expiry flag at: /opt/post_to_tuf/tuf_refreshing_expiry
          DEBUG (refresh.monitor_worker_is_refreshing): Detected currently processing scheduled 'refresh-expiry' task.
          INFO (monitor-worker.find_new_targets_task): Monitor worker is refreshing expiry periods. Waiting for next scan to continue.
          """
     Then I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for:
          """
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should not get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          """
    Given I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I run "sudo sudo -u rugged rmdir /opt/post_to_tuf/tuf_refreshing_expiry/"
      And I wait "1" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should not get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Detected currently processing scheduled 'refresh-expiry' task.
          INFO (monitor-worker.refresh_expiry_task): Waiting for current processing task to complete.
          """
     Then I should get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Received find-new-targets task.
          INFO (monitor-worker.find_new_targets_task): New content found in post-to-tuf directory.
          INFO (monitor-worker.find_new_targets_task): Count of post-to-tuf content: 1
          INFO (monitor-worker.find_new_targets_task): Age of oldest post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task): List of post-to-tuf content:
          DEBUG (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for:
          """
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          """

  @refresh-expiry @tuf-processing-dir
  Scenario: Ensure that scheduled refresh-expiry tasks wait until a `tuf_processing_` directory (in `post-to-tuf`) is removed.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_refreshing_expiry/"
      And I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
      And I run the Rugged command "cp features/fixtures/config/expiry/short_snapshot_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I initialize a Rugged repo
      And I record the expiry timestamp from "snapshot.json"
    Given I run the Rugged command "rmdir /opt/post_to_tuf/tuf_refreshing_expiry/"
      And I wait "5" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Received refresh-expiry task.
          DEBUG (refresh.create_refreshing_flag): Creating 'refresh-expiry' task semaphore directory: /opt/post_to_tuf/tuf_refreshing_expiry
          DEBUG (processing.monitor_worker_is_processing): Detected currently processing scheduled 'add-targets' task.
          INFO (processing.wait_for_processing_task_to_complete): Waiting for currently processing 'add-targets' task to complete.
          """
     When I wait "6" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (processing.wait_for_processing_task_to_complete): Waited 5 seconds for currently processing 'add-targets' task to complete.
          """
      And I should not get:
          """
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          """
      And the expiry timestamp from "snapshot.json" has not changed
    Given I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I run the Rugged command "rmdir /opt/post_to_tuf/tuf_processing_TIMESTAMP/"
      And I wait "5" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should not get:
          """
          INFO (processing.wait_for_processing_task_to_complete): Detected currently processing scheduled 'add-targets' task.
          INFO (processing.wait_for_processing_task_to_complete): Waiting for current processing task to complete.
          """
      And I should get:
          """
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          """
      And the expiry timestamp from "snapshot.json" has changed
