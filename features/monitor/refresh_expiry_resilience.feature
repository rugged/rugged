@rugged @refresh-expiry-resilience @gitlab-173
Feature: Refresh expiry tasks on the monitor-worker are resilient to network instability.
  In order to run reliably for extended periods
  As a TUF operator
  I need automatic expiry-refresh tasks to tolerate occasional network instability.

  Background:
    Given I reset Rugged
      And I run "sudo rm -rf /opt/post_to_tuf/*"
      And I run "sudo cp features/fixtures/config/expiry/short_targets_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo mkdir /opt/post_to_tuf/tuf_refreshing_expiry"
      And I run the Rugged command "rugged generate-keys --local"
      And I run the Rugged command "rugged logs --truncate --worker=monitor-worker"
      And I run the Rugged command "rugged initialize --local"

  Scenario: Ensure that monitor worker has necessary config for testing 'refresh-expiry' resilience.
     When I run the Rugged command "rugged config --worker=monitor-worker"
     Then I should get:
          """
          scheduler_log_level                                      DEBUG
          task_timeout                                             1
          monitor_enable_network_instability_resiliency_test_mode  True
          scheduler_refresh_period                                 5.0
          """

# N.B. We only test Targets metadata below because we can only force
# timeouts on a task type, but not a specific worker. As such, this will
# always timeout on Targets metadata, which will never give us a chance to
# detect a timeout on a Snapshot or Timestamp, which come after.

  Scenario: If a 'get_expiring_metadata_task' task times out, it is retried.
    Given I run the Rugged command "touch /opt/post_to_tuf/get_expiring_metadata_task.test-flag"
      And I run "sudo rm -rf /opt/post_to_tuf/tuf_refreshing_expiry"
      And I wait "10" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
      And I should get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Received refresh-expiry task.
          INFO (monitor-worker.refresh_expiry_task): Fetching expiring metadata.
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetching expiring metadata from targets-worker.
          DEBUG (monitor-worker._run_remote_task): Diverting 'get_expiring_metadata_task' task from 'targets-worker' to 'sleep' on test worker, to force a timeout.
          DEBUG (timeout_error.__init__): RuggedTimeoutError: The operation exceeded the given deadline.
          WARNING (task_queue.run_task): The operation timed out. Check status of test-worker.
          DEBUG (timeout_error.__init__): RuggedTimeoutError: The operation timed out. Check status of test-worker.
          WARNING (monitor-worker._dispatch_fetch_expiring_metadata): Task timed-out when fetching expiring metadata from targets-worker.
          INFO (monitor-worker._dispatch_fetch_expiring_metadata): Retry 1/2: Fetching expiring metadata from targets-worker.
          INFO (monitor-worker._dispatch_fetch_expiring_metadata): Retry 2/2: Fetching expiring metadata from targets-worker.
          ERROR (monitor-worker._dispatch_fetch_expiring_metadata): Tried to fetch expiring metadata from targets-worker 2 times, but failed.
          DEBUG (metadata_error.__init__): RuggedMetadataError: Tried to fetch expiring metadata from targets-worker 2 times, but failed.
          ERROR (logger.log_exception): RuggedMetadataError thrown in _fetch_expiring_metadata: Tried to fetch expiring metadata from targets-worker 2 times, but failed.
          ERROR (monitor-worker._fetch_expiring_metadata): Failure during attempt to fetch expiring metadata from targets-worker.
          """

  Scenario: If a 'refresh_expiry_task' task times out, it is retried.
    Given I run the Rugged command "touch /opt/post_to_tuf/refresh_expiry_task.test-flag"
      And I run "sudo rm -rf /opt/post_to_tuf/tuf_refreshing_expiry"
      And I wait "10" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
      And I should get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Received refresh-expiry task.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from targets-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from snapshot-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from timestamp-worker.
          INFO (monitor-worker.refresh_expiry_task): Expiring metadata from targets-worker:
          INFO (monitor-worker.refresh_expiry_task):   targets.json
          INFO (monitor-worker._dispatch_refresh_expiring_metadata): Dispatching task to refresh expiring metadata on targets-worker.
          DEBUG (monitor-worker._run_remote_task): Diverting 'refresh_expiry_task' task from 'targets-worker' to 'sleep' on test worker, to force a timeout.
          DEBUG (timeout_error.__init__): RuggedTimeoutError: The operation exceeded the given deadline.
          WARNING (monitor-worker._dispatch_refresh_expiring_metadata): Task timed-out when refreshing expiring metadata from targets-worker.
          INFO (monitor-worker._dispatch_refresh_expiring_metadata): Retry 1/2: Refreshing expiring metadata from targets-worker.
          INFO (monitor-worker._dispatch_refresh_expiring_metadata): Retry 2/2: Refreshing expiring metadata from targets-worker.
          ERROR (monitor-worker._dispatch_refresh_expiring_metadata): Tried to refresh expiring metadata from targets-worker 2 times, but failed.
          DEBUG (metadata_error.__init__): RuggedMetadataError: Tried to refresh expiring metadata from targets-worker 2 times, but failed.
          ERROR (logger.log_exception): RuggedMetadataError thrown in _refresh_expiring_metadata: Tried to refresh expiring metadata from targets-worker 2 times, but failed.
          ERROR (monitor-worker._refresh_expiring_metadata): Failure during attempt to refresh expiring metadata from targets-worker.
          """
