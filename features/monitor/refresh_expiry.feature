@rugged @monitor @refresh-expiry
Feature: Metadata expiry periods are refreshed automatically.
  In order to keep a TUF systems running smoothly,
  As a TUF administrator
  I need to ensure that metadata expiry periods are refreshed automatically.

  Background:
    Given I reset Rugged
      And I run "sudo rm -rf /opt/post_to_tuf/*"

  Scenario: Ensure that monitor worker has necessary config for testing 'refresh-expiry'.
     When I run the Rugged command "rugged config --worker=monitor-worker"
     Then I should get:
          """
          scheduler_log_level                           DEBUG
          task_timeout                                  3
          scheduler_refresh_period                      5.0
          scheduler_scan_period                         120.0
          """

  Scenario: Ensure that monitor worker is polling for expiry refresh.
    Given I initialize a Rugged repo
    # We have to wait for 6 second to ensure that the scheduled task has run at least once.
      And I wait "6" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Received refresh-expiry task.
          """

  Scenario Outline: Metadata expiry periods are refreshed automatically.
    Given I run the Rugged command "rugged pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I run "sudo cp features/fixtures/config/expiry/short_<ROLE>_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I initialize a Rugged repo
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/<ROLE>.json"
      And I record the expiry timestamp from "<ROLE>.json"
      And I run the Rugged command "rugged resume-processing"
      And I wait (up to "60" seconds) for the file "/var/rugged/tuf_repo/metadata/<ROLE>.json" to change
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should not get:
          """
          ERROR
          """
      And I should get:
          """
          DEBUG (monitor-worker.refresh_expiry_task): Received refresh-expiry task.
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetching expiring metadata from targets-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from targets-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetching expiring metadata from snapshot-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from snapshot-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetching expiring metadata from timestamp-worker.
          DEBUG (monitor-worker._dispatch_fetch_expiring_metadata): Fetched expiring metadata from timestamp-worker.
          INFO (monitor-worker.refresh_expiry_task): Expiring metadata from targets-worker:
          INFO (monitor-worker.refresh_expiry_task): Expiring metadata from snapshot-worker:
          INFO (monitor-worker.refresh_expiry_task): Expiring metadata from timestamp-worker:
          INFO (monitor-worker.refresh_expiry_task):   <ROLE>.json
          INFO (monitor-worker._dispatch_refresh_expiring_metadata): Refreshed <ROLE> metadata expiry period.
          """
     When I run the Rugged command "rugged logs --worker=<ROLE>-worker --limit=0"
     Then I should get:
          """
          '<ROLE>.json' metadata expiry is imminent
          """
      And the expiry timestamp from "<ROLE>.json" has changed
     Examples:
       | ROLE |
       | targets   |
       | snapshot  |
       | timestamp |
