@rugged @workers @monitor-worker
Feature: Scheduled tasks respect pause/resume commands.
  In order to cleanly redeploy Rugged and TUF Root metadata
  As a TUF administrator
  I need to be able to pause and resume scheduled tasks.

  Background:
    Given I reset Rugged
      And I run "sudo rm -rf /opt/post_to_tuf/*"

####
#
# To keep our local logs from filling up the disk, we default to a log-level of
# INFO. For these tests to pass, we need the log-level to be DEBUG. Also we
# default to a 5-second period between scans for new target files.
#
# To speed up development and testing that depends on the scheduler, we can
# reduce this period.
#
# To accomplish this in a local environment, run `make enable-debug-on-monitor`.
#
# To return to the default behaviour, run: `make disable-debug-on-monitor`.
#
# We've disabled the below tasks tagged with "@scheduler", since they won't
# work without the changes described above. To run these, run:
# `ddev behat --tags=scheduler`
#
####

  @scheduler @pause-processing
  Scenario: The pause-processing semaphore blocks scheduled target scans.
    Given I run the Rugged command "rugged pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I initialize a Rugged repo
      And I record the expiry timestamp from "targets.json"
     When I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP/ /opt/post_to_tuf/tuf_ready_TIMESTAMP/"
      And I wait "5" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (pause.monitor_worker_is_paused): Detected pause-processing flag at: /opt/post_to_tuf/tuf_paused
          WARNING (monitor-worker.find_new_targets_task): The monitor worker is paused.
          """
      And I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          """
     Then the "/opt/post_to_tuf/tuf_ready_TIMESTAMP" directory should exist
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should not get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          """
      And the expiry timestamp from "targets.json" has not changed

  @refresh-expiry @pause-processing
  Scenario: The pause-processing semaphore blocks scheduled expiry refresh tasks.
    Given I run the Rugged command "rugged pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I run the Rugged command "cp features/fixtures/config/expiry/short_timestamp_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I initialize a Rugged repo
      And I record the expiry timestamp from "timestamp.json"
          #This is the short expiry that we set in the config above.
      And I wait "5" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (pause.monitor_worker_is_paused): Detected pause-processing flag at: /opt/post_to_tuf/tuf_paused
          WARNING (monitor-worker.refresh_expiry_task): The monitor worker is paused.
          """
      And I should not get:
          """
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          """
     When I run the Rugged command "rugged logs --worker=timestamp-worker --limit=0"
     Then I should not get:
          """
          'timestamp.json' metadata expiry is imminent
          """
      And the expiry timestamp from "timestamp.json" has not changed

  @scheduler @resume-processing
  Scenario: The resume-processing semaphore allows scheduled target scans to start.
    Given I run the Rugged command "rugged pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I initialize a Rugged repo
      And I record the expiry timestamp from "targets.json"
      And I run the Rugged command "rm -rf /opt/post_to_tuf/tuf_refreshing_expiry"
      And I run the Rugged command "rugged pause-processing"
     When I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP/ /opt/post_to_tuf/tuf_ready_TIMESTAMP/"
      And I run the Rugged command "rugged resume-processing"
          #This is the scan period that gets shortened during setup for these tests.
      And I wait "5" seconds
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          """
     Then the "/opt/post_to_tuf/tuf_ready_TIMESTAMP" directory should not exist
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          """
      And the expiry timestamp from "targets.json" has changed

  @refresh-expiry @resume-processing
  Scenario: The resume-processing semaphore allows scheduled expiry refresh tasks to start.
    Given I run the Rugged command "rugged pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I run the Rugged command "cp features/fixtures/config/expiry/short_timestamp_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I initialize a Rugged repo
      And I record a reference hash of "/var/rugged/tuf_repo/metadata/timestamp.json"
      And I record the expiry timestamp from "timestamp.json"
      And I run the Rugged command "rm -rf /opt/post_to_tuf/tuf_refreshing_expiry"
      And I run the Rugged command "rugged pause-processing"
      And I run the Rugged command "rugged resume-processing"
     When I wait (up to "60" seconds) for the file "/var/rugged/tuf_repo/metadata/timestamp.json" to change
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker._fetch_expiring_metadata): Fetching expiring metadata from workers.
          """
     When I run the Rugged command "rugged logs --worker=timestamp-worker --limit=0"
     Then I should get:
          """
          'timestamp.json' metadata expiry is imminent
          """
      And the expiry timestamp from "timestamp.json" has changed
