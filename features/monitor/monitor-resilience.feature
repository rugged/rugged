@rugged @monitor-worker @monitor-resilience @gitlab-142
Feature: Monitor worker is resilient to network instability.
  In order to run reliably for extended periods
  As a TUF operator
  I need the monitor worker to tolerate occasional network instability.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo
      And I run "sudo rm -f /opt/post_to_tuf/*.test-flag"

####
#
# As with other tests related to the monitor worker, we set configuration to
# log at DEBUG level and to speed up scans for new targets (every 1 second).
#
# In addition, we set a test-only config
# (`monitor_enable_network_instability_resiliency_test_mode`) that replaces the
# dispatch of an `add-targets` task to the targets worker, with a `sleep` task
# sent to the test orker. We also set a low `task-timeout` value.
#
# This has the combined effect of reproducing network instability that can
# cause the monitor worker to never receive a return value on the `add-targets`
# task. That is, it forces a timeout.
#
# All of this is required in order for us to test Rugged's resilience to such
# failures. For more details, see: https://gitlab.com/rugged/rugged/-/issues/142
#
# To accomplish this in a local environment, run:
#     `make prep-before-monitor-resilience-tests`
#
# To return to the default behaviour, run:
#     `make reset-after-monitor-resilience-tests`
#
# We've disabled tasks tagged with "@monitor-resilience", ie. all of this
# feature, since they won't work without the changes described above. To run
# these, run `ddev behat --tags=monitor-resilience`.
#
# To run these tests end-to-end, run: `make tests-monitor-resilience`
#
####

  Scenario: Ensure that monitor worker has necessary config for testing 'add-targets' resilience.
     When I run the Rugged command "rugged config --worker=monitor-worker"
     Then I should get:
          """
          scheduler_log_level                                      DEBUG
          scheduler_scan_period                                    1.0
          task_timeout                                             1
          monitor_enable_network_instability_resiliency_test_mode  True
          """

  Scenario: Monitor worker redirects tasks to test worker.
    Given I run the Rugged command "rugged logs --truncate"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/add_targets_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I wait "5" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_FIRST
          DEBUG (monitor-worker._run_remote_task): Diverting 'add_targets_task' task from 'targets-worker' to 'sleep' on test worker, to force a timeout.
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ
          """
      And I should not get:
          """
          INFO (monitor-worker._dispatch_add_targets): Added the following targets to the repository:
          """
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should not get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task
          """
     When I run the Rugged command "rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (test-worker.echo): Test-worker received 'sleep' task with interval of '5'.
          """

  Scenario: Monitor worker detects timeout and stops further processing.
    Given I run the Rugged command "rugged logs --truncate"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/add_targets_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I wait "5" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_FIRST
          DEBUG (monitor-worker._run_remote_task): Diverting 'add_targets_task' task from 'targets-worker' to 'sleep' on test worker, to force a timeout.
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ
          ERROR (monitor-worker._dispatch_add_targets): Timeout while dispatching 'add_targets_task' task to 'targets-worker' for 'tuf_processing_FIRST'.
          """
      And I should not get:
          """
          INFO (monitor-worker._dispatch_add_targets): Added the following targets to the repository:
          DEBUG (monitor-worker.find_new_targets_task): Updating snapshot metadata
          INFO (update_metadata.update_snapshot_metadata): Updated snapshot metadata.
          DEBUG (monitor-worker.find_new_targets_task): Updating timestamp metadata
          INFO (update_metadata.update_timestamp_metadata): Updated timestamp metadata.
          """

  Scenario: Monitor worker detects timeout (add_targets_task) and resets the failed target.
    Given I run the Rugged command "rugged logs --truncate"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/add_targets_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I wait "3" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          ERROR (monitor-worker._dispatch_add_targets): Timeout while dispatching 'add_targets_task' task to 'targets-worker' for 'tuf_processing_FIRST'.
          WARNING (monitor-worker._reset_processing_target): Resetting 'tuf_processing_FIRST' to 'tuf_ready_FIRST'.
          DEBUG (monitor-worker._reset_processing_target): Renaming '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          INFO (monitor-worker._reset_processing_target): Renamed '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          """

  Scenario: Monitor worker retries the failed target first.
    Given I run "sudo fallocate -l 1 /opt/post_to_tuf/add_targets_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_SECOND/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_SECOND/foo2.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_SECOND/ /opt/post_to_tuf/tuf_ready_SECOND/"
      And I wait "3" seconds
    Given I run the Rugged command "rugged logs --truncate"
      And I wait "3" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          ERROR (monitor-worker._dispatch_add_targets): Timeout while dispatching 'add_targets_task' task to 'targets-worker' for 'tuf_processing_FIRST'.
          WARNING (monitor-worker._reset_processing_target): Resetting 'tuf_processing_FIRST' to 'tuf_ready_FIRST'.
          DEBUG (monitor-worker._reset_processing_target): Renaming '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          INFO (monitor-worker._reset_processing_target): Renamed '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          """
     Then I should not get:
          """
          ERROR (monitor-worker._dispatch_add_targets): Timeout while dispatching 'add_targets_task' task to 'targets-worker' for 'tuf_processing_SECOND'.
          WARNING (monitor-worker._reset_processing_target): Resetting 'tuf_processing_SECOND' to 'tuf_ready_SECOND'.
          DEBUG (monitor-worker._reset_processing_target): Renaming '/opt/post_to_tuf/tuf_processing_SECOND' to '/opt/post_to_tuf/tuf_ready_SECOND'.
          INFO (monitor-worker._reset_processing_target): Renamed '/opt/post_to_tuf/tuf_processing_SECOND' to '/opt/post_to_tuf/tuf_ready_SECOND'.
          """

  Scenario: Monitor worker detects timeout (update_snapshot_task) and resets the failed target.
    Given I run the Rugged command "rugged logs --truncate"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/update_snapshot_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I wait "3" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker._network_instability_resiliency_test_mode_enabled_for_task): Checking test mode for 'update_snapshot_task' task.
          DEBUG (monitor-worker._network_instability_resiliency_test_mode_enabled_for_task): Checking for existence of test flag file at '/opt/post_to_tuf/update_snapshot_task.test-flag'.
          ERROR (monitor-worker._dispatch_update_snapshot): Timeout while dispatching 'update_snapshot_task' task to 'snapshot-worker' for 'tuf_processing_FIRST'.
          WARNING (monitor-worker._reset_processing_target): Resetting 'tuf_processing_FIRST' to 'tuf_ready_FIRST'.
          DEBUG (monitor-worker._reset_processing_target): Renaming '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          INFO (monitor-worker._reset_processing_target): Renamed '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          """

  Scenario: Monitor worker detects timeout (update_timestamp_task) and resets the failed target.
    Given I run the Rugged command "rugged logs --truncate"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/update_timestamp_task.test-flag"
      And I run "sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_FIRST/"
      And I run "sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_FIRST/foo1.zip"
      And I run "sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_FIRST/ /opt/post_to_tuf/tuf_ready_FIRST/"
      And I wait "3" seconds
     When I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          DEBUG (monitor-worker._network_instability_resiliency_test_mode_enabled_for_task): Checking test mode for 'update_timestamp_task' task.
          DEBUG (monitor-worker._network_instability_resiliency_test_mode_enabled_for_task): Checking for existence of test flag file at '/opt/post_to_tuf/update_timestamp_task.test-flag'.
          ERROR (monitor-worker._dispatch_update_timestamp): Timeout while dispatching 'update_timestamp_task' task to 'timestamp-worker' for 'tuf_processing_FIRST'.
          WARNING (monitor-worker._reset_processing_target): Resetting 'tuf_processing_FIRST' to 'tuf_ready_FIRST'.
          DEBUG (monitor-worker._reset_processing_target): Renaming '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          INFO (monitor-worker._reset_processing_target): Renamed '/opt/post_to_tuf/tuf_processing_FIRST' to '/opt/post_to_tuf/tuf_ready_FIRST'.
          """
