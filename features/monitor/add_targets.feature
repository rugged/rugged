@rugged @workers @monitor-worker
Feature: A worker to dispatch `add-targets` tasks based on monitoring a directory for target files.
  In order to allow a packaging pipeline not to hold credentials for the task queue,
  As a TUF administrator
  I need to ensure that a monitor worker can check a shared directory and dispatch `add-targets` tasks.

  Background:
    Given I reset Rugged
      And I run the Rugged command "rugged --debug pause-processing --refreshing-timeout=0 --processing-timeout=0"
      And I initialize a Rugged repo
      And I run the Rugged command "rugged logs --truncate"

####
#
# To keep our local logs from filling up the disk, we default to a log-level of
# INFO. For these tests to pass, we need the log-level to be DEBUG. Also we
# default to a 5-second period between scans for new target files.
#
# To speed up development and testing that depends on the scheduler, we can
# reduce this period.
#
# To accomplish this in a local environment, run `make enable-debug-on-monitor`.
#
# To return to the default behaviour, run: `make disable-debug-on-monitor`.
#
# We've disabled the below tasks tagged with "@scheduler", since they won't
# work without the changes described above. To run these, run:
# `ddev behat --tags=scheduler`
#
####

  @scheduler @add-targets
  Scenario: Dispatch `add-targets` task to targets-worker when new target is ready to add.
     When I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
      And I run the Rugged command "rugged resume-processing"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should not get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          """
     When I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP/ /opt/post_to_tuf/tuf_ready_TIMESTAMP/"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP' to 'tuf_processing_TIMESTAMP'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP" directory should not exist
     When I run the Rugged command "rugged logs --worker=targets-worker --limit=0"
     Then I should get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          DEBUG (repo.get_inbound_targets): Scanning for inbound targets in '/var/rugged/incoming_targets'

          DEBUG (repo.get_inbound_targets): Found target: foo.zip
          DEBUG (repo._move_inbound_target_to_targets_dir): Moved '/var/rugged/incoming_targets/foo.zip' to '/var/rugged/tuf_repo/targets/foo.zip'
          INFO (repo._move_inbound_target_to_targets_dir): Moved inbound target 'foo.zip' to targets directory.
          INFO (repo.add_target_to_metadata): Added target 'foo.zip' to 'targets' role.
          DEBUG (repo.sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          """
     When I run the Rugged command "rugged logs --worker=snapshot-worker --limit=0"
     Then I should get:
          """
          INFO (snapshot-worker.update_snapshot_task): Received update-snapshot task.
          DEBUG (repo.sign_metadata): Signed 'snapshot' metadata with 'snapshot' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'snapshot' metadata to file '/var/rugged/tuf_repo/metadata/snapshot.json'.
          INFO (snapshot-worker.update_snapshot_task): Updated snapshot metadata.
          """
     When I run the Rugged command "rugged logs --worker=timestamp-worker --limit=0"
     Then I should get:
          """
          INFO (timestamp-worker.update_timestamp_task): Received update-timestamp task.
          DEBUG (repo.sign_metadata): Signed 'timestamp' metadata with 'timestamp' key.
          DEBUG (repo.write_metadata_to_file): Wrote 'timestamp' metadata to file '/var/rugged/tuf_repo/metadata/timestamp.json'.
          INFO (timestamp-worker.update_timestamp_task): Updated timestamp metadata.
          """
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo.zip": {
          """
      And I should not get:
          """
          tuf_
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  @scheduler @tuf-processing-dir @find-new-targets
  Scenario: Ensure that multiple targets are supported.
    Given I run "for i in $(seq -f '%03g' 0 4); do sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ ; done;"
      And I run "for i in $(seq -f '%03g' 0 4); do sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/foo$i.zip ; done;"
      And I run the Rugged command "rugged resume-processing"
     When I run "for i in $(seq -f '%03g' 0 4); do sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ /opt/post_to_tuf/tuf_ready_TIMESTAMP$i/ ; done;"
      And I wait (up to "60" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo004.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP000
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP004
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP000' to 'tuf_processing_TIMESTAMP000'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP000
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP000" directory should not exist
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo000.zip": {
          "foo001.zip": {
          "foo002.zip": {
          "foo003.zip": {
          "foo004.zip": {
          """
      And I should not get:
          """
          tuf_
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  @scheduler @tuf-processing-dir @find-new-targets
  Scenario: Ensure that targets in directories are supported.
    Given I run "for i in $(seq -f '%03g' 0 1); do sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/dir/ ; done;"
      And I run "for i in $(seq -f '%03g' 0 1); do sudo fallocate -l 1 /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/dir/foo$i.zip ; done;"
      And I run the Rugged command "rugged resume-processing"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
     When I run "for i in $(seq -f '%03g' 0 1); do sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ /opt/post_to_tuf/tuf_ready_TIMESTAMP$i/ ; done;"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo001.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP000
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP001
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP000' to 'tuf_processing_TIMESTAMP000'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP000
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP000" directory should not exist
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "dir/foo000.zip": {
          "dir/foo001.zip": {
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  @scheduler @tuf-processing-dir
  Scenario: Ensure that errant `tuf_` directories (in `inbound_targets`) are ignored when scanning for targets.
     When I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP/foo.zip"
     When I run the Rugged command "mkdir -p /var/rugged/incoming_targets/tuf_ready_TIMESTAMP3/"
      And I run the Rugged command "fallocate -l 1K /var/rugged/incoming_targets/tuf_ready_TIMESTAMP3/foo3.zip"
     When I run the Rugged command "mkdir -p /var/rugged/incoming_targets/tuf_processing_TIMESTAMP4/"
      And I run the Rugged command "fallocate -l 1K /var/rugged/incoming_targets/tuf_processing_TIMESTAMP4/foo4.zip"
     When I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
      And I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP/ /opt/post_to_tuf/tuf_ready_TIMESTAMP/"
      And I run the Rugged command "rugged resume-processing"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo.zip"
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo.zip": {
          """
      And I should not get:
          """
          tuf_
          "foo3.zip": {
          "foo4.zip": {
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  # The Scenarios below all take varying amount of time to complete, and
  # so fail occasionally. As a result, they are all marked @wip.
  # @TODO: Figure out how to get these to work reliably.

  @scheduler @tuf-processing-dir @find-new-targets @wip
  Scenario: Ensure that very big targets are supported.
    Given I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP1/"
      And I run the Rugged command "fallocate -l 3G /opt/post_to_tuf/tuf_tmp_TIMESTAMP1/foo.zip"
      And I run the Rugged command "mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/"
      And I run the Rugged command "fallocate -l 1K /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/foo2.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
     When I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP1/ /opt/post_to_tuf/tuf_ready_TIMESTAMP1/"
      And I run the Rugged command "mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP2/ /opt/post_to_tuf/tuf_ready_TIMESTAMP2/"
      And I run the Rugged command "rugged resume-processing"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo002.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP1
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP2
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP1' to 'tuf_processing_TIMESTAMP1'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP1
          """
     Then I should not get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP2' to 'tuf_processing_TIMESTAMP2'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP2
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP" directory should not exist
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo.zip": {
          "foo2.zip": {
          """
      And I should not get:
          """
          tuf_
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  @scheduler @tuf-processing-dir @find-new-targets @wip
  Scenario: Ensure that several medium-sized targets are supported.
    Given I run "for i in $(seq -f '%03g' 0 4); do sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ ; done;"
      And I run "for i in $(seq -f '%03g' 0 4); do sudo fallocate -l 1M /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/foo$i.zip ; done;"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
     When I run "for i in $(seq -f '%03g' 0 4); do sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ /opt/post_to_tuf/tuf_ready_TIMESTAMP$i/ ; done;"
      And I run the Rugged command "rugged resume-processing"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo004.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP000
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP004
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP000' to 'tuf_processing_TIMESTAMP000'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP000
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP000" directory should not exist
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo000.zip": {
          "foo001.zip": {
          """
      And I should not get:
          """
          tuf_
          """
      And I wait "5" seconds
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """

  @scheduler @tuf-processing-dir @find-new-targets @wip
  Scenario: Ensure that many small targets are supported.
    Given I run "for i in $(seq -f '%03g' 0 499); do sudo sudo -u rugged mkdir -p /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ ; done;"
      And I run "for i in $(seq -f '%03g' 0 499); do sudo fallocate -l 1k /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/foo$i.zip ; done;"
      And I run the Rugged command "rugged logs --worker=monitor-worker --truncate"
     When I run "for i in $(seq -f '%03g' 0 499); do sudo sudo -u rugged mv /opt/post_to_tuf/tuf_tmp_TIMESTAMP$i/ /opt/post_to_tuf/tuf_ready_TIMESTAMP$i/ ; done;"
      And I run the Rugged command "rugged resume-processing"
      And I wait (up to "30" seconds) for the file "/var/rugged/tuf_repo/metadata/targets.json" to contain "foo009.zip"
      And I run the Rugged command "rugged logs --worker=monitor-worker --limit=0"
     Then I should get:
          """
          INFO (monitor-worker.find_new_targets_task): New targets found in post-to-tuf directory:
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP000
          INFO (monitor-worker.find_new_targets_task):   tuf_ready_TIMESTAMP499
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP000' to 'tuf_processing_TIMESTAMP000'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP000
          """
     Then I should not get:
          """
          DEBUG (monitor-worker.find_new_targets_task): Renaming 'tuf_ready_TIMESTAMP499' to 'tuf_processing_TIMESTAMP499'.
          INFO (monitor-worker.find_new_targets_task): Dispatching 'add-targets' task for: tuf_processing_TIMESTAMP499
          """
      And the "/var/rugged/incoming_targets/tuf_processing_TIMESTAMP000" directory should not exist
     When I run "cat /var/rugged/tuf_repo/metadata/targets.json"
     Then I should get:
          """
          "foo000.zip": {
          "foo009.zip": {
          """
      And I should not get:
          """
          "foo499.zip": {
          tuf_
          """
     When I run the Rugged command "rugged validate"
     Then I should get:
          """
          Metadata for the 'root' role is valid.
          Metadata for the 'timestamp' role is valid.
          Metadata for the 'snapshot' role is valid.
          Metadata for the 'targets' role is valid.
          """
